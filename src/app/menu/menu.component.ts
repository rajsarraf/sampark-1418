import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard/dashboard.service';
import { Socialusers } from '../home/user.model';
import { MapsAPILoader } from '@agm/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  socialusers: Socialusers;
  lat: any;
  lng: any;
  city: any;
  address;
  zoom;
  private geoCoder;

  constructor(private dashboardService : DashboardService,private mapsAPILoader: MapsAPILoader) { }

  ngOnInit() {

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getVentBoxGroupList();
    
  }

  ventList = [];
  getVentBoxGroupList() {
    this.dashboardService.getVentBoxGroup()
      .subscribe((response: any) => {
        // console.log('vent Box Response', response);
        this.ventList = response.object;
      })
  }

  sosSend() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

    });
  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.lat, this.lng);
      });
    }
  }
  getAddress(latitude, longitude) {
    console.log('google address result by geocoder', latitude, longitude);
    this.geoCoder = new google.maps.Geocoder;
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      //console.log('google address result by geocoder', results);
      //console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          // console.log('Address In GeoCoder', this.address)
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to sent SOS",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Send SOS'
          }).then((result) => {
            if (result.value) {
              this.dashboardService.sendEmergencyMessage(this.socialusers.id, longitude, latitude, this.address)
                .subscribe((response: any) => {
                  // console.log('Send Emegency Message Response', response);
                  // this.toastr.success('Message Sent successfully');
                })
              Swal.fire(
                'Sent!',
                'Your SOS Sent Successfully',
                'success'
              )
            }
          })

        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }

    });
  }

  birthday(){
    Swal.fire({
      title: 'Birthday',
      text: 'Coming soon',
      imageUrl: 'assets/img/birthday.png',
      imageWidth: 100,
      imageHeight: 100,
      imageAlt: 'Custom image',
    })      
  }

}
