import { Role } from './role';

export class Socialusers {  
    provider: string;  
    id: number;  
    email: string; 
    image?:string; 
    name: string;  
    role: Role; 
    token?: string;  
    idToken?: string; 
    dob:string;
    phone:string;
    username : string;
    password : string;
}