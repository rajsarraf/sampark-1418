import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { FormGroup, FormControl, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Socialusers } from './user.model';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from 'sweetalert2';
import { SocialUser } from 'angularx-social-login';
import { TouchSequence } from 'selenium-webdriver';
import { RegistrationService } from '../registration/registration.service';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  model: any = {};
  isShown: boolean = true;
  isshown: boolean = true;
  btnshown: boolean = false;
  isOTP: boolean = false;
  social1: boolean = true;
  errormassage
  alert
  loading = false;
  socialusers = new Socialusers();
  responseOtp
  loginResponse
  data
  responseData
  phoneField: boolean;
  otpField: boolean;
  response;
  sentOTPFlag : boolean = true;
  verifyOTPFlag : boolean = false;
  otpVerificationFlag : boolean = true;
  resetPasswordFlag : boolean = false;
  loginForm: FormGroup;
  isLoginError: boolean = false;
  returnUrl: string;
  adminReturnUrl:string;
  form: FormGroup;

  constructor(private homeService: HomeService, private router: Router,
    private userService: RegistrationService,
    public OAuth: AuthService, private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,fb: FormBuilder) {
    
      if (this.homeService.currentUserValue) {
        console.log(this.homeService.currentUser)
      this.router.navigate(['/navigation/dashboard']);
      }

      this.form = fb.group({
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      }, {
        validator: this.MatchPassword // your validation method
      })
  

  }

  postForm = new FormGroup({

    content: new FormControl(),

  })

  MatchPassword(AC: AbstractControl) {
    let password = AC.get('password').value; // to get value in input tag
    let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if (password != confirmPassword) {
     // console.log('false');
      AC.get('confirmPassword').setErrors({ MatchPassword: true })
    } else {
      //console.log('true');
      return null
    }
  }

  ngOnInit() {

    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 2000);


    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/navigation/dashboard';
    this.adminReturnUrl = this.route.snapshot.queryParams['adminReturnUrl'] || '/admin/dashboard';
  }


  sentOtp(contact){
    
    console.log(contact)
    this.homeService.sendOtp(contact).subscribe((response : any)=>{

      if(response.object != null){
        this.sentOTPFlag = false;
        this.toastr.success('OTP sent on your register mobile number')
        console.log(response);
        this.verifyOTPFlag = true;
      }else{
        this.toastr.warning(response.message)
        console.log(response)
      }
      
    },err =>{
      this.toastr.error(err);
    }
    )
  }
 
  verifyOtp(contact,otp){
    console.log(contact,otp)
    this.homeService.verifyOtp(contact,otp)
    .subscribe((response : any)=>{  
      if(response.object != null){
        this.toastr.success(response.message)
        console.log(response);
        this.otpVerificationFlag = false;
        this.resetPasswordFlag = true;
      }else{
        this.toastr.warning(response.message)
        console.log(response);
      }
    })
  }

  submitted = false;
  formData
  resetPassword(){
    console.log(this.model.contact,this.form.value)
    this.submitted = true;
    if (this.form.invalid) {
      console.log("Invalid form details");
      return;
    }else {
      console.log(this.form.value);
      this.formData = this.form.value;
      console.log(this.formData.password,this.model.contact)
      this.homeService.forgotPassword(this.model.contact,this.formData.password)
      .subscribe((response : any)=>{
        this.toastr.success(response.message);
        window.document.getElementById("closeChnagePasswordModel").click();
      },err =>{
        this.toastr.error(err);
      }
      )
    }
  }

  login() {

    this.spinner.show();
    this.homeService.loginByPassword(this.model.email, this.model.password)

      .pipe(first())
      .subscribe(
        data => {
          if (data != null) {
            if(data.role == 0){
              if(data.active_status == 1){
                this.spinner.hide();
                console.log('login routing', data.role)
                this.router.navigate([this.returnUrl]);
                this.toastr.success('You have logged in successfully to 1418 !');
              }else{
                this.spinner.hide();
                Swal.fire({
                  title: 'oops..',
                  text: data.remark,
                  icon: 'warning',
                  confirmButtonColor: '#3085d6',  
                  confirmButtonText: 'Ok'
                }).then((result) => {
                  if (result.value) {
                    Swal.fire(
                      'Please Contact To The 1418 Admin',
                      'Thanks'
                    )
                  }
                })
                //this.toastr.warning('Your Are Blocked...Please Contact To the Admin')
                console.log('Your Are Blocked',data);
              }
             
            }else{
              this.spinner.hide();
              console.log('login routing', data.role)
              //this.router.navigate(['/admin/dashboard']);
              this.router.navigate([this.adminReturnUrl]);
              this.toastr.success('You have logged in successfully to 1418 !');
            }
           
          } else {
            this.toastr.error('User ID or Password has been entered incorrectly');
            this.spinner.hide();
          }

        },
        error => {
          this.spinner.hide();
          //this.error = error;
          this.loading = false;
        });
  }


  OnSubmit(email, password) {
    console.log(email);
    this.spinner.show();
    this.homeService.userAuthentication(email, password).subscribe((data: any) => {

      if (this.response.object != null) {
        this.spinner.hide();
        localStorage.setItem('userToken', data.access_token);
        this.router.navigate(['/navigation/dashboard']);
        this.toastr.success('You have logged in successfully to 1418 !');
      } else {
        this.spinner.hide();
        this.toastr.error('User ID or Password has been entered incorrectly');
      }


    },
      (err => {
        this.isLoginError = true;
      }));
  }

error = '';
  //login with otp

  logInWithOtp() {
    this.spinner.show();
    if (this.model != null) {
      if (this.model.otp == null && this.model.phone != null) {
        console.log('Your data is', this.model.phone)
        this.homeService.sendOtp(this.model.phone)
          .subscribe((response: any) => {
            console.log(response)
            if (response.object == null) {
              this.toastr.error('User Does not exist');
              this.spinner.hide();

            } else {
              this.responseOtp = response.object;
              this.otpField = false;
              this.phoneField = true;
              this.toastr.success('OTP sent to registered phone / email ID');
              this.spinner.hide();
            }

          },
            error => console.log(error)
          );
      } else if (this.model.phone != null && this.model.otp != null) {

        this.homeService.loginByOtp(this.model.phone, this.model.otp)
        .pipe(first())
        .subscribe(
          data => {
            if (data != null) {
              if(data.role == 0){
                if(data.active_status == 1){
                  this.spinner.hide();
                  console.log('login routing', data.role)
                  this.router.navigate([this.returnUrl]);
                  this.toastr.success('You have logged in successfully to 1418 !');
                }else{
                  this.spinner.hide();
                  Swal.fire({
                    title: 'oops..',
                    text: data.remark,
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',  
                    confirmButtonText: 'Ok'
                  }).then((result) => {
                    if (result.value) {
                      Swal.fire(
                        'Please Contact To The 1418 Admin',
                        'Thanks'
                      )
                    }
                  })
                  //this.toastr.warning('Your Are Blocked...Please Contact To the Admin')
                  console.log('Your Are Blocked',data);
                }
               
              }else{
                this.spinner.hide();
                console.log('login routing', data.role)
                //this.router.navigate(['/admin/dashboard']);
                this.router.navigate([this.adminReturnUrl]);
                this.toastr.success('You have logged in successfully to 1418 !');
              }
             
            } else {
              this.toastr.error('User ID or Password has been entered incorrectly');
              this.spinner.hide();
            }
  
          },
          error => {
            this.spinner.hide();
            //this.error = error;
            this.loading = false;
          });

         
      }
    }
  }


  //login with social ( Facebook and Google )

  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
      this.toastr.success('You have logged in successfully to 1418 !')
      console.log(socialProvider, socialusers);
      console.log(socialusers.email);
      this.model = socialusers;
      console.log('fb data response in model')
      localStorage.setItem('currentUser', JSON.stringify(socialusers));
      this.router.navigate(['/registration']);

    });
  }

  // Save Facebook Response in local database

  // Savesresponse(socialusers) {
  //   this.userService.Savesresponse(socialusers).subscribe((res: any) => {
  //     debugger;
  //     console.log('response in response section', res);
  //     this.socialusers = res;
  //     console.log('response in socialusers', socialusers)
  //     this.response = res.userDetail;
  //     console.log('response in response details', res.userDetail)
  //     localStorage.setItem('socialusers', JSON.stringify(socialusers));
  //     console.log('response saved in service', localStorage.setItem('socialusers', JSON.stringify(socialusers)));
  //     this.router.navigate(['/navigation/dashboard']);
  //     console.log('data save successfully');

  //   });
  // }

  //Save Google Response in local Database

  // saveGoogleResponse(email, name, lastName, firstName, photoUrl) {
  //   console.log(email)
  //   this.userService.saveGoogleResponse(email, name, lastName, firstName, photoUrl)
  //     .subscribe((res: any) => {
  //       console.log(res);
  //       console.log('response in response section', res);
  //       this.socialusers = res;
  //       console.log('response in socialusers', this.socialusers)
  //       this.response = res.userDetail;
  //       console.log('response in response details', res.userDetail)
  //       localStorage.setItem('socialusers', JSON.stringify(this.socialusers));
  //       console.log('response saved in service', localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
  //       this.router.navigate(['/navigation/dashboard']);
  //       console.log('response Saved successfully');
  //     })
  // }




  toggleShow() {

    this.isShown = !this.isShown;

  }
  myCode() {
    this.isShown = !this.isShown;

  }

  setSaving(element, text) {
    element.textContent = text;

  }
  
  getOTP() {
    this.isOTP = !this.isOTP;
    this.isShown = !this.isShown;
    this.phoneField = false;
    this.otpField = true;
  }
  myfun() {
    this.social1 = !this.social1;
  }
  fun() {
    this.social1 = !this.social1;
  }
}
