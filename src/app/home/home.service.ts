import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Socialusers } from './user.model';
import { SocialUser } from 'angularx-social-login';
import { AppSetting } from '../app.setting';
import { QBHelper } from '../helper/qbHelper';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';
import { QBconfig } from '../QBconfig';

declare var QB: any;

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  socialusers = new Socialusers();
  public _usersCache = [];
  public errorSubject = new Subject<any>();
  public successSubject = new Subject<boolean>()
  usersCacheEvent: EventEmitter<any> = new EventEmitter();
  public chatUser: any;
  private currentUserSubject: BehaviorSubject<Socialusers>;
  public currentUser: Observable<Socialusers>;

  constructor(private http: HttpClient, private qbHelper: QBHelper, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Socialusers>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Socialusers {
    return this.currentUserSubject.value;
  }

  // canActivate(): boolean {
  //   console.log('ghdjgdgjghcthgc')
  //   const self = this;
  //   // localStorage.setItem('currentUser', JSON.stringify(user));
  //   this.chatUser = JSON.parse(localStorage.getItem('loggedinUser'));
  //   // this.chatUser = JSON.parse(localStorage.getItem('currentUser'));
  //   console.log('chatUser Response',this.chatUser)
  //   const sessionResponse = JSON.parse(localStorage.getItem('sessionResponse'));
  //   if (this.qbHelper.getSession() && this.chatUser && sessionResponse) {
  //     return true;
  //   } else if (sessionResponse && this.chatUser) {
  //     self.login({
  //        userLogin: this.chatUser.login,
  //        userName: this.chatUser.full_name,

  //     }).then(() => {
  //       this.router.navigate(['/navigation/dashboard']);
  //     });
  //   } else {
  //     self.qbHelper.qbLogout();
  //   }
  // }


  canActivate(): boolean {
    const self = this;
    this.chatUser = JSON.parse(localStorage.getItem('loggedinUser'));
    console.log('chat User response in active route')
    const sessionResponse = JSON.parse(localStorage.getItem('sessionResponse'));
    if (this.qbHelper.getSession() && this.chatUser && sessionResponse) {
      return true;
    } else if (sessionResponse && this.chatUser) {
      self.login({
        userid: this.chatUser.login,
        name: this.chatUser.full_name,
      }).then(() => {
        this.router.navigate(['/navigation/chat']);
      });
    } else {
      this.logSec = self.qbHelper.qbLogout();
      self.qbHelper.qbLogout();
    }
  }
  logSec
  logoutQuickBlox() {
    this.logSec;
  }


  public addToCache(chatUser: any) {
    const id = chatUser.id;
    console.log('login id jhgkjh', id)
    if (!this._usersCache[id]) {
      this._usersCache[id] = {
        id: id,
        color: Math.floor(Math.random() * (10 - 1 + 1)) + 1
      };
    }
    this._usersCache[id].last_request_at = chatUser.last_request_at;
    this._usersCache[id].name = chatUser.full_name || chatUser.login || 'Unknown user (' + id + ')';
    this.usersCacheEvent.emit(this._usersCache);
    return this._usersCache[id];
  }

  loginByPassword(email: string, password: string) {
    console.log('in Home Service Section', email, password)
    const params = new HttpParams().set('email', email).set('password', password)
    return this.http.post<any>(AppSetting.API_ENDPOINT_JAVA + 'authenticateby_email', params)
      .pipe(map(user => {

        console.log('in service section', user);
        if (user && user.token) {
          if (user.active_status == 1) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
            this.login(user)
          }

        }
        return user;
      }));
  }

  public setUser(user) {
    this.chatUser = user;
    console.log('set User data', user)
    localStorage.setItem('loggedinUser', JSON.stringify(user));
  }

  // create User
  public createUser(chatUser): Promise<any> {
    return new Promise((resolve, reject) => {
      QB.users.create(chatUser, function (createErr, createRes) {
        if (createErr) {
          console.log('User creation Error : ', createErr);
          reject(createErr);
        } else {
          console.log('User Creation successfull ');
          resolve(createRes);
        }
      });
    });
  }

  // update User
  public updateUser(userId, params): Promise<any> {
    const self = this;
    return new Promise((resolve, reject) => {
      QB.users.update(userId, params, function (updateError, updateUser) {
        if (updateError) {
          console.log('User update Error : ', updateError);
          reject(updateError);
        } else {
          self.addToCache(updateUser);
          console.log('User update successfull ', updateUser);
          resolve(updateUser);
        }
      });
    });
  }

  // get Users List
  public getUserList(args): Promise<any> {
    if (typeof args !== 'object') {
      args = {};
    }
    const
      self = this,
      params = {
        filter: {
          field: args.field || 'full_name',
          param: 'in',
          value: args.value || [args.full_name || '']
        },
        order: args.order || {
          field: 'updated_at',
          sort: 'desc'
        },
        page: args.page || 1,
        per_page: args.per_page || 100
      };
    return new Promise(function (resolve, reject) {
      QB.users.listUsers(params, function (userErr: any, usersRes: any) {
        if (userErr) {
          reject(userErr);
        } else {
          console.log('User List === ', usersRes);
          const users = usersRes.items.map((userObj: any) => {
            self.addToCache(userObj.user);
            return userObj.user;
          });
          resolve(users);
        }
      });
    });
  }


  public login(loginPayload) {
    console.log('login payload details', loginPayload)
    return new Promise((resolve, reject) => {
      QB.init(QBconfig.credentials.appId, QBconfig.credentials.authKey, QBconfig.credentials.authSecret, QBconfig.appConfig);
      const user = {
        // login: loginPayload.name,
        // password: loginPayload.password,
        // full_name: loginPayload.name,
        // email : loginPayload.email,
        // phone : loginPayload.phone
        login: loginPayload.userid,
        password: 'quickblox',
        full_name: loginPayload.name
      };
      const loginSuccess = (loginRes) => {
        console.log('login Response :', loginRes);
        this.setUser(loginRes);
        console.log('chat connection :', loginRes.id, user.password, loginPayload.id);
        this.updateChatId(loginPayload.id, loginRes.id);
        // Establish chat connection
        this.qbHelper.qbChatConnection(loginRes.id, user.password).then(chatRes => {
          this.successSubject.next(true);
          resolve();
        },
          chatErr => {
            console.log('chat connection Error :', chatErr);
          });
      };

      const loginError = (error) => {
        const message = Object.keys(error.detail).map(function (key) {
          return key + ' ' + error.detail[key].join('');
        });
        alert(message);
      };

      console.log('User : ', user);
      this.qbHelper.qbCreateConnection(user)
        .then((loginRes) => {
          /** Update info */
          if (loginRes.login !== user.login) {
            console.log('update user info', user)
            this.updateUser(loginRes.id, {
              'full_name': user.login
            }).then(updateUser => {
              loginSuccess(updateUser);
            });
          } else {
            loginSuccess(loginRes);
          }
        })
        .catch((loginErr) => {
          if (loginErr.status === undefined || loginErr.status !== 401) {
            /** Login failed, trying to create account */
            this.createUser(user).then(createRes => {
              console.log('create user success :', createRes);
              // Relogin
              this.qbHelper.qbCreateConnection(user).then(reLoginRes => {
                loginSuccess(reLoginRes);
              });
            }).catch(createErr => {
              loginError(createErr);
            });
          }
        });
    });
  }

  userAuthentication(email, password) {
    var data = "email=" + email + "&password=" + password + "grant_type=" + password;
    const params = new HttpParams().set('email', email).set('password', password);
    return this.http.post(AppSetting.API_ENDPOINT_JAVA + 'authenticateby_email', params)
  }

  getUserDetails() {
    return this.http.get(AppSetting.API_ENDPOINT_JAVA + 'getAlluser', { headers: new HttpHeaders(localStorage.getItem('token')) });
  }

  getUserClaims() {
    return this.http.get(AppSetting.API_ENDPOINT_JAVA + 'getAlluser',
      { headers: new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('userToken') }) });
  }


  sendOtp(phone: string) {
    console.log(phone)
    const params = new HttpParams().set('phone', phone)
    return this.http.post(AppSetting.API_ENDPOINT_JAVA + 'getOtp', params);
  }

  verifyOtp(phone, otp) {
    const params = new HttpParams().set('phone', phone).set('otp', otp);
    return this.http.post(AppSetting.API_ENDPOINT_JAVA + 'verifyOTP', params);
  }

  forgotPassword(phone, password) {
    const params = new HttpParams().set('phone', phone).set('password', password);
    return this.http.post(AppSetting.API_ENDPOINT_JAVA + 'forgotPassword', params);
  }



  loginByOtp(phone: string, otp: string) {
    console.log()
    console.log('in Home Service Section', phone, otp)
    const params = new HttpParams().set('phone', phone).set('otp', otp)
    return this.http.post<any>(AppSetting.API_ENDPOINT_JAVA + 'authenticate_byotp', params)
      .pipe(map(user => {

        console.log('in service section', user);
        if (user && user.token) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  public getRecipientUserId(users) {
    const self = this;
    if (users.length === 2) {
      return users.filter(function (user) {
        if (user !== self.chatUser.id) {
          return user;
        }
      })[0];
    }
  }

  updateChatId(id, chat_id) {
    console.log('update user response', chat_id, id)
    const params = new HttpParams().set('chat_id', chat_id).set('id', id);
    return this.http.post(AppSetting.API_ENDPOINT_JAVA + 'updateChatId', params)
      .subscribe((response : any)=>{
        console.log('chatid update response',response);
      })
  }


}
