export enum Role {
    User = 0,
    Admin = 1,
    Parent = 2
}