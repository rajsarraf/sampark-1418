import { Component, OnInit } from '@angular/core';
import { SportlightService } from './sportlight.service';
import { SocialUser } from 'angularx-social-login';
import { Socialusers } from '../home/user.model';
import { ToastrService } from 'ngx-toastr';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import { DatePipe } from '@angular/common';
// import * as $ from 'jquery';
import { FormGroup, FormControl } from '@angular/forms';
import { HomeService } from '../home/home.service';
import { Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-spotlight',
  templateUrl: './spotlight.component.html',
  styleUrls: ['./spotlight.component.css']
})
export class SpotlightComponent implements OnInit {

  model: any = {};
  data: any = {};
  fileToUpload;
  loading = false;
  spotLightForm: boolean = true;
  spotLightFormPost: boolean = false;
  socialusers: Socialusers;
  subCateroryName: any = {};
  myDate = new Date();
  currentUser: Socialusers;
  imgData : String ="assets/img/avtarbg.jpg";
  trendingContent = 10;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
};
 

  constructor(private spotLightservice: SportlightService, private toastr: ToastrService,
    private updateProfileService: UpdateprofileService, private datePipe: DatePipe
    ,private homeService:HomeService
    ,private router:Router) { }


  postForm = new FormGroup({
    //user_id:new FormControl(12),
    content: new FormControl(),

  })

  ngOnInit() {
    

    this.getCategoryList();
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getTrendingPostList();
    this.getIntrestPostList();
    this.getHobbiesPostList();
    this.getUserDetails();
    this.pendingSpotlightList();
    this.getApprovedSpotlight();
 
  }

  getUserDetails() {
    this.updateProfileService.getUserDetails(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.model = response.object;
      })
  }

  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.spotLightservice.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
        console.log(response);
        this.categoryList = response;
      })
  }
  
  logout() {
    this.homeService.logout();
    this.router.navigate(['/home']);
}

shop(){
  Swal.fire({
    title: 'Shop',
    text: 'Coming soon',
    imageUrl: 'assets/img/Shop.svg',
    imageWidth: 400,
    imageHeight: 200,
    imageAlt: 'Custom image',
  })
}

  subCaterory = [];
  changeCategoryListValue(value) {
    console.log(value)
    this.spotLightservice.getIntrestList(value)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;

      })
  }
 
  // storesubcategoryValue(value){
  //   console.log('subcategory value',value)
  // }


  achivementImage(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onload = (event:any)=>{
      this.imgData= event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    console.log(this.fileToUpload);
  }
 
  onDateChanged(value){
    this.model.date = value.formatted;
    console.log('hsjdfhf',value.formatted)
  }

  spotlightid
  requestSportlight() {
    this.loading = true;
    console.log('category id', this.data.value)
    console.log('subCategory id', this.model.value.id,this.model.value.activity_name)
    console.log('subcateroryname', this.subCateroryName.value)
    console.log(this.model.description, this.model.achievement)
    console.log('before change date formate', this.model.date);
    var data = document.getElementById('datepicker');
    // console.log('date picker',data)
    // var dateFormate = this.datePipe.transform(this.model.date, "MM/dd/yyyy");
    // console.log('Change Date Formate', dateFormate); 

    this.spotLightservice.sendsportLightRequest(this.socialusers.id, this.data.value, this.model.value.id, this.fileToUpload, this.model.description,
      this.model.achievement, this.model.name, this.model.value.activity_name, this.model.date, this.model.location)
      .subscribe((response: any) => {
        console.log(response);
        if (response.object != null) {
          this.toastr.success('SpotLight requests been sent successfully');
          this.model = null;
          //this.mySpotlightForm();
          this.spotlightid = response.object.id;
          window.document.getElementById('close_spotlight_model').click();
          this.pendingSpotlightList();
          console.log('response id for create post in spotlight',response.object.id);
          this.router.navigateByUrl('/navigation/spotlight', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/navigation/spotlight']);
        }); 
        } else {
          this.toastr.error('Something Went Wrong');
          this.loading = false;
        }

      })
  }
  error="";
  //spotlight post
  store() {
    let post = this.postForm.value;
    this.spotLightservice.createPost(this.socialusers.id, post,this.spotlightid, this.fileToUpload, this.socialusers.name)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('response data in post', post);
          //this.fileUpload = post;
          console.log(post);
          //this.fileUpload = post,
         console.log('spotlight post response',response)
          this.fileToUpload = '';
          this.postForm.reset();
          
        } else if (response.object == null) {
          console.log('Something Went Wrong')
        }

      },
        err => this.error = err
      );

  }

  //get Tranding List
  //get Tranding List

  totaldata;
  page1 : number = 1;

  popularPost;
  type = 1;
  getTrendingPostList() {
    this.spotLightservice.getTrendingPostList(this.type,this.page1)
      .subscribe((response: any) => {
        console.log('Trending List', response);
        this.popularPost = response.object.data; 
        this.totaldata = response.object.total
        console.log('trending post lenth',this.totaldata)
      })
      
  }

  popularSpotLightChange(event){
    console.log('page1',event);
    this.page1 = event;
    console.log('pageEvent',this.page1);
    this.spotLightservice.getTrendingPostList(this.type,this.page1)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
      console.log('page2 response',newPost);
      this.popularPost  = newPost;
    })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }
 
  //get interest List

  interest = 1;
  totalInterestData
  page2 : number = 1;
  interestData = [];
  getIntrestPostList() {
    this.spotLightservice.getIntrestPostList(this.socialusers.id, this.interest,this.page2)
      .subscribe((response: any) => {
        console.log('intrest post', response);
        this.interestData = response.object.data;
        this.totalInterestData = response.object.total;
      })
     
  }

  interestSpotLightChange(event){
    console.log('page1',event);
    this.page2 = event;
    console.log('pageEvent',this.page2);
    this.spotLightservice.getIntrestPostList(this.socialusers.id, this.interest,this.page2)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
      console.log('page2 response',newPost);
      this.interestData  =  newPost;
    })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }


  //post according to hobbies
  totalhobbyData;
  page3 : number = 1;
  hobbies = 0;
  hobbiesData = [];
  getHobbiesPostList() {

    this.spotLightservice.getHobbiesPostList(this.socialusers.id,this.hobbies,this.page3)
      .subscribe((response: any) => {
        console.log('post according to hobbies', response);
        this.hobbiesData = response.object.data;
        this.totalhobbyData = response.object.total;
      })
     
  }

  hobbySpotLightChange(event){
    console.log('page1',event);
    this.page3 = event;
    console.log('pageEvent',this.page3);
    this.spotLightservice.getHobbiesPostList(this.socialusers.id, this.interest,this.page3)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
      console.log('page2 response',newPost);
      this.hobbiesData  =  newPost;
    })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }

  approveStatus = 1;
  approvedSpotlight
  totalArrovedSpotlight:Number;
  page4 : number = 1;
  getApprovedSpotlight(){
    this.spotLightservice.approvedSpotlightList(this.socialusers.id,this.approveStatus)
    .subscribe((response : any) =>{
      console.log('approved spotlight list',response)
      this.approvedSpotlight = response.object.data;
      this.totalArrovedSpotlight = response.object.total;
    })
  }

  approvedSpotLightChange(event){
    console.log('page4',event);
    this.page4 = event;
    console.log('pageEvent',this.page4);
    this.spotLightservice.approvedSpotlightListPage(this.socialusers.id,this.approveStatus,this.page4)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
      console.log('page2 response',newPost);
      this.approvedSpotlight = newPost;
    })
  }


  //get post data By id

  postData = [];
  getPostData(id) {
    console.log('post Data By id', id);
    this.spotLightservice.getPostDataById(id)
      .subscribe((response: any) => {
        console.log('post data response', response);
        this.postData = response.object;

      })

  }

  //comment on Post
  addComment(id) {
    console.log('comment in typescript', id, this.model.comment)
    console.log(this.socialusers.id)
    //this.comment = this.commentForm.value;
    console.log(this.model.comment, id);
    this.spotLightservice.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.model.comment = "";

      })
  }

  likePost(post_id) {
    //console.log('like'+post_id)
    return this.spotLightservice.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
    });

  }

//Pending spotlight list

pendingSpotlight = [];
isapprove = 0;
pendingSpolightConfig : any;

totalRecords:Number;
page = 1;

pendingSpotlightList(){
  this.spotLightservice.pendingSpotLight(this.socialusers.id,this.isapprove,this.page)
  .subscribe((response : any)=>{
    console.log('pending spotlight',response)
    this.pendingSpotlight = response.object.data;
    console.log('pending spotlight',this.pendingSpotlight)
    this.totalRecords = response.object.total;
    console.log('Pending spotlight list Data',this.totalRecords);
  })
  
}

pendingSpotLightChange(event){
  console.log('page1',event);
  this.page = event;
  console.log('pageEvent',this.page);
  this.spotLightservice.pendingSpotLight(this.socialusers.id,this.isapprove,this.page)
  .subscribe((response : any)=>{
    const newPost = response.object.data;
    console.log('page2 response',newPost);
    this.pendingSpotlight = newPost;
  })
  //this.page += 1;
  //this.pendingSpolightConfig.currentPage = event;
}
//Spotlight Details

spotLightList=[];
viewPendingSpotlightDetalis(id){
  console.log(id)
   this.spotLightservice.viewPendingSpotlightData(id)
   .subscribe((response : any)=>{
     console.log('View pending spotlight ',response);
     this.spotLightList = response.object;
   })
}



  //spotlight post
  postSpotlight(spotlightid) {
    let post = this.postForm.value;
    this.spotLightservice.createPost(this.socialusers.id, post,spotlightid, this.fileToUpload, this.socialusers.name)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('response data in post', post);
          //this.fileUpload = post;
          console.log(post);
          //this.fileUpload = post,
         console.log('spotlight post response',response)
          this.fileToUpload = '';
          this.postForm.reset();
          this.pendingSpotlightList();
          this.viewPendingSpotlightDetalis(spotlightid);
        } else if (response.object == null) {
          console.log('Something Went Wrong')
        }

      },
        err => this.error = err
      );

  }


  mySpotlightForm() {
    this.spotLightForm = !this.spotLightForm;
    this.spotLightFormPost = !this.spotLightFormPost
  }
}
