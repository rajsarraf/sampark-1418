import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Socialusers } from '../home/user.model';
import { GroupdashboardService } from '../groupdashboard/groupdashboard.service';
import { PeopleFriendService } from '../people-friend/people-friend.service';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import { HomeService } from '../home/home.service';
import { DashboardService } from '../dashboard/dashboard.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  socialusers: Socialusers;
  private user_id: string;
  model: any = { comment: '' };
  searchText;
  fileToUpload
  showMore = false;
  showChar = 200;
  currentusers:Socialusers;
  comment_message: string = "";
  comment_img;
  imgURL: any = "";
  status_id: number;
  message: string = "";
  status = false;

  constructor(private profileService: ProfileService, private router: Router,
    private toastr: ToastrService, private route: ActivatedRoute,private groupDashboardService :GroupdashboardService,
    private peopleFriendService: PeopleFriendService,
    private userService: HomeService,private postService : DashboardService,
    private updateProfileService: UpdateprofileService) { }

  ngOnInit() {
    this.currentusers = JSON.parse(localStorage.getItem('currentUser'));

    this.getUserDetails();

    this.getUserInfo();
    this.getProfileBasicDetails();

    $("input[type='image']").click(function () {
      $("input[id='File']").click();


    });

    $("input[type='image1']").click(function () {
      $("input[id='File1']").click();


    });

  }
  logout() {  
    
    this.userService.logout();
    this.router.navigate(['/home']);
    this.toastr.success('logout successfull')
   }
   mode =[]
   getUserDetails() {
    this.updateProfileService.getUserDetails(this.currentusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.mode = response.object;
      }) 
    }
  getUserInfo() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    console.log('userInformation', this.socialusers)
  }

  userId;
  profileDetails = [];
  coverImage 
  coverimgLenth
  hobbies;
  getProfileBasicDetails() {
    console.log('start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.userId = params["id"];
        console.log(this.userId)
        console.log('process.......');
        this.profileService.viewProfileBasicDetails(this.userId,this.socialusers.id)
          .subscribe((response: any) => {
           
            if (response.object != null) {
              console.log('user profile basic details', response);
              this.profileDetails = response.object;
              for(let data of response.object.cover_image){
                this.coverImage= data.bg_image;

              }
              this.coverimgLenth=response.object.cover_image.length
            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }

  uploadCoverPic(files: FileList){
   
      this.fileToUpload = files.item(0);
      console.log(this.fileToUpload);
    
      this.profileService.uplaodCoverPic(this.socialusers.id,this.fileToUpload)
      .subscribe((response : any)=>{
        console.log('cover pic response',response)
        if(response.object != null){
          console.log('cover pic response',response);
          this.toastr.success('Cover Picture changed successfully');
          this.getProfileBasicDetails();
        }else{
          this.toastr.error('Something Went Wrong');
        }
      })
   
  }
  //post like

  likePost(post_id) {

    return this.groupDashboardService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.getProfileBasicDetails();
    });

  }
  Platform_type = "dashboard";
  addComment(id) {

    var comment_type = 'normal';
    console.log(this.socialusers.id)
    console.log('commnet and id', this.model.comment, id);
    this.postService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload,comment_type,this.Platform_type)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.model.comment = "";
        this.status = true;
        this.model = {};
        this.fileToUpload = null;
        this.imgURL = "";
        this.getProfileBasicDetails();
      })
  }

  // addComment(id) {
  //   console.log(this.socialusers.id)

  //   console.log(this.model.comment, id);
  //   this.groupDashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name)
  //     .subscribe((response: any) => {
  //       console.log('comment Response', response)
  //       //this.status = true;
  //       this.model.comment = "";
  //       this.getProfileBasicDetails();
  //     })

  // }

  deletePost(id) {
    console.log(id)
    this.groupDashboardService.deleteGroupPost(id)
      .subscribe((response: any) => {
        console.log('Delete Post Response', response);
        this.toastr.success('Post has been successfully deleted');
        this.getProfileBasicDetails();

      })
  }


  //friends List

  friendLoader : boolean = false;
  friendstatus = 1;
  requestList = [];
  friendsListData = [];
  friend_type = 0;
  friendList() {
    this.friendLoader = true;
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus,this.friend_type)
      .subscribe((response: any) => {
        this.friendLoader = false;
        console.log('Friends List', response);
        if (response != null) {
          this.friendsListData = response.object;
        } else {
          this.toastr.error('something went wrong');
        }
      })
  }

  //followers list

  followers = [];

  followersList() {
    this.peopleFriendService.followersList(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('followers List', response);
        if(response.object !=  null){
          this.followers = response.object;
        }else{
          console.log('something went wrong...No Followers Available');
        }

      })
  }

  //following list

  following = [];

  followingList() {
    this.peopleFriendService.followingList(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('Following List', response);
        if(response.object != null){
          this.following = response.object;
          console.log('object in following', this.following)
        }else{
          console.log('something went wrong...No Following Available')
        }
        
      })
  }
  unFriend(id) {
    this.peopleFriendService.unFriend(this.socialusers.id, id)
      .subscribe((response: any) => {
        console.log('Unfriend Response', response);
        this.friendList();
      })
  }

  reportFriend(receiver_id) {
    console.log(this.socialusers.id, receiver_id)
    this.peopleFriendService.reportUser(this.socialusers.id, receiver_id)
      .subscribe((response : any) => {
        this.toastr.success('Report to admin successful');
        console.log('report User', response);
        this.friendList();
      })
  }

  request_status = 3;
  blockFriend(receiver_id) {
    console.log(this.socialusers.id, receiver_id, this.request_status)
    this.peopleFriendService.blockUser(this.socialusers.id, receiver_id, this.request_status)
      .subscribe((response: any) => {
        console.log('block user', response);
        this.toastr.success('User has been successfully blocked');
        this.friendList();
      })
  }

  followFriend(id) {
    console.log(this.socialusers.id, id)
    this.peopleFriendService.followFriend(this.socialusers.id, id)
      .subscribe((response: any) => {
        console.log('follow Response sent', response);
        this.toastr.success(response.message)
       // this.followingList();
       // this.followersList();
        //this.friendList();
        this.getProfileBasicDetails();
      })
  }
  
  sentRequestList = [];
  request = 0;
  type = 0;
  sentFriendRequestList(){
    this.profileService.sentFriendRequest(this.socialusers.id,this.request,this.type)
    .subscribe((response: any)=>{
      if(response.object != null){
        console.log('sent Friend Request list',response);
        this.sentRequestList = response.object;
      }else{
        console.log('something Went wrong')
      }
      
    })
  }

  cancelSentRequest(id){
    this.profileService.cancelSentRequest(this.socialusers.id,id)
    .subscribe((response : any) =>{
      if(response != null){
        console.log('cancel request',response);
        this.toastr.success('Request has been cancelled');
        this.getProfileBasicDetails();
        this.sentFriendRequestList();
      }else{
        this.toastr.error('Something went wrong');
        console.log('Something Went Wrong');
      }
     
    })
  }

  div_id;
  triggerUpload(id) {
    this.div_id = id;
    console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }
  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }
  handleComment($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.model.comment += $event.emoji.native;
  }

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.postService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }

  editPostModel: any = {};
  editData = [];
  editFormData
  showPost(id) {
    console.log(id);
    this.postService.showPostEdit(id)
      .subscribe((response: any) => {
        console.log('showpost', response)
        this.editPostModel = response.object;

      })
  }

  imageToUpload;
  editPostImage(files: FileList) {
    this.imageToUpload = files.item(0);
    console.log('Edit Image', this.imageToUpload);
  }

  editUserPost(id, content) {
   // let formData = this.editForm.getRawValue()
    console.log('Edit Image in Typescript', id, this.imageToUpload, content)
    this.postService.updatePost(id, this.imageToUpload, content)
      .subscribe((response: any) => {
        if (response.object != null) {
          this.toastr.success('Your Post has been updated successfully');
          content = {};
          this.getProfileBasicDetails();
          window.document.getElementById("close_model").click();
        } else {
          this.toastr.warning('Something Went wrong');
        }

      })
  }

}
