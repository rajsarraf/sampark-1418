import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppSetting } from '../app.setting';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  getUserAllDetails(user_id) {

    const params = new HttpParams().set('user_id', user_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'basicinfo', params)
  }

  viewUserProfile(user_id, sender_id) {
    const params = new HttpParams().set('user_id', user_id).set('sender_id', sender_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'basicinfo', params)

  }


viewProfileBasicDetails(user_id,sender_id){
   const params = new HttpParams().set('sender_id',sender_id).set('user_id',user_id);
   return this.http.post(AppSetting.API_ENDPOINT_PHP+'basicinfo',params);
}

uplaodCoverPic(user_id,File){
  const formData = new FormData();
  formData.append('user_id',user_id);
  formData.append('file',File,File.name);
  return this.http.post(AppSetting.API_ENDPOINT_PHP+'coverimage/store_image',formData);
}

unFriend(user_sender_id,user_receiver_id){
  const params = new HttpParams().set('user_sender_id',user_sender_id).set('user_receiver_id',user_receiver_id);
  return this.http.post(AppSetting.API_ENDPOINT_PHP+'addfriends/unfriend',params);
}


sentFriendRequest(user_id,request_status,friend_type){
  const params =  new HttpParams().set('user_id',user_id).set('request_status',request_status)
  .set('friend_type',friend_type);
  return this .http.post(AppSetting.API_ENDPOINT_PHP+'friendrequestsent',params)
}

//http://social.nuagedigitech.com/api/addfriends/unfriend?user_sender_id=4&user_receiver_id=3

cancelSentRequest(user_sender_id,user_receiver_id){
    const params = new HttpParams().set('user_sender_id',user_sender_id).set('user_receiver_id',user_receiver_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+ 'addfriends/unfriend',params);
}
 
}
