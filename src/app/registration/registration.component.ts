import { Component, OnInit } from '@angular/core';
import { RegistrationService } from './registration.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { Router, ActivatedRoute } from '@angular/router';
import { Socialusers } from '../home/user.model';
import { HomeService } from '../home/home.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

  response;
  socialusers = new Socialusers();
  model: any = {};
  loginmodel: any = {};
  isLoginError: boolean;
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  emailPattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)";
  hide = true;
  returnUrl: string;
  adminReturnUrl: string;
  passwordShow : boolean = true;
  passwordHide : boolean = false;


  constructor(private userService: RegistrationService,
    public OAuth: AuthService, private router: Router,
    private homeService: HomeService, private toastr: ToastrService, private spinner: NgxSpinnerService,
    private route: ActivatedRoute) { }


  ngOnInit() {
    this.spinner.hide();
    this.hide = true;

    // this.model = JSON.parse(localStorage.getItem('socialusers'));
    // console.log('fb data in model localstorage',this.model)

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/navigation/dashboard';
    this.adminReturnUrl = this.route.snapshot.queryParams['adminRetyrnUrl'] || '/admin';
    this.getAVtarList();
    
  }


  avtarId
  avtarList = [];
  selectedAvtar;
  selectedAvtarId;
  getAVtarList() {
    console.log('start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.avtarId = params["id"];
        console.log(this.avtarId)
        console.log('group process.......');
        this.userService.getAvtarById(this.avtarId)
          .subscribe((response: any) => {

            if (response != null) {
              this.selectedAvtar = response.select_image.image_url;
              this.selectedAvtarId = response.select_image.id;
              this.avtarList = response;
              console.log('Avtar Data', response)
            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }

  updateSelectedImage(user_id) {

    this.userService.updateUserRegistrationTime(user_id, this.selectedAvtarId)
      .subscribe((response: any) => {
        console.log('Selected Image Upload Response', response);
      })
  }

  showPassword() {
    this.hide = false;
    this.passwordShow = false;
    this.passwordHide = true;
  }
  hidePassword(){
    this.hide = true;
    this.passwordShow = true;
    this.passwordHide = false;
  }

  ageFromDateOfBirth(value) {
    const today = new Date();

    const birthDate = new Date(value);
    console.log(birthDate)
    let age = today.getFullYear() - birthDate.getFullYear();
    // this.model = age;
    console.log(age)

    const month = today.getMonth() - birthDate.getMonth();
    if (month < 0 || (month == 0 && today.getDate() < birthDate.getDate())) {
      age--;
      //this.model = age;
      if (age > 14) {
        console.log('parent Registratation');
        this.model.age = age;
      } else {
        console.log('Child Registratation')
        this.model.age = age;
      }
    }
  }

  register() {
    this.spinner.show();
    console.log(this.model)
    console.log(this.model.age);
    if (this.model.age >= 14 && this.model.age <= 18) {
      this.userService.createUser(this.model)
        .subscribe((response: any) => {
          console.log('resgister response', response);
          if (response.object != null) {
            this.model = {};
            this.updateSelectedImage(response.object.id);
            console.log('User regsitration successfull');
            this.router.navigate(['/home']);
            this.toastr.success('User has been registered successfully. Welcome to 1418 !');
            this.spinner.hide();
          } else {
            console.log('This user ID already exists');
            this.toastr.warning('This user ID already existst')
            this.spinner.hide();
          }
        }, (error) => {
          console.log('Error from server', error)
          console.log('All Fields are Mandatory');
          this.toastr.warning('All Field Are mandatory');
          this.spinner.hide();
        }
        )
    } else {
      this.toastr.warning('Your age has to be between 14 to 18 to register as a user');
      this.spinner.hide();
    }


  }

  loading: boolean;
  login() {

    this.spinner.show();
    this.homeService.loginByPassword(this.loginmodel.email, this.loginmodel.password)

      .pipe(first())
      .subscribe(
        data => {
          if (data != null) {
            if (data.role == 0) {
              if (data.active_status == 1) {
                this.spinner.hide();
                console.log('login routing', data.role)
                this.router.navigate([this.returnUrl]);
                this.toastr.success('Welcome ' + data.name);
              } else {
                this.spinner.hide();
                Swal.fire({
                  title: 'oops..',
                  text: data.remark,
                  icon: 'warning',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Ok'
                }).then((result) => {
                  if (result.value) {
                    Swal.fire(
                      'Please Contact To The 1418 Admin',
                      'Thanks'
                    )
                  }
                })
                //this.toastr.warning('Your Are Blocked...Please Contact To the Admin')
                console.log('Your Are Blocked', data);
              }

            } else {
              this.spinner.hide();
              console.log('login routing', data.role)
              //this.router.navigate(['/admin/dashboard']);
              this.router.navigate([this.adminReturnUrl]);
              this.toastr.success('Welcome ' + data.name);
            }

          } else {
            this.toastr.error('Bad Credential');
            this.spinner.hide();
          }

        },
        error => {
          this.spinner.hide();
          //this.error = error;
          this.loading = false;
        });
  }

  // login() {

  //   this.spinner.show();

  //   if (this.loginmodel != null) {
  //     if (this.loginmodel.email != null && this.loginmodel.password != null) {
  //       this.homeService.loginByPassword(this.loginmodel.email, this.loginmodel.password)
  //         .subscribe((response: any) => {
  //           if (response != null) {

  //             this.socialusers = response;
  //             localStorage.setItem('socialusers', JSON.stringify(this.socialusers));
  //             //console.log('Response message is',response.message);
  //             // localStorage.setItem('userToken',response.object)

  //             this.router.navigate(['/navigation/dashboard']);

  //             this.toastr.success(this.socialusers.name + ' Login Successfull')
  //             this.spinner.hide();
  //           } else {
  //             this.isLoginError = true;
  //             this.toastr.error('Bad Credential');
  //             console.log(response.message);
  //             this.spinner.hide();
  //           }
  //         }, error => console.log(error)
  //         );
  //     }
  //   }

  // }





  //social login 

  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
      console.log(socialProvider, socialusers);
      console.log(socialusers.email);
      //this.toastr.success(this.socialusers.name + ' Login Successfull')
      //this.Savesresponse(socialusers);
      this.model = socialusers;
      console.log('fb data response in model')
      localStorage.setItem('socialusers', JSON.stringify(socialusers));
      if (socialusers.provider = 'google') {
        //  this.saveGoogleResponse(socialusers.email, socialusers.name, socialusers.lastName, socialusers.firstName, socialusers.photoUrl);
      } else {
        // this.Savesresponse(socialusers);
      }

      // this.router.navigate(['/dashboard']);

    });
  }

  // Savesresponse(socialusers) {
  //   this.userService.Savesresponse(socialusers).subscribe((res: any) => {
  //     debugger;
  //     console.log('response in response section', res);
  //     this.socialusers = res;
  //     console.log('response in socialusers', socialusers)
  //     this.response = res.userDetail;
  //     console.log('response in response details', res.userDetail)
  //     localStorage.setItem('socialusers', JSON.stringify(socialusers));
  //     console.log('response saved in service', localStorage.setItem('socialusers', JSON.stringify(socialusers)));
  //     this.router.navigate(['/navigation/dashboard']);
  //     console.log('data save successfully');

  //   });
  // }
  // saveGoogleResponse(email, name, lastName, firstName, photoUrl) {
  //   console.log(email)
  //   this.userService.saveGoogleResponse(email, name, lastName, firstName, photoUrl)
  //     .subscribe((res: any) => {
  //       console.log(res);
  //       console.log('response in response section', res);
  //       this.socialusers = res;
  //       console.log('response in socialusers', this.socialusers)
  //       this.response = res.userDetail;
  //       console.log('response in response details', res.userDetail)
  //       localStorage.setItem('socialusers', JSON.stringify(this.socialusers));
  //       console.log('response saved in service', localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
  //       this.router.navigate(['/navigation/dashboard']);
  //       console.log('response Saved successfully');
  //     })
  // }

}
