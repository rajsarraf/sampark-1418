import { Component, OnInit } from '@angular/core';
import { Socialusers } from '../home/user.model';
import { HomeService } from '../home/home.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { first } from 'rxjs/operators';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  isLoginError: boolean;
  model: any = {};
  response;
  socialusers = new Socialusers();
  returnUrl
  loading:boolean;
  adminReturnUrl:string;

  constructor(private homeService: HomeService,
    private router: Router, private toastr: ToastrService, private spinner: NgxSpinnerService,
    private route: ActivatedRoute,private register : RegisterService) {

     }

  ngOnInit() {
    this.spinner.hide();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/navigation/dashboard';
    this.adminReturnUrl = this.route.snapshot.queryParams['adminReturnUrl'] || '/admin/dashboard';
    this.getAvtarImage();
    this.popUpOpen();
  }

  popUpOpen(){
    ($('#myModal') as any).modal('show');
  }

  closeVideo(){
    
    let audioPlayer = <HTMLVideoElement> document.getElementById("videoId");
    audioPlayer.pause();
    
    // $('#myModal').on('hidden.bs.modal', function (e) {
    //   console.log('click section')
    //   var iframe = document.getElementById("videoId");
    //   let audioPlayer = <HTMLVideoElement> document.getElementById("videoId");
    //   audioPlayer.pause();
    
    // })
    
  }

  login() {

    this.spinner.show();
    this.homeService.loginByPassword(this.model.email, this.model.password)

    .pipe(first())
    .subscribe(
      data => {
        if (data != null) {
          if(data.role == 0){
            if(data.active_status == 1){
              this.spinner.hide();
              console.log('login routing', data.role)
              this.router.navigate([this.returnUrl]);
              this.toastr.success('Welcome ' + data.name);
            }else{
              this.spinner.hide();
              Swal.fire({
                title: 'oops..',
                text: data.remark,
                icon: 'warning',
                confirmButtonColor: '#3085d6',  
                confirmButtonText: 'Ok'
              }).then((result) => {
                if (result.value) {
                  Swal.fire(
                    'Please Contact To The 1418 Admin',
                    'Thanks'
                  )
                }
              })
              //this.toastr.warning('Your Are Blocked...Please Contact To the Admin')
              console.log('Your Are Blocked',data);
            }
           
          }else{
            this.spinner.hide();
            console.log('login routing', data.role)
            //this.router.navigate(['/admin/dashboard']);
            this.router.navigate([this.adminReturnUrl]);
            this.toastr.success('Welcome ' + data.name);
          }
         
        } else {
          this.toastr.error('Bad Credential');
          this.spinner.hide();
        }

      },
      error => {
        this.spinner.hide();
        //this.error = error;
        this.loading = false;
      });
  }


  avtarList = [];
  getAvtarImage(){
    this.register.getAvtarList()
    .subscribe((response : any)=>{
      console.log('response Avtar',response);
      this.avtarList = response;
    })
  }

}
