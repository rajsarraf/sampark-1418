import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { AppSetting } from '../app.setting';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http : HttpClient) { }

  getAvtarList(){
    return this .http.get(AppSetting.API_ENDPOINT_PHP+'fetch_avtar_images')
  }

}
