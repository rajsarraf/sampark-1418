import { Component, OnInit } from '@angular/core';
import { GroupsService } from './groups.service';
import { Socialusers } from '../home/user.model';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { PeopleFriendService } from '../people-friend/people-friend.service';
import { SportlightService } from '../spotlight/sportlight.service';
import { PostLikeService } from '../dashboard/post-like.service';
import * as $ from 'jquery';
import { DashboardService } from '../dashboard/dashboard.service';
import { HomeService } from '../home/home.service';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  groupmodel: any = {};
  data: any = {};
  fileToUpload;
  socialusers: Socialusers;
  model: boolean;
  friendListModel: boolean;
  category: any = {};
  interest: any = {};
  currentUser: Socialusers;
  trendingContent = 10;

  imgData : String ="https://www.trickscity.com/wp-content/uploads/2016/04/Top-100-Whatsapp-group-name-for-friends-1024x576.jpg/";
  constructor(private groupsService: GroupsService, private toastr: ToastrService,
    private router: Router, private peopleFriendService: PeopleFriendService,
    private spotLightservice: SportlightService, private postLikeService: PostLikeService, 
    private dashboardService: DashboardService,private updateProfileService: UpdateprofileService
    ,private homeService:HomeService) { }

  private subscription: Subscription;



  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.getUserDetails();


    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });

    this.getUserInfo();

    this.subscription = this.groupsService.userChanged
      .subscribe(
        (m) => {
          console.log('Refresh after' + m);

          this.getUserInfo();
        }
      );
    this.getCategoryList();
    this.getUserGroupList();
    this.getTrendingPostList();
    this.groupsBasedOnInterest();
    this.groupsBasedOnProfile();

  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  mode=[]
  getUserDetails() {
    this.updateProfileService.getUserDetails(this.currentUser.id)
      .subscribe((response: any) => {
       // console.log('user Details Response', response);
        this.mode = response.object;
      })
  }
  logout() {
    this.homeService.logout();
    this.router.navigate(['/home']);
}

  getUserInfo() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));

  }


  groupIcon(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onload = (event:any)=>{
      this.imgData = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    //console.log(this.fileToUpload);

  }

  status = 0;
  createGropupRequest() {
    
    // var group_answer =[]
    //     var data:any ={ }
    // for(let item of this.userGroupList.groupquestions){
    //   data.id= item.id
    //   data.answer=item.answer
    //    group_answer.push(Object.assign({}, data))
    // }
    //console.log(this.socialusers.id, this.category.value)
    this.groupsService.createGroupRequest(this.socialusers.id, this.category.value,
      this.groupmodel.name, this.groupmodel.description,
      this.fileToUpload, this.status, this.data.value, this.interest.value).subscribe((response: any) => {
       // console.log(response);
        if (response.object != null) {
          this.toastr.success('Group creation request has been Sent');
          this.getUserInfo();
          this.router.navigate(['/navigation/groups']);
          this.getUserGroupList();
          window.document.getElementById("close_group_model").click();
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  userGroupList : any=[];
  totaluserGroup
  page : number = 1;
  getUserGroupList() {
    this.groupsService.getUserGroupList(this.socialusers.id,this.page)
      .subscribe((response: any) => {
       // console.log('Your Group List', response.object.data);
        // for (let data of response.groupquestions) {
        //   data.answer = ""
        // }
        this.userGroupList = response.object.data;
        this.totaluserGroup = response.object.total;
      });
  }

  userGroupChange(event){
    console.log('page1',event);
    this.page = event;
    //console.log('pageEvent',this.page);
    this.groupsService.getUserGroupList(this.socialusers.id,this.page)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
      //console.log('page2 response',newPost);
      this.userGroupList  =  newPost;
    })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }

  // send request for adding member in group


  addStatus = 0;
  requestForAddFriendGroup(groupid, friend_id) {
    this.groupsService.addFriendInGroupRequest(groupid, friend_id, this.addStatus)
      .subscribe((response: any) => {
       // console.log('add friend group response', response);
        if (response.object == null) {
          //console.log(response.message);
          this.toastr.warning(response.message);
        } else {
         // console.log(response.message);
          this.toastr.success('user Added')
        }
      })
  }

  //create Group Admin
  is_admin = 1;
  groupid
  friend_id
  makeGroupAdmin() {
    this.groupsService.makeGroupAdmin(this.is_admin, this.groupid, this.friend_id)
      .subscribe((response: any) => {
        console.log('add group admin Request', response);
      })
  }

  //get friend List
  // friendstatus = 1;
  // friendList

  // getFriendList() {
  //   this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus)
  //     .subscribe((response: any) => {
  //       console.log('Friends List', response);
  //       this.friendList = response.object;
  //     })
  // }

  openGroup() {
   // console.log('clicked')
    this.friendListModel = true;
  }


  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.groupsService.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
       // console.log('interest category', response);
        this.categoryList = response.object;
      })
  }

  subCaterory = [];
  changeCategoryListValue(value) {
    //console.log(value)
    this.groupsService.getIntrestList(value)
      .subscribe((response: any) => {
       // console.log(response);
        this.subCaterory = response.object;
      })
  }


  trendingPost = [];
  type = 0;
  getTrendingPostList() {
    this.dashboardService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        //console.log('Trending List', response);
        this.trendingPost = response.object;
      })
  }

  likePost(post_id) {
    //console.log('like'+post_id)
    return this.postLikeService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);

    });

  }

  // Groups On Behalf of Interest

 
  InterestedGroupsList = [];
  totalintrestedGroup
  page1  : number = 1;
  groupsBasedOnInterest() {
    this.groupsService.groupsBasedOnInterest(this.socialusers.id,this.page1)
      .subscribe((response: any) => {
       // console.log('Group Details Based On Interest', response);
        this.InterestedGroupsList = response.object.data;
        this.totalintrestedGroup = response.object.total;
      })
  }

  intrestedGroupChange(event){
   // console.log('page1',event);
    this.page1 = event;
    //console.log('pageEvent',this.page1);
    this.groupsService.groupsBasedOnInterest(this.socialusers.id,this.page1)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
     // console.log('page2 response',newPost);
      this.InterestedGroupsList  =  newPost;
    })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }

  //groups On Behalf Of Profile

  profileGroupList = [];
  totalprofileGroup;
  page2 : number = 1; 
  groupsBasedOnProfile() {
    this.groupsService.groupsBasedOnProfile(this.socialusers.id,this.page2)
      .subscribe((response: any) => {
       // console.log('Group list On Behalf Of profile', response);
        this.profileGroupList = response.object.data;
        this.totalprofileGroup = response.object.total;
      })
  }

  profileGroupChange(event){
   // console.log('page1',event);
    this.page2 = event;
   // console.log('pageEvent',this.page2);
    this.groupsService.groupsBasedOnProfile(this.socialusers.id,this.page2)
    .subscribe((response : any)=>{
      const newPost = response.object.data;
    //  console.log('page2 response',newPost);
      this.profileGroupList  =  newPost;
    })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }

  // join Group

  joinStatus = 1;

  joinGroup(id) {
    this.groupsService.joinGroup(id, this.socialusers.id, this.joinStatus)
      .subscribe((response: any) => {
        //console.log(response);
        if (response.object == null) {
         // console.log(response.message);
          this.toastr.warning(response.message);
          
        } else {
        //  console.log(response.message);
          this.toastr.success('Group has been successfully joined');
          this.getUserGroupList();
          this.groupsBasedOnInterest();
          this.groupsBasedOnProfile();
        }
      })
  }


  seeAll: boolean = true;
  SeeAllFun() {
    this.seeAll = !this.seeAll
  }
  seeAll1: boolean = true;
  SeeAllFun1() {
    this.seeAll1 = !this.seeAll1
  }

  //your groups seeAll
  seeAllgroup: boolean = true;
  SeeAllFungroup() {
    this.seeAllgroup = !this.seeAllgroup
  }
  seeAll1group: boolean = true;
  SeeAllFun1group() {
    this.seeAll1group = !this.seeAll1group
  }

  //profile based groups
  seeAllProfile: boolean = true;
  SeeAllFunProfile() {
    this.seeAllProfile = !this.seeAllProfile
  }
  seeAll1Profile: boolean = true;
  SeeAllFun1Profile() {
    this.seeAll1Profile = !this.seeAll1Profile
  }

}
