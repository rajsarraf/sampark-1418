import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { XploreService } from '../xplore.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxLinkifyjsService } from 'ngx-linkifyjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Socialusers } from 'src/app/home/user.model';
import { PeopleFriendService } from 'src/app/people-friend/people-friend.service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.css']
})
export class TrendingComponent implements OnInit {

  socialusers: Socialusers;
  user: any;
  status = false;
  CloseBtn: boolean = true;
  post: any;
  postObj;
  model: any = { comment: '' };
  editPostModel: any = {};
  fileToUpload;
  fileUpload: any = { status: '', message: '', filePath: '' };
  error: string;
  iFlag: boolean = false;
  message: string = "";
  status_id: number;
  privatepost;
  searchmodel: any = {};
  notscrolly = true;
  notEmptyPost = true;
  comment_message: string = "";
  comment_img;
  imgURL: any = "";

  selectedItems = { id: "", groupname: "" };
  dropdownSettings = {};

  constructor(private xploreService: XploreService, public linkifyService: NgxLinkifyjsService, private spinner: NgxSpinnerService,
    private router: Router, private toastr: ToastrService, private peopleFriendService: PeopleFriendService,
    private dashboardService: DashboardService) { }


  postForm = new FormGroup({

    content: new FormControl(),

  })

  ngOnInit() {

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.posts();
    var videos = document.getElementsByTagName("video");

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      enableCheckAll: false,
      // selectAllText: '',
      // unSelectAllText: '',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.getFriendList();

  }

  // get all posts
  page: number = 1;
  showMore = false;
  showChar = 200;
  link = []
  allPost
  posts() {

    //console.log('page count', this.page)

    this.xploreService.getAdminTrendingPost(this.socialusers.id).subscribe((data: any) => {
      this.post = data
      console.log('post response', this.post);
      this.allPost = this.post.object.data;
    }, err => {
      console.log(err)
    }
    );
  }


  scrollcount

  onScrollDown() {
    console.log('scrolling...................', this.page)
    if (this.notscrolly && this.notEmptyPost) {
      const start = this.page;
      this.page += 1;
      this.spinner.show();
      this.notscrolly = false;
      // console.log('page.................', this.page)
      this.xploreService.getAdminTrendingPostPage(this.socialusers.id, this.page)
        .subscribe((response: any) => {
          const newPost = response.object.data;
          this.spinner.hide();
          this.notEmptyPost = false;
          // console.log('page 2 response',response);
          if (newPost != null) {
            this.allPost = this.allPost.concat(newPost)
            this.notscrolly = true;
            //this.notEmptyPost = false;
          } else {
            this.notscrolly = false;
            this.notEmptyPost = false;
          }
        })
    }

  }

  addComment(id) {
    console.log(this.dataArr)
    if (this.dataArr.length == 0) {
      var comment_type = 'normal';
      console.log('commnet and id', this.model.comment, id);
      this.xploreService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type)
        .subscribe((response: any) => {
          //console.log('comment Response', response)
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.router.navigateByUrl('/navigation/trending', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/navigation/trending']);
          });
        })
    } else {
      console.log('taged section')
      var comment_type_tag = 'tag'
      this.xploreService.commentTagPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type_tag, this.dataArr)
        .subscribe((response: any) => {
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.router.navigateByUrl('/navigation/trending', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/navigation/trending']);
          });
        })

    }

  }

  div_id;
  triggerUpload(id) {
    this.div_id = id;
    // console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    //console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }
  handleSelection($event) {
    //console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }

  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    //console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }

  handleComment($event) {
    //console.log($event);
    if (this.status_id != null) {
      this.model.comment += $event.emoji.native;
    }
    //this.selectedEmoji = $event.emoji;

  }

  //delete post

  deletePost(id) {
    this.spinner.show()
    //console.log(id)
    this.xploreService.deletePost(id)
      .subscribe((response: any) => {
        // console.log('Delete Post Response', response);
        this.posts();
        this.spinner.hide();
        this.toastr.success('post deleted successfully');
      })
  }

  //Report post

  reportPost(id, receiver_id) {

    // console.log(id, this.socialusers.id, receiver_id)
    this.xploreService.reportPost(id, this.socialusers.id, receiver_id)
      .subscribe((response => {
        // console.log('response', response);
        // console.log('report Added successfully');
        this.toastr.success('Report Added');
        this.posts();
      }))


  }


  likePost(post_id) {
    //console.log('like'+post_id)
    this.xploreService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      //console.log('like Data ', data);
      this.posts();

    });

  }

  input_id
  focusFunction(id) {
    console.log('focus function', id)
    this.input_id = id;
  }

  tagSection: boolean = false;
  friendstatus = 1;
  friendsList = [];
  friend_type = 0;
  dropdownList = [];
  doSomething(event) {
    console.log(event)
    var compare_text = event
    var search_value = new Array("@");
    for (var i = 0; i < search_value.length; i++) {
      for (var j = 0; j < (compare_text.length); j++) {
        if (search_value[i] == compare_text.substring(j, (j + search_value[i].length)).toLowerCase()) {
          console.log('tag section');
          this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
            .subscribe((response: any) => {
              console.log('Friends List', response);
              if (response != null) {
                this.friendsList = response.object;
                this.dropdownList = response.object;
                this.tagSection = true
                //this.popUpOpen();
              } else {
                this.toastr.error('something went wrong');
              }
            })
        } else {
          //console.log('Comment Section')
        }
      }

    }


  }

  popUpOpen() {
    console.log('popup');
    ($('#tagModal') as any).modal('show');
  }

  dataArr = []
  myArrStr
  concvertData
  onItemSelect(item: any) {
    this.tagSection = false;
    console.log('item', item);
    this.dataArr.push(item.id);
    this.model.comment += item.name
    //this.myArrStr = JSON.stringify(this.dataArr);
    console.log('my final array', this.dataArr);
  }

  onDeSelect(item: any) {
    this.tagSection = false;
    this.dataArr.pop();
    console.log('Deselect item', item)
  }

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.dashboardService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }
  Platform_type = "dashboard";
  addCommentModal(id) {
    console.log(this.dataArr)
    if (this.dataArr.length == 0) {
      var comment_type = 'normal';
      console.log('commnet and id', this.model.comment, id);
      this.dashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type,this.Platform_type)
        .subscribe((response: any) => {
          //console.log('comment Response', response)
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.getPostDetailsById(id);
        })
    } else {
      console.log('taged section')
      var comment_type_tag = 'tag'
      this.dashboardService.commentTagPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type_tag, this.dataArr,this.Platform_type)
        .subscribe((response: any) => {
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.getPostDetailsById(id);
        })

    }
  }

  likePostModal(post_id) {
    return this.xploreService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      //console.log('like Data ', data);
      this.posts();
      this.getPostDetailsById(post_id);
    });

  }

  friendListData
  getFriendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        console.log('Friend List Response',response)
        this.friendListData = response.object;
      })
  }

  tagFriendsArr = []
  tagArrString
  searchText;
  onChangeCategoryEvent(event, data1 : any) { 

    if(event == true){
      this.tagFriendsArr.push(data1.id);
      this.tagArrString = JSON.stringify(this.tagFriendsArr);
      console.log(this.tagArrString)
    }else{
      this.tagFriendsArr.pop();
      this.tagArrString = JSON.stringify(this.tagFriendsArr);
      console.log(this.tagArrString)
     
    }
    
  }

  tagMember 
  getPostDetailsForTagView(id){
    console.log(id);
    this.dashboardService.getPostDetailsById(id).subscribe((response : any) =>{
      console.log(response);
      this.tagMember = response.object;
      console.log(this.tagMember)
    },err =>{
      console.log(err)
    }
    )
  } 
  
  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

}
