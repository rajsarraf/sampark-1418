import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpEventType, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Post } from '../dashboard/post';
import { AppSetting } from '../app.setting';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class XploreService {

  constructor(private http : HttpClient) { }

  postInXploreInterest(user_id, content, File, user_name, visibility,post_type,friendtag_arr,category,interest,playlist_arr): Observable<Post>{
    console.log(user_id, content, File, user_name, visibility,post_type,friendtag_arr,category,interest,playlist_arr)
    if (File != null) {
      //console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('user_name', user_name);
      formData.append('visibility', visibility);
      formData.append('post_type',post_type)
      formData.append('friendtag_arr',friendtag_arr)
      formData.append('category',category);
      formData.append('interest',interest);
      formData.append('playlist_arr',playlist_arr)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post_in_explode/', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
    } else {
      //console.log(user_name, visibility)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('user_name', user_name).set('visibility', visibility)
      .set('post_type',post_type).set('friendtag_arr',friendtag_arr).set('category',category).set('interest',interest).set('playlist_arr',playlist_arr)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post_in_explode/', parmas);
    }
  }
// post_in_explode  store_post
  createPostWithMetaDataInterest(user_id, content, user_name, visibility,url_title,metafile,url,metadata,post_type,friendtag_arr,category,interest): Observable<Post> {
    console.log('metadata post service',user_id, content, user_name, visibility,url_title,metafile,url,metadata,friendtag_arr)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('user_name', user_name).set('visibility', visibility)
      .set('url_title',url_title).set('metafile',metafile).set('url',url)
      .set('metadata',metadata).set('post_type',post_type).set('friendtag_arr',friendtag_arr).set('category',category).set('interest',interest);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post_in_explode/', parmas);
   // }

  }

  postInXplore(user_id, content, File, user_name, visibility,post_type,friendtag_arr): Observable<Post>{
    console.log(user_id, content, File, user_name, visibility,post_type,friendtag_arr)
    if (File != null) {
      //console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('user_name', user_name);
      formData.append('visibility', visibility);
      formData.append('post_type',post_type)
      formData.append('friendtag_arr',friendtag_arr)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_post/', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
    } else {
      //console.log(user_name, visibility)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('user_name', user_name).set('visibility', visibility)
      .set('post_type',post_type).set('friendtag_arr',friendtag_arr)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_post/', parmas);
    }
  }

  createPostWithMetaData(user_id, content, user_name, visibility,url_title,metafile,url,metadata,post_type,friendtag_arr): Observable<Post> {
    console.log('metadata post service',user_id, content, user_name, visibility,url_title,metafile,url,metadata,friendtag_arr)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('user_name', user_name).set('visibility', visibility)
      .set('url_title',url_title).set('metafile',metafile).set('url',url)
      .set('metadata',metadata).set('post_type',post_type).set('friendtag_arr',friendtag_arr);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_post/', parmas);
   // }

  }

  getInterestAndHobbiesPost(user_id,category,sub_category,post_type){
    const params = new HttpParams().set('user_id',user_id).set('category',category).set('sub_category',sub_category)
    .set('post_type',post_type)
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'explorepost_category',params)
  }


  getMetaData(key,q) : Observable<any>{
    const params = new HttpParams().set('key',key).set('q',q)
    return this.http.post('http://api.linkpreview.net/?',params)
    .pipe(map(data => data))
  }
  
  progressCount(id) {
    //console.log('count id', id)
    const params = new HttpParams().set('id', id);
    return this.http.post(AppSetting.API_ENDPOINT_JAVA + 'getprofilecount', params);
  }


  private getEventMessage(event: HttpEvent<any>, formData) {

    switch (event.type) {
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
        break;
      case HttpEventType.Response:
        return this.apiResponse(event);
        break;
      default:
        return `File "${formData.get('file').name}" surprising upload event: ${event.type}.`;
    }
  }

  private fileUploadProgress(event) {
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    return event.body;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened. Please try again later.');
  }

  getXplorePost(user_id,post_type){
    const params = new HttpParams().set('user_id',user_id).set('post_type',post_type);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'fetch_postdata',params)
  }

  getXplorePostPage(user_id,post_type, page): Observable<Post[]> {

    const params = new HttpParams().set('user_id', user_id).set('post_type',post_type).set('page', page);
    return this.http.post<Post[]>(AppSetting.API_ENDPOINT_PHP + 'fetch_postdata/', params);
  }

  commentPost(user_id, parent_id, comment, user_name, File,comment_type) {
    if (File == null) {
     // console.log('in comment srvice', user_id, parent_id, comment, user_name)
      const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
        .set('user_name', user_name).set('comment_type',comment_type);
      return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
    } else {
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('parent', parent_id);
      formData.append('user_id', user_id);
      formData.append('content', comment);
      formData.append('user_name', user_name);
      formData.append('comment_type',comment_type)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_comment/', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );

    }
  }

  commentTagPost(user_id, parent_id, comment, user_name, File,comment_type,friends_id){
    if (File == null) {
      // console.log('in comment srvice', user_id, parent_id, comment, user_name)
       const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
         .set('user_name', user_name).set('comment_type',comment_type).set('friends_id',friends_id);
       return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
     } else {
       const formData: FormData = new FormData();
       formData.append('file', File, File.name);
       formData.append('parent', parent_id);
       formData.append('user_id', user_id);
       formData.append('content', comment);
       formData.append('user_name', user_name);
       formData.append('comment_type',comment_type)
       formData.append('usertag_id',friends_id)
       return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_comment/', formData, {
         reportProgress: true,
         observe: 'events',
       }).pipe(
         map(event => this.getEventMessage(event, formData)),
         catchError(this.handleError)
       );
 
     }
  }


  deletePost(post_id) {
    //console.log('post delete in service', post_id)
    return this.http.delete(AppSetting.API_ENDPOINT_PHP + 'post/destory_post/' + post_id)
  }

  reportPost(post_id, complainer_userid, receiver_id) {
    //console.log("report", post_id, complainer_userid, receiver_id)
    const params = new HttpParams().set('complainer_userid', complainer_userid).set('receiver_id', receiver_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post/report?', params);

  }

  storeLike(user_id, post_id) {

    console.log(post_id)
    const params = new HttpParams().set('user_id', user_id).set('post_id', post_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'like/post', params);

  }

  getAdminTrendingPost(user_id) {
    const params = new HttpParams().set('user_id', user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'posts/admin_trendingpost', params);
  }

  getAdminTrendingPostPage(page,user_id) {
    const params = new HttpParams().set('page',page).set('user_id', user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'posts/admin_trendingpost', params);
  }

}
