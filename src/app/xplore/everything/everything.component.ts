import { Component, OnInit } from '@angular/core';
import { Link, NgxLinkifyjsService } from 'ngx-linkifyjs';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { XploreService } from '../xplore.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Socialusers } from 'src/app/home/user.model';
import { PeopleFriendService } from 'src/app/people-friend/people-friend.service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-everything',
  templateUrl: './everything.component.html',
  styleUrls: ['./everything.component.css']
})
export class EverythingComponent implements OnInit {

  socialusers: Socialusers;
  user: any;
  status = false;
  CloseBtn: boolean = true;
  post: any;
  postObj;
  model: any = { comment: '' };
  editPostModel: any = {};
  fileToUpload;
  fileUpload: any = { status: '', message: '', filePath: '' };
  error: string;
  iFlag: boolean = false;
  message: string = "";
  status_id: number;
  privatepost;
  searchmodel: any = {};
  notscrolly = true;
  notEmptyPost = true;
  comment_message: string = "";
  comment_img;
  imgURL: any = "";
  
  selectedItems = { id: "", groupname: "" };
  dropdownSettings = {};

  constructor(private xploreService:XploreService,public linkifyService: NgxLinkifyjsService,private spinner: NgxSpinnerService,
    private router : Router,private toastr:ToastrService,private peopleFriendService: PeopleFriendService,
    private dashboardService : DashboardService) { }


  postForm = new FormGroup({

    content: new FormControl(),

  })

  ngOnInit() {

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.posts();
    var videos = document.getElementsByTagName("video");

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      enableCheckAll: false,
      // selectAllText: '',
      // unSelectAllText: '',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.getFriendList();
    
  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  max_size = 104857600
  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload)
  }


  linkInfo
  key
  q
  metadata = 1
  post_type = 4
  store() {

    var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi

    if(this.tagArrString == undefined){
      console.log("undefined array");
      this.tagArrString = [];
    }
    var str = this.postForm.value.content;
    //console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    //console.log('New Words', newContent)

    const foundLinks: Link[] = this.linkifyService.find(newContent);
    console.log('link found', foundLinks)
    if (foundLinks.length != 0 && this.fileToUpload == null) {
      for (let link of foundLinks) {
        
        console.log(link.value);
        this.key = '1536acb288c438aae1c7084f769c988e';
        this.q = link.value;
        this.xploreService.getMetaData(this.key, this.q).subscribe((response: any) => {
          this.linkInfo = response;
          console.log(this.linkInfo);
          if (this.privatepost == null) {
            this.privatepost = 0;
            this.status = false;
            //let post = this.postForm.value;
            //console.log('metadata post',this.linkInfo.description)
            this.xploreService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.privatepost, this.linkInfo.title,
              this.linkInfo.image, this.linkInfo.url, this.metadata,this.post_type,this.tagArrString).subscribe(

                post => {
                  this.fileUpload = post;
                  this.iFlag = false;
                  console.log('response from server', post);
                  //this.fileUpload = post,
                  this.status = true;
                  this.emojiFlag = false;
                  // this.fileToUpload ='';
                  // this.privatepost;
                  this.postForm.reset();
                   this.posts();
                   this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
                    this.router.navigate(['/navigation/everything']);
                }); 
                },
                err => this.error = err
              );
          } else {
            this.status = false;
            //let post = this.postForm.value;
            this.xploreService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.privatepost, this.linkInfo.title,
              this.linkInfo.image, this.linkInfo.url, this.metadata,this.post_type,this.tagArrString).subscribe(

                post => {
                  this.fileUpload = post;
                  this.iFlag = false;
                  console.log('response from server', post);
                  //this.fileUpload = post,
                  this.status = true;
                  // this.fileToUpload ='';
                  this.emojiFlag = false;
                  this.privatepost;
                  this.postForm.reset();
                   this.posts();
                   this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
                    this.router.navigate(['/navigation/everything']);
                }); 
                },
                err => this.error = err
              );
          }
        },err => {
          console.log('error response section',err);
          if (this.privatepost == null) {
            this.privatepost = 0;
            this.status = false;
            let post = this.postForm.value;
            this.xploreService.postInXplore(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost
              ,this.post_type,this.tagArrString).subscribe(
    
              post => {
                this.fileUpload = post;
                this.iFlag = false;
                 console.log('response from server',post);
                //this.fileUpload = post,
                this.status = true;
                this.emojiFlag = false;
                // this.fileToUpload ='';
                // this.privatepost;
                this.postForm.reset();
                 this.posts();
                 this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
                  this.router.navigate(['/navigation/everything']);
              }); 
              },
              err => this.error = err
            );
          } else {
            this.status = false;
            let post = this.postForm.value;
            this.xploreService.postInXplore(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost,
              this.post_type,this.tagArrString).subscribe(
    
              post => {
                this.fileUpload = post;
                this.iFlag = false;
                 console.log('response from server',post);
                //this.fileUpload = post,
                this.status = true;
                // this.fileToUpload ='';
                this.emojiFlag = false;
                this.privatepost;
                this.postForm.reset();
                this.posts();
                this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
                  this.router.navigate(['/navigation/everything']);
              }); 
              },
              err => this.error = err
            );
          }
    

        })
      }
    } else {
      if(this.fileToUpload != undefined && this.fileToUpload.size > this.max_size){
        this.toastr.warning('Maximum size allowed is 100MB')
     }else{
      if (this.privatepost == null) {
        this.privatepost = 0;
        this.status = false;
        let post = this.postForm.value;
        this.xploreService.postInXplore(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost,
          this.post_type,this.tagArrString).subscribe(

          post => {
            this.fileUpload = post;
            this.iFlag = false;
             console.log('response from server',post);
            //this.fileUpload = post,
            this.status = true;
            this.emojiFlag = false;
            // this.fileToUpload ='';
            // this.privatepost;
            this.postForm.reset();
            this.posts();
            this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/navigation/everything']);
          }); 
          },
          err => this.error = err
        );
      } else {
        this.status = false;
        let post = this.postForm.value;
        this.xploreService.postInXplore(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost,
          this.post_type,this.tagArrString).subscribe(

          post => {
            this.fileUpload = post;
            this.iFlag = false;
             console.log('response from server',post);
            //this.fileUpload = post,
            this.status = true;
            // this.fileToUpload ='';
            this.emojiFlag = false;
            this.privatepost;
            this.postForm.reset();
            this.posts();
            this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/navigation/everything']);
          }); 
          },
          err => this.error = err
        );
      }
     }
    
    }
  }

  // getAllPost(){
  //   this.xploreService.getXplorePost(this.socialusers.id,this.post_type)
  //   .subscribe((response : any)=>{
  //     console.log(response)
  //   },err =>{
  //     console.log(err)
  //   }
  //   )
  // }

   // get all posts
   page: number = 1;
   showMore = false;
   showChar = 200;
   link = []
   allPost
   posts() {
 
     //console.log('page count', this.page)
 
     this.xploreService.getXplorePost(this.socialusers.id,this.post_type).subscribe((data: any) => {
       this.post = data
       console.log('post response', this.post.object.data);
       this.allPost = this.post.object.data;
     },err =>{
       console.log(err)
     }
     );
   }
 
 
   scrollcount
 
   onScrollDown() {
     console.log('scrolling...................', this.page)
     if (this.notscrolly && this.notEmptyPost) {
       const start = this.page;
       this.page += 1;
       this.spinner.show();
       this.notscrolly = false;
       // console.log('page.................', this.page)
       this.xploreService.getXplorePostPage(this.socialusers.id,this.post_type, this.page)
         .subscribe((response: any) => {
           const newPost = response.object.data;
           this.spinner.hide();
           this.notEmptyPost = false;
           // console.log('page 2 response',response);
           if (newPost != null) {
             this.allPost = this.allPost.concat(newPost)
             this.notscrolly = true;
             //this.notEmptyPost = false;
           } else {
             this.notscrolly = false;
             this.notEmptyPost = false;
           }
         })
     }
 
   }

   addComment(id) {
    console.log(this.dataArr)
        if(this.dataArr.length == 0){
          var comment_type = 'normal';
          console.log('commnet and id', this.model.comment, id);
          this.xploreService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload,comment_type)
            .subscribe((response: any) => {
              //console.log('comment Response', response)
              this.status = true;
              this.model = {};
              this.commentEmoji = false;
              this.posts();
              this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
                this.router.navigate(['/navigation/everything']);
            }); 
            })
        }else{
          console.log('taged section')
          var comment_type_tag = 'tag'
          this.xploreService.commentTagPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload,comment_type_tag,this.dataArr)
          .subscribe((response : any)=>{
            this.status = true;
            this.model = {};
            this.commentEmoji = false;
            this.posts();
            this.router.navigateByUrl('/navigation/everything', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/navigation/everything']);
          }); 
          })
       
        }
    
      }
 
   div_id;
   triggerUpload(id) {
     this.div_id = id;
     // console.log('this is div_id', this.div_id);
     var comment_id = 'comment_file' + id;
     $("#comment_file" + id).click();
   }
 
   imagePath: FileList;
   createCommentImage(files: FileList) {
     var reader = new FileReader();
     this.imagePath = files;
     this.fileToUpload = files.item(0);
     //console.log(this.fileToUpload);
     this.comment_img = files;
     reader.readAsDataURL(files[0]);
     reader.onload = (_event) => {
       this.imgURL = reader.result;
     }
 
   }

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }
  handleSelection($event) {
    //console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }

  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    //console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }

  handleComment($event) {
    //console.log($event);
    if (this.status_id != null) {
      this.model.comment += $event.emoji.native;
    }
    //this.selectedEmoji = $event.emoji;

  }

  //delete post

  deletePost(id) {
    this.spinner.show()
    //console.log(id)
    this.xploreService.deletePost(id)
      .subscribe((response: any) => {
        // console.log('Delete Post Response', response);
        this.posts();
        this.spinner.hide();
        this.toastr.success('post deleted successfully');
      })
  }

  //Report post

  reportPost(id, receiver_id) {

    // console.log(id, this.socialusers.id, receiver_id)
    this.xploreService.reportPost(id, this.socialusers.id, receiver_id)
      .subscribe((response => {
        // console.log('response', response);
        // console.log('report Added successfully');
        this.toastr.success('Report Added');
        this.posts();
      }))


  }


  likePost(post_id) {
    //console.log('like'+post_id)
    this.xploreService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      //console.log('like Data ', data);
      this.posts();
      
    });

  }

  input_id 
  focusFunction(id){
    console.log('focus function',id)
    this.input_id = id;
  }
  
  tagSection : boolean = false;
  friendstatus = 1;
  friendsList = [];
  friend_type = 0;
  dropdownList = [];
  doSomething(event){
    console.log(event)
    var compare_text = event
    var search_value=new Array("@");
    for(var i=0; i<search_value.length; i++)
   {
    for(var j=0; j<(compare_text.length); j++)
    {
      if(search_value[i]==compare_text.substring(j,(j+search_value[i].length)).toLowerCase())
      {
        console.log('tag section');
        this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
          .subscribe((response: any) => {
            console.log('Friends List', response);
            if (response != null) {
              this.friendsList = response.object;
              this.dropdownList = response.object;
              this.tagSection = true
              //this.popUpOpen();
            } else {
              this.toastr.error('something went wrong');
            }
          })
      }else{
        //console.log('Comment Section')
      }
    }
  
   }
  
   
  }
  
  popUpOpen(){
    console.log('popup');
    ($('#tagModal') as any).modal('show');
  }
  
  dataArr = []
    myArrStr
    concvertData
    onItemSelect(item: any) {
      this.tagSection = false;
      console.log('item', item);
      this.dataArr.push(item.id);
      this.model.comment += item.name
      //this.myArrStr = JSON.stringify(this.dataArr);
      console.log('my final array', this.dataArr);
    }
  
    onDeSelect(item: any) {
      this.tagSection = false;
      this.dataArr.pop();
      console.log('Deselect item', item)
    }

    postData = []

  getPostDetailsById(id) {
     console.log(id);
    this.dashboardService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }
  Platform_type = "dashboard";

  addCommentModal(id) {
    console.log(this.dataArr)
        if(this.dataArr.length == 0){
          var comment_type = 'normal';
          console.log('commnet and id', this.model.comment, id);
          this.dashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload,comment_type,this.Platform_type)
            .subscribe((response: any) => {
              //console.log('comment Response', response)
              this.status = true;
              this.model = {};
              this.commentEmoji = false;
              this.posts();
              this.getPostDetailsById(id);
            })
        }else{
          console.log('taged section')
          var comment_type_tag = 'tag'
          this.dashboardService.commentTagPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload,comment_type_tag,this.dataArr,this.Platform_type)
          .subscribe((response : any)=>{
            this.status = true;
            this.model = {};
            this.commentEmoji = false;
            this.posts();
            this.getPostDetailsById(id);
          })
       
        }
    
      }

      likePostModal(post_id){
        return this.xploreService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
          //console.log('like Data ', data);
          this.posts();
          this.getPostDetailsById(post_id);
        });
    
      }

      friendListData
  getFriendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        console.log('Friend List Response',response)
        this.friendListData = response.object;
      })
  }

  tagFriendsArr = []
  tagArrString
  searchText;
  onChangeCategoryEvent(event, data1 : any) { 

    if(event == true){
      this.tagFriendsArr.push(data1.id);
      this.tagArrString = JSON.stringify(this.tagFriendsArr);
      console.log(this.tagArrString)
    }else{
      this.tagFriendsArr.pop();
      this.tagArrString = JSON.stringify(this.tagFriendsArr);
      console.log(this.tagArrString)
     
    }
    
  }
  tagMember 
  getPostDetailsForTagView(id){
    console.log(id);
    this.dashboardService.getPostDetailsById(id).subscribe((response : any) =>{
      console.log(response);
      this.tagMember = response.object;
      console.log(this.tagMember)
    },err =>{
      console.log(err)
    }
    )
  }   
  

}
