import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Socialusers } from '../home/user.model';
import { UpdateprofileService } from './updateprofile.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-updateprofile',
  templateUrl: './updateprofile.component.html',
  styleUrls: ['./updateprofile.component.css']
})
export class UpdateprofileComponent implements OnInit {


  imageUrl = "assets/img/avtar.png"
  fileToUpload: File = null;
  socialusers: Socialusers;
  users
  model: any = {};
  updatemodel: any = {}
  achivement = [];
  subjectValue: any = {};
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  aadharNumberPattern = "^([0-9]{12,18})$";
  licencePattern = "^[0-3][0-9]{7}$";
  dropdownList = [];
  selectedItems = { id: "", groupname: "" };
  dropdownSettings = {};
  emergencyModel: any = {};

  constructor(private updateProfileService: UpdateprofileService,
    private router: Router, private toastr: ToastrService) { }

  ngOnInit() {


    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'subjectname',
      enableCheckAll: false,
      // selectAllText: '',
      // unSelectAllText: '',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    $(".dropdown dt a").on('click', function () {
      $(".dropdown dd ul").slideToggle('fast');
    });

    $(".dropdown dd ul li a").on('click', function () {
      $(".dropdown dd ul").hide();
    });

    function getSelectedValue(id) {
      return $("#" + id).find("dt a span.value").html();
    }

    $(document).bind('click', function (e) {
      var $clicked = $(e.target);
      if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
    });

    $("input[type='image']").click(function () {
      $("input[id='File']").click();


    });


    this.getCategoryList();

    // $("input[type='image']").click(function () {
    //   $("input[id='File']").click();
    // })

    // $("#upfile1").click(function () {
    //   $("#file1").trigger('click');
    // });

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));

    //console.log('update profile Data', this.model);

    this.getUserDetails();
    this.getAllStateList();
    this.stateForPersonalDetails();
    this.getSubjectList();
    //this.getOrganisationList();
    this.getHobbiesCategoryList();
    this.getEmergencyNumbers();
   // this.getAllAvtarImage();
    this.getAllSchoolList();

  
  }



  handleFileInput(file: FileList) {
    window.document.getElementById("uploadModal").click();
    this.fileToUpload = file.item(0);

    console.log('clicked')
    //Show image Preview

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);

    this.updateProfileService.profileImageUpload(this.socialusers.id, this.fileToUpload)
      .subscribe((response: any) => {
        console.log('Image Uplaoad Successfully');
        this.toastr.success('Image has been uploaded successfully');
        this.getUserDetails();
      })

  }

  accountPrivacy(value) {
    console.log('Account Privacy Value', value);

    Swal.fire({
      title: 'Are you sure?',
      text: "You Want To Change Your Account Privacy",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.updateProfileService.setAccountPrivacy(this.socialusers.id, value)
          .subscribe((response: any) => {
            console.log('Account Privacy Change Response', response);
            // this.toastr.success('Message Sent successfully');
            this.getUserDetails();
            this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
          })
        Swal.fire(
          'Sent!',
          'Your Account Privacy Changed Successfully',
          'success'
        )
      }
    })

  }

  publicProfile() {
    let value = 0;
    Swal.fire({
      title: 'Are you sure?',
      text: "You Want To Make Your Account Public",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.updateProfileService.setAccountPrivacy(this.socialusers.id, value)
          .subscribe((response: any) => {
            console.log('Account Privacy Change Response', response);
            this.getUserDetails();
            this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
            // this.toastr.success('Message Sent successfully');
          })
        Swal.fire(
          'Sent!',
          'Your Account Privacy Changed Successfully',
          'success'
        )
      }
    })
  }

  privateProfile() {
    let value = 1;
    Swal.fire({
      title: 'Are you sure?',
      text: "You Want To Make Your Account Private",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.updateProfileService.setAccountPrivacy(this.socialusers.id, value)
          .subscribe((response: any) => {
            console.log('Account Privacy Change Response', response);
            this.getUserDetails();
            this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
            // this.toastr.success('Message Sent successfully');
          })
        Swal.fire(
          'Sent!',
          'Your Account Privacy Changed Successfully',
          'success'
        )
      }
    })
  }

  getUserDetails() {
    this.updateProfileService.getUserDetails(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.model = response.object;
        this.onChageValue(this.model.stateid);
        this.onChangeSchoolValue(this.model.orgstateid)
      })
  }

  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.updateProfileService.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
        console.log('interest category list response', response);
        this.categoryList = response.object;
      })
  }
  interestCategoryid;
  subCaterory = [];
  changeCategoryListValue(id) {
    this.interestCategoryid = id
    console.log('Sub Category List', id)
    this.updateProfileService.getIntrestList(id)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;
      })
  }

  hobbiesList = [];
  statusHobbies = 0;
  getHobbiesCategoryList() {
    this.updateProfileService.getHobbiesCategoryList(this.statusHobbies)
      .subscribe((response: any) => {
        console.log('Hobbies category list response', response);
        this.hobbiesList = response.object;
      })
  }

  hobbiesCategoryId
  hobbiesIdArray = [];
  subCateroryHobbies = [];
  // changeCategoryHobbiesList(id) {
  //   console.log('hobbies id',id)
  //   this.hobbiesCategoryId = id;
  //   console.log('hobbies array',this.hobiesIdArray)
  //   console.log('Sub Category List', id)
  //   this.updateProfileService.getHobbiesList(id)
  //     .subscribe((response: any) => {
  //       console.log(response);
  //       this.subCateroryHobbies = response.object;
  //     })
  // }
  hobbyCategoryIdStr
  changeCategoryHobbiesList(event, hobbies: any){
    console.log('select hobbies',hobbies)
    this.hobbiesCategoryId = hobbies.id;
    console.log(event)
    if(event == true){
      this.hobbiesIdArray.push(hobbies.id);
      this.hobbyCategoryIdStr = JSON.stringify(this.hobbiesIdArray)
      console.log('hobbies id array',this.hobbiesIdArray);
    }else{
      this.hobbiesIdArray.pop()
    console.log('hobbies id array pop',this.hobbiesIdArray)
    }
    this.updateProfileService.getHobbiesList(hobbies.id)
      .subscribe((response: any) => {
        console.log(response);
        this.subCateroryHobbies = response.object;
      })
  }

  InterestCategoryId
  interestIdArray = []
  changeCategoryInterestList(event, interestList: any){

    console.log('select hobbies',interestList)
    this.InterestCategoryId = interestList.id;
    console.log(event)
    if(event == true){
      this.interestIdArray.push(interestList.id);
      this.hobbyCategoryIdStr = JSON.stringify(this.interestIdArray)
      console.log('hobbies id array',this.interestIdArray);
    }else{
      this.interestIdArray.pop()
    console.log('hobbies id array pop',this.interestIdArray)
    }
    this.updateProfileService.getIntrestList(interestList.id)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;
      })
  }

  interestArr = []
  interestArr1 = []
  interestArr2

  //interestArr: any = { "interestSubCategory": [] };

  onChangeCategoryEvent(event, subCategoryList: any) {
    
    this.interestArr.push(subCategoryList.id);
    console.log(this.interestArr)

    if(event == true){
      console.log(subCategoryList)
      console.log(this.hobbiesCategoryId)
      this.interestArr.push(subCategoryList.id);
      this.myArrStr = JSON.stringify(this.interestArr)
      console.log(this.interestArr)
      console.log('array string',this.myArrStr)
      console.log(this.interestArr)
      var Obj = { }; 
      var arraynew = subCategoryList.id;
      Obj[this.InterestCategoryId]=arraynew; 
      console.log('ghdfjdghdfgh',Obj); 
      this.interestArr1.push(Obj);
      this.interestArr2 = JSON.stringify(this.interestArr1)
      console.log('Final array',this.interestArr2)
     }else{
       this.interestArr.pop()
       this.interestArr1.pop();
       console.log('Final array',this.interestArr1)
     }

  }


  hobbiesArr = [];
  arr = [];
  arr1 
  //hobbiesArr : any = { "hobbiesSubCategory" : [] };
  onChangeCategoryHobbies(event, hobbiesSubList: any) {
   if(event == true){
    console.log(hobbiesSubList)
    console.log(this.hobbiesCategoryId)
    this.hobbiesArr.push(hobbiesSubList.id);
    this.myArrStr = JSON.stringify(this.hobbiesArr)
    console.log(this.hobbiesArr)
    console.log('array string',this.myArrStr)
    console.log(this.hobbiesArr)
    var Obj = { }; 
    var arraynew = hobbiesSubList.id;
    Obj[this.hobbiesCategoryId]=arraynew; 
    console.log('ghdfjdghdfgh',Obj); 
    this.arr.push(Obj);
    this.arr1 = JSON.stringify(this.arr)
    console.log('Final array',this.arr1)
   }else{
     this.hobbiesArr.pop()
     this.arr.pop();
     console.log('Final array',this.arr)
   }
  }

  dataArr = []
  myArrStr
  concvertData
  onItemSelect(item: any) {
    console.log('item', item);
    this.dataArr.push(item.id);
    //this.myArrStr = JSON.stringify(this.dataArr);
    console.log('my final array', this.dataArr);
  }

  onDeSelect(item: any) {
    this.dataArr.pop();
    console.log('Deselect item', item)
  }
  // onSelectAll(items: any) {
  //   console.log(items)
  //    this.myArrStr = JSON.stringify(items);
  //   console.log('select all section',this.myArrStr);
  // }



  updateInterestAndHobbies() {
    console.log('Ts hit successfully',this.socialusers.id, this.interestArr2,
       this.hobbiesIdArray, this.hobbiesArr, this.dataArr)
    this.updateProfileService.updateUsersInterestAndHobbiesTest(this.socialusers.id, this.interestArr2,
       this.arr1, this.dataArr)
      .subscribe((response: any) => {
        console.log('update interest and Hobbies Response from Server', response);
      })
  }


  sosNumberArr = [];
  updateEmergencyNumbers() {
    console.log('Emergency Numbers Details', this.emergencyModel);
    this.sosNumberArr.push(this.emergencyModel.phone_number1, this.emergencyModel.phone_number2, this.emergencyModel.phone_number3, this.emergencyModel.phone_number4,
      this.emergencyModel.phone_number5);
    console.log('Sos Number Array', this.sosNumberArr)
    this.updateProfileService.saveEmergencyNumbers(this.socialusers.id, this.sosNumberArr)
      .subscribe((response: any) => {
        console.log('save emergency number response', response);
      })
  }

  getEmergencyNumbers() {
    this.updateProfileService.getEmergencyNumbers(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('Emergency Number Response', response.object);
        this.emergencyModel = response.object;
        // for(let data of response.object){
        //   this.emergencyModel.phone1=data.phone_number
        //   this.emergencyModel.phone2=data.phone_number
        //   console.log('data in loop',data);
        // }
      })
  }


  profileUpdate() {
    this.updateEmergencyNumbers();
    console.log(this.model.ogrpincode)
    this.updateInterestAndHobbies();
    console.log('Interest and hobbies List Array in profile Update',this.interestCategoryid,this.interestArr,
    this.hobbiesIdArray,this.arr);
    console.log('dropdown model in profile update', this.model.stateid, this.model.cityid, this.model.schoolname,
      this.model.schooladdress, this.model.classname, this.model.section, this.model.teachername)
    console.log('model in update profile', this.model)
    this.updateProfileService.updateUserProfile(this.model)
      .subscribe((response: any) => {
        if (response != null) {
          this.toastr.success('Profile has been updated successfully');
          //console.log('profile updated Successfully');
          console.log(response);
          this.model = {};
          this.router.navigate(['/navigation/dashboard']);
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  // state list
  stateList = [];
  getAllStateList() {
    this.updateProfileService.stateList().subscribe((response: any) => {
      console.log('state list in respone', response);
      this.stateList = response;
      console.log('school details ',response)
    })
  }

  //city List

  cityList = [];

  cityForSchoolDetails(value) {
    console.log(value)
    this.updateProfileService.cityListById(value)
      .subscribe((response: any) => {
        console.log('city list', response)
        this.cityList = response;
      })
  }

  personalDetailsState = [];

  stateForPersonalDetails() {
    this.updateProfileService.stateList().subscribe((response: any) => {
      this.personalDetailsState = response;
    })
  }

  personalDetailsCity = [];

  cityForPersonalDetails(value) {
    console.log(value)
    this.updateProfileService.cityListById(value)
      .subscribe((response: any) => {
        this.personalDetailsCity = response;
        console.log(response)
      })
  }

  onChageValue(id){
    console.log('on change Value',id);
    if(id != null && id != 0){
      this.updateProfileService.cityListById(id)
      .subscribe((response: any) => {
        this.personalDetailsCity = response;
        console.log(response)
      })
    }
  }

  onChangeSchoolValue(id){
    console.log('on change Value school',id);
    if(id != null && id != 0){
      this.updateProfileService.cityListById(id)
      .subscribe((response: any) => {
        this.cityList = response;
        console.log('school data response',response)
      })
    }
  }

  subjectList = []

  getSubjectList() {
    this.updateProfileService.getSubjectList().subscribe((response: any) => {
      console.log('Subject List', response);
      //this.subjectList = response.object;
      this.dropdownList = response.object;
    })
  }

  organisationList = [];
  getOrganisationList() {
    this.updateProfileService.getOrganisationList().subscribe((response: any) => {
      console.log('Organisation List', response);
      this.organisationList = response;
    })
  }

  avtarImage = []
  getAllAvtarImage(){
    window.document.getElementById("uploadModal").click();
    this.updateProfileService.getAvtarList().subscribe((response : any)=>{
      console.log(response);
      if(response != null){
        this.avtarImage = response;
      }else{
        console.log('something went wrong')
      }
    })
  }
chooseAvtar(id){ 
  this.updateProfileService.saveAvtarImage(this.socialusers.id,id)
  .subscribe((response : any)=>{
    console.log('Avtar save response from server',response)
    if(response.object != null){
      window.document.getElementById("avtarModal").click();
      this.toastr.success('Image Upload Successfully')
      this.router.navigateByUrl('/navigation/updateProfile', { skipLocationChange: true }).then(() => {
        this.router.navigate(['/navigation/updateProfile']);
    }); 
    }else{
      this.toastr.warning('Something Went Wrong')
    }
  },err =>{
    console.log(err)
  }
  )

}

schoolList
getAllSchoolList(){
  this.updateProfileService.getAllSchoolList().subscribe((response :any)=>{
    console.log('all school list response',response);
    this.schoolList = response.object;
  })
}

selectSchoolName(value){
console.log(value);
}

  panField: boolean = false;
  aadharField: boolean = false;
  voterCardField: boolean = false;
  LicenceField: boolean = false;
  schoolIdField: boolean = false;
  defaultFlag: boolean = true;
  idChange(value) {
    this.defaultFlag = false;
    console.log(value)
    if (value == "PAN Card") {
      this.panField = true;
      this.aadharField = false;
      this.voterCardField = false;
      this.schoolIdField = false;
      this.LicenceField = false;
      this.model.idproofno = null;
    } else if (value == "Aadhaar Card") {
      this.aadharField = true;
      this.voterCardField = false;
      this.schoolIdField = false;
      this.LicenceField = false;
      this.panField = false;
      this.model.idproofno = null;
    } else if (value == "voterId") {
      this.voterCardField = true;
      this.schoolIdField = false;
      this.LicenceField = false;
      this.panField = false;
      this.aadharField = false;
      this.model.idproofno = null;
    }
    else if (value == "Licence") {
      this.LicenceField = true;
      this.panField = false;
      this.aadharField = false;
      this.voterCardField = false;
      this.schoolIdField = false;
      this.model.idproofno = null;
    }
    else if (value == "School Id Card") {
      this.schoolIdField = true;
      this.LicenceField = false;
      this.panField = false;
      this.aadharField = false;
      this.voterCardField = false;
      this.model.idproofno = null;
    }
  }

}
