import {Component, Input, OnInit} from '@angular/core';
import {Helpers} from 'src/app/helper/helpers';
import {CONSTANTS} from 'src/app/QBconfig';
import { ChatDemoService } from '../chat-demo.service';
import { DialogService } from '../dialogs/dialog.service';
import { MessageService } from '../messages/message.service';
import { HomeService } from 'src/app/home/home.service';
import { PeopleFriendService } from 'src/app/people-friend/people-friend.service';
import { Socialusers } from 'src/app/home/user.model';

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.css']
})
export class CreateDialogComponent implements OnInit {

  @Input() dialog: any;

  public loggedinUser: any;
  public userName: string;
  public users: any = [];
  public selectedUsers: number[] = [];
  public selecteUserName;
  public helpers: Helpers;
  public _usersCache: any;
  public messageField = '';
  socialusers: Socialusers;

  constructor(
    private dashboardService: ChatDemoService,
    public dialogService: DialogService,
    private messageService: MessageService,
    private userService: HomeService,
    private peopleFriendService : PeopleFriendService) {
    this.helpers = Helpers;
    this._usersCache = this.userService._usersCache;
    this.userService.usersCacheEvent.subscribe((usersCache: Object) => {
      this._usersCache = usersCache;
    });
  }

  ngOnInit() {
    console.log('ngOnInit');
    // this.getUserList('');
    this.loggedinUser = this.userService.chatUser;
    console.log('logiiedinUser in chat boc creation',this.loggedinUser)
    this.selectedUsers.push(this.loggedinUser.id);
    console.log('seleced id for chat dialog creation',this.selectedUsers)
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.friendList();
  }

  toggleSelectItem(userId: number,name:string) {
    console.log(userId,name)
    const index = this.selectedUsers.indexOf(userId);
     this.selecteUserName = name;
    if (this.loggedinUser.id === userId) {
      return false;
    }
    if (index >= 0) {
      this.selectedUsers.splice(index, 1);
    } else {
      this.selectedUsers.push(userId);
    }
  }

  goBack() {
    this.dashboardService.showComponent({
      'createGroupClicked': false,
      'updateDialog': false,
      'onChatClick': !this.dashboardService.components.welcomeChat
    });
  }

  // getUserList(args) {
  //   this.userService.getUserList(args).then((users) => {
  //     this.users = users;
  //     console.log('users list for chat',this.users)
  //   }).catch((err) => {
  //     console.log('Get User List Error: ', err);
  //   });
  // }

  friendstatus = 1;
  requestList = [];
  friendsList = [];
  friend_type = 0;
  friendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        console.log('Friends List', response);
        if (response != null) {
          this.users = response.object;
          this.friendsList = response.object;
        } else {
          // this.toastr.error('something went wrong');
        }
      })
  }

  public onSubmit() {
    const self = this;
    const type = this.selectedUsers.length > 2 ? 2 : 3;
    const params = {
      type: type,
      occupants_ids: this.selectedUsers.join(',')
    };

    let name = '';

    if (type === 2) {
      const userNames = this.users.filter((array) => {
        return self.selectedUsers.indexOf(array.id) !== -1 && array.id !== this.loggedinUser.id;
      }).map((array) => {
        return array.full_name;
      });
      name = userNames.join(', ');
    }

    if (this.messageField) {
      name = this.messageField;
    }

    if (type !== 3 && name) {
      params['name'] = this.selecteUserName;
    }

    this.dialogService.createDialog(params).then(dialog => {
      const
        occupantsNames = [];
      let messageBody = this.userService.chatUser.name + ' created new Group with: ';
      dialog['occupants_ids'].forEach(userId => {
        occupantsNames.push(this.selecteUserName);
      });

      messageBody += occupantsNames.join(', ');

      const
        systemMessage = {
          extension: {
            notification_type: 1,
            dialog_id: dialog._id
          }
        },
        notificationMessage = {
          type: 'groupchat',
          body: messageBody,
          extension: {
            save_to_history: 1,
            dialog_id: dialog._id,
            notification_type: 1,
            date_sent: Date.now()
          }
        };

      (new Promise(function (resolve) {
        if (dialog.xmpp_room_jid) {
          self.dialogService.joinToDialog(dialog).then(() => {
            if (dialog.type === CONSTANTS.DIALOG_TYPES.GROUPCHAT) {
              const
                message = self.messageService.sendMessage(dialog, notificationMessage),
                newMessage = self.messageService.fillNewMessageParams(self.userService.chatUser.id, message);
              self.dialogService.dialogs[dialog._id] = dialog;
              self.dialogService.setDialogParams(newMessage);
              self.messageService.messages.push(newMessage);
              self.messageService.addMessageToDatesIds(newMessage);
              self.messageService.messagesEvent.emit(self.messageService.datesIds);
            }
            resolve();
          });
        }
        resolve();
      })).then(() => {

        const userIds = dialog.occupants_ids.filter((userId) => {
          return userId !== self.userService.chatUser.id;
        });
        self.messageService.sendSystemMessage(userIds, systemMessage);
        if (self.dialogService.dialogs[dialog._id] === undefined) {
          const tmpObj = {};
          tmpObj[dialog._id] = dialog;
          self.dialogService.dialogs = Object.assign(tmpObj, self.dialogService.dialogs);
          self.dialogService.dialogsEvent.emit(self.dialogService.dialogs);
        }

        this.dialogService.currentDialog = dialog;
        this.dialogService.currentDialogEvent.emit(dialog);
        this.dashboardService.showComponent({
          'createGroupClicked': false,
          'updateDialog': false,
          'welcomeChat': false,
          'onChatClick': true
        });
      });
    });
  }

}
