import { TestBed } from '@angular/core/testing';

import { ChatDemoService } from './chat-demo.service';

describe('ChatDemoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChatDemoService = TestBed.get(ChatDemoService);
    expect(service).toBeTruthy();
  });
});
