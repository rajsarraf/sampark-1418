import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { AppSetting } from '../app.setting';
import { Post } from '../dashboard/post';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GroupdashboardService {

  private groupDetailsUrl: string = "http://social.nuagedigitech.com/api/group/fetch_group_details"
  private groupPostUrl: string = "http://social.nuagedigitech.com/api/group/post_in_group"

  post_id: number;

  constructor(private http: HttpClient) { }

  // get Group By id

  getGroupDetails(groupid) {
    const params = new HttpParams().set('groupid', groupid);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/fetch_group_details', params);
  }
  getGroupDetailsPage(groupid,page) {
    const params = new HttpParams().set('groupid', groupid).set('page',page)
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/fetch_group_details', params);
  }

  addFriendInGroupRequest(groupdetailsid, user_id, status) {
    const params = new HttpParams().set('groupdetailsid', groupdetailsid).set('user_id', user_id)
      .set('status', status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_add_member_request', params);
  }

  makeGroupAdmin(user_id, groupdetailsid, is_admin) {
    const params = new HttpParams().set('user_id', user_id).set('groupdetailsid', groupdetailsid)
      .set('is_admin', is_admin)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_create_admin', params);
  }

  removeGroupMember(user_id, groupdetailsid) {
    const params = new HttpParams().set('user_id', user_id).set('groupdetailsid', groupdetailsid);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_remove_member', params)
  }

  blockGroupMember(user_id, groupdetailsid,status){
    const params = new HttpParams().set('user_id', user_id).set('groupdetailsid', groupdetailsid)
      .set('status', status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/block_member',params)
  }


  createPost(user_id, content, group_id, File, user_name): Observable<Post> {
    console.log('post service', content, user_id, user_name);
    if (File != null) {
      console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('group_id', group_id);
      formData.append('user_name', user_name);
     


      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', formData)
    } else {
      console.log(user_name)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('group_id', group_id).set('user_name', user_name)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', parmas);
    }

  }

  createPostWithMetaData(user_id, content, user_name,url_title,metafile,url,metadata,group_id): Observable<Post> {
    console.log('metadata post service',user_id, content, user_name,url_title,metafile,url,metadata)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('user_name', user_name)
      .set('url_title',url_title).set('metafile',metafile).set('url',url)
      .set('metadata',metadata).set('group_id',group_id);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_post/', parmas);
   // }

  }

  commentPost(user_id, parent_id, comment, user_name, File) {
    if (File == null) {
      console.log('in comment srvice', user_id, parent_id, comment, user_name)
      const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
        .set('user_name', user_name);
      return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
    } else {
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('parent', parent_id);
      formData.append('user_id', user_id);
      formData.append('content', comment);
      formData.append('user_name', user_name);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post/', formData, {
        reportProgress: true,
        observe: 'events',
      })

    }
  }

  storeLike(user_id, post_id) {

    console.log(post_id)

    const params = new HttpParams().set('user_id', user_id).set('post_id', post_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'like/post', params);

  }

  deleteGroupPost(post_id) {
    console.log('post delete in service', post_id)
    return this.http.delete(AppSetting.API_ENDPOINT_PHP + 'post/destory_post/' + post_id)
  }

  //http://social.nuagedigitech.com/api/group_member_list?user_id=7&group_id=52

friendListForGroup(user_id,group_id){
const params = new HttpParams().set('user_id',user_id).set('group_id',group_id)
return this.http.post(AppSetting.API_ENDPOINT_PHP+'group_member_list',params)
}

}
