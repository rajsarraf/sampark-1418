import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupdashboardService } from './groupdashboard.service';
import { Socialusers } from '../home/user.model';
import { PeopleFriendService } from '../people-friend/people-friend.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { SportlightService } from '../spotlight/sportlight.service';
import { GroupsService } from '../groups/groups.service';
import { DashboardService } from '../dashboard/dashboard.service';
import { HomeService } from '../home/home.service';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Link, NgxLinkifyjsService } from 'ngx-linkifyjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-groupdashboard',
  templateUrl: './groupdashboard.component.html',
  styleUrls: ['./groupdashboard.component.css']
})
export class GroupdashboardComponent implements OnInit {

  private groupid: string;
  socialusers: Socialusers;
  fileToUpload;
  iFlag: boolean = false;
  model: any = { comment: '' };
  editPostModel: any = {};
  groupUserDetails;
  groupDetailsData;
  postContent;
  privatepost;
  status = false;
  message: string = "";
  error: string;
  data: any = {};
  fileUpload: any = { status: '', message: '', filePath: '' };
  currentusers: Socialusers;
  notscrolly = true;
  notEmptyPost = true;
  trendingContent = 10

  constructor(private route: ActivatedRoute,
    private router: Router, private groupDashboardService: GroupdashboardService,
    private peopleFriendService: PeopleFriendService, private toastr: ToastrService,
    private spotLightservice: SportlightService, private groupsService: GroupsService,
    private dashboardService: DashboardService, private userService: HomeService,
    private updateProfileService: UpdateprofileService, private spinner: NgxSpinnerService, public linkifyService: NgxLinkifyjsService) { }


  postForm = new FormGroup({
    //user_id:new FormControl(12),
    content: new FormControl(),

  })



  ngOnInit() {

    this.currentusers = JSON.parse(localStorage.getItem('currentUser'));

    this.getUserDetails();

    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });

    this.getUserInfo();
    //this.getFriendList();
    this.getGroupDetails();
    this.getTrendingPostList();
    this.groupsBasedOnInterest();
    this.getFriendListForGroup();
  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  logout() {

    this.userService.logout();
    this.router.navigate(['/home']);
    this.toastr.success('logout successfull')
  }
  mode = []
  getUserDetails() {
    this.updateProfileService.getUserDetails(this.currentusers.id)
      .subscribe((response: any) => {
        // console.log('user Details Response', response);
        this.mode = response.object;
      })
  }
  handleSelection($event) {
    // console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }

  page = 1;
  showMore = false;
  showChar = 100;
  user
  groupAdmin : any;
  getGroupDetails() {
    //console.log('start')
    this.route.params.subscribe(params => {

      if (params["groupdetails_id"]) {
        this.groupid = params["groupdetails_id"];

        this.groupDashboardService.getGroupDetails(this.groupid)
          .subscribe((response: any) => {
            console.log('groip details response',response)
            if (response.object != null) {
              this.groupUserDetails = response.object.userdetails;

              for(let data of this.groupUserDetails){
                if(this.socialusers.id == data.user_id){
                  this.groupAdmin = data.is_admin
                   console.log('group admin status',this.groupAdmin);
                }
              }

              this.groupDetailsData = response.object.groupdetails;

              this.postContent = response.object.posts.data;

            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }


  onScrollDown() {
    console.log('scrolling...................');

    if (this.notscrolly && this.notEmptyPost) {
      const start = this.page;
      this.page += 1;
      this.spinner.show();
      this.notscrolly = false;

      this.route.params.subscribe(params => {
        if (params["groupdetails_id"]) {
          this.groupid = params["groupdetails_id"];
          this.groupDashboardService.getGroupDetailsPage(this.groupid, this.page)
            .subscribe((response: any) => {
              const newPost = response.object.posts.data;
              this.spinner.hide();
              this.notEmptyPost = false;

              if (newPost != null) {
                this.postContent = this.postContent.concat(newPost)
                this.notscrolly = true;

              } else {
                this.notscrolly = false;
                this.notEmptyPost = false;
              }

            })
        }
      });
    }
  }

  // post in group
  imageUrl
  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  privatePost(data) {
    //console.log('data',data);
    this.privatepost = data;
  }
  group_id
  linkInfo
  key
  q
  metadata = 1
  store() {
    var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi

    var str = this.postForm.value.content;
    //console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    //console.log('New Words', newContent)

    this.route.params.subscribe(params => {
      if (params["groupdetails_id"]) {
        this.groupid = params["groupdetails_id"];
        this.status = false;
        const foundLinks: Link[] = this.linkifyService.find(newContent);
        console.log('link found', foundLinks)
        if (foundLinks.length != 0 && this.fileToUpload == null) {
          for (let link of foundLinks) {
            console.log(link.value);
            this.key = '1536acb288c438aae1c7084f769c988e';
            this.q = link.value;
            this.dashboardService.getMetaData(this.key, this.q).subscribe((response: any) => {
              this.linkInfo = response;
              console.log(this.linkInfo);
              this.groupDashboardService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.linkInfo.title,
                this.linkInfo.image, this.linkInfo.url, this.metadata,this.groupid).subscribe(
  
                  post => {
                    this.fileUpload = post;
                    this.iFlag = false;
                    console.log('response from server', post);
                    //this.fileUpload = post,
                    this.status = true;
                    this.emojiFlag = false;
                    // this.fileToUpload ='';
                    // this.privatepost;
                    this.postForm.reset();
                   // this.posts();
                   this.router.navigateByUrl('/navigation/groupdesk/'+this.groupid, { skipLocationChange: true }).then(() => {
                    this.router.navigate(['/navigation/groupdesk',this.groupid]);
                }); 
                  },
                  err => this.error = err
                );
            },err =>{
              this.privatepost = 0;
              this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
                .subscribe((response: any) => {
                  if (response.object != null) {
                    // console.log(post);
                    //this.fileUpload = post,
                    this.status = true;
                    this.iFlag = false;
                    this.emojiFlag = false;
                    // this.fileToUpload = '';
                    this.postForm.reset();
                    this.getGroupDetails();
                    this.router.navigateByUrl('/navigation/groupdesk/'+this.groupid, { skipLocationChange: true }).then(() => {
                      this.router.navigate(['/navigation/groupdesk',this.groupid]);
                  }); 
                  } else if (response.object == null) {
                    console.log('Something Went Wrong')
                  }
    
                },
                  err => this.error = err
                );
            })
          }
        } else {
          this.privatepost = 0;
          this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
            .subscribe((response: any) => {
              if (response.object != null) {
                // console.log(post);
                //this.fileUpload = post,
                this.status = true;
                this.iFlag = false;
                this.emojiFlag = false;
                // this.fileToUpload = '';
                this.postForm.reset();
                this.getGroupDetails();
                this.router.navigateByUrl('/navigation/groupdesk/'+this.groupid, { skipLocationChange: true }).then(() => {
                  this.router.navigate(['/navigation/groupdesk',this.groupid]);
              }); 
              } else if (response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );

        }




      } else {
        console.log('No Response From Server');
      }
    })

  }




  // store() {

  //   var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi
  //   //  console.log('replace text data', badWords);

  //   var str = this.postForm.value.content;
  //   // console.log('compare text', this.postForm.value.content);
  //   var newContent = str.replace(badWords, '*****');
  //   //console.log('New Words', newContent)

  //   this.route.params.subscribe(params => {
  //     if (params["groupdetails_id"]) {
  //       this.groupid = params["groupdetails_id"];
  //       this.status = false;
  //       let post = this.postForm.value;
  //       // console.log('post content', post)
  //       if (post.content != null && this.fileToUpload == null) {
  //         this.privatepost = 0;
  //         this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
  //           .subscribe((response: any) => {
  //             if (response.object != null) {
  //                console.log('response data in post', post);
  //               this.fileUpload = post;
  //               // console.log(post);
  //               //this.fileUpload = post,
  //               this.status = true;
  //               this.iFlag = false;
  //               this.emojiFlag = false;
  //               // this.fileToUpload = '';
  //               this.postForm.reset();
  //               this.getGroupDetails();
  //             } else if (response.object == null) {
  //               console.log('Something Went Wrong')
  //             }

  //           },
  //             err => this.error = err
  //           );
  //       } else if (post.content != null && this.fileToUpload != null) {
  //         this.privatepost = 0;
  //         this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
  //           .subscribe((response: any) => {
  //             if (response.object != null) {
  //               // console.log('response data in post', post);
  //               this.fileUpload = post;
  //               this.status = true;
  //               //this.fileToUpload = '';
  //               this.iFlag = false;
  //               this.emojiFlag = false;
  //               this.postForm.reset();
  //               this.getGroupDetails();

  //             } else if (response.object == null) {
  //               console.log('Something Went Wrong')
  //             }

  //           },
  //             err => this.error = err
  //           );
  //       } else if (post.content == null && this.fileToUpload != null) {
  //         this.privatepost = 0;
  //         this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
  //           .subscribe((response: any) => {
  //             if (response.object != null) {
  //               this.fileUpload = post;
  //               // console.log(post);

  //               this.status = true;
  //               this.iFlag = false;
  //               this.emojiFlag = false;

  //               this.postForm.reset();
  //               this.getGroupDetails();
  //             } else if (response.object == null) {
  //               console.log('Something Went Wrong')
  //             }

  //           },
  //             err => this.error = err
  //           );
  //       } else {
  //         console.log('something went wrong');
  //       }
  //     }


  //   })

  // }
  showpost(id) {
    this.dashboardService.showPostEdit(id)
      .subscribe((response: any) => {
        //console.log('group post edit',response)
        this.editPostModel = response.object
      })

  }
  imageToUpload;
  editPostImage(files: FileList) {
    this.imageToUpload = files.item(0);
    //console.log('Edit Image',this.imageToUpload);
  }
  editGroupPost(id, content) {
    // console.log('edit image in group',id,this.imageToUpload,content,this.groupid)
    this.dashboardService.updateGroupPost(id, this.imageToUpload, content, this.group_id)
      .subscribe((response: any) => {
        if (response.object != null) {
          this.toastr.success('Post has been updated successfully');
          content = {};
          this.getGroupDetails();
          window.document.getElementById("close_groupedit_model").click();
        } else {
          this.toastr.warning('Something Went wrong');
        }
      })

  }


  //get post details

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.dashboardService.getPostDetailsById(id).subscribe((response: any) => {
      // console.log('post details by id response',response);
      this.postData = response.object;
    })
  }


  // comment in group

  commentImage(files: FileList) {
    this.fileToUpload = files.item(0);
    // console.log(this.fileToUpload);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  //post comment

  // addComment(id) {
  //   console.log(this.socialusers.id)

  //   console.log(this.model.comment, id);
  //   this.groupDashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name)
  //     .subscribe((response: any) => {
  //       console.log('comment Response', response)
  //       this.status = true;
  //       this.model.comment = "";
  //       this.getGroupDetails();
  //     })

  // }

  addComment(id) {
    // console.log('commnet and id', this.model.comment, id);
    this.groupDashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload)
      .subscribe((response: any) => {
        // console.log('comment Response', response)
        this.status = true;
        this.model = {};
        this.commentEmoji = false;
        this.imgURL = "";
        this.fileToUpload = null;
        this.getGroupDetails();

      })
  }

  //post like

  likePost(post_id) {

    return this.groupDashboardService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.getGroupDetails();
    });

  }

  //add friends in Group request

  addStatus = 1;
  requestForAddFriendGroup(friend_id) {
    this.route.params.subscribe(params => {
      if (params["groupdetails_id"]) {
        this.groupid = params["groupdetails_id"];
        // console.log('group process.......');
        this.groupDashboardService.addFriendInGroupRequest(this.groupid, friend_id, this.addStatus)
          .subscribe((response: any) => {
            // console.log('add friend group response', response);
            if (response.object == null) {
              // console.log(response.message);
              this.toastr.warning(response.message);
            } else {

              this.toastr.success('User has been successfully added to the Group');
              this.getGroupDetails();
            }
          })

      }
    });
  }

  //add as a group admin
  //  is_admin = 1;

  //  friend_id
  //  makeGroupAdmin(){
  //   this.route.params.subscribe(params =>{
  //     console.log(params)
  //     if(params["groupdetails_id"]){
  //       this.groupid = params["groupdetails_id"];
  //       console.log('group process.......');
  //       this.groupDashboardService.makeGroupAdmin(this.is_admin,this.groupid,this.friend_id)
  //       .subscribe((response: any) =>{
  //         console.log(response);
  //         if(response.object == null){
  //           console.log(response.message);
  //           this.toastr.warning(response.message);
  //         }else{
  //           console.log(response.message);
  //           this.toastr.success('user Added')
  //         }
  //       })

  //     }
  //   })
  //  }


  getUserInfo() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    // console.log('userInformation', this.socialusers)
  }

  //get friend List
  friendstatus = 1;
  friendList
  friend_type = 0;
  getFriendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        //console.log('Friends List', response);
        this.friendList = response.object;
      })
  }
  reportPost(id, receiver_id) {

    //console.log(id,this.socialusers.id,receiver_id)
    this.dashboardService.reportPost(id, this.socialusers.id, receiver_id)
      .subscribe((response => {
        // console.log('response',response);
        this.toastr.success('Reported to admin');
      }))


  }

  groupFriends = []
  getFriendListForGroup() {
    this.route.params.subscribe(params => {
      if (params["groupdetails_id"]) {
        this.groupid = params["groupdetails_id"];
        console.log('group process.......');
        this.groupDashboardService.friendListForGroup(this.socialusers.id, this.groupid)
          .subscribe((response: any) => {

            if (response.object != null) {
              this.groupFriends = response.object;
              // console.log('friends list for group join',response)

              //this.toastr.warning(response.message);
            } else {
              console.log(response.message);
            }
          })

      }
    });
  }

  // File Upload
  uploadImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  //make group admin or remove from group

  updateGroupMemberStatus(groupuser_id, groupdetails_id) {

    if (groupuser_id && groupdetails_id && this.data.value == 1) {
      this.groupDashboardService.makeGroupAdmin(groupuser_id, groupdetails_id, this.data.value)
        .subscribe((response: any) => {

          if (response.object != null) {
            // console.log('update Member Response', response);
            //this.toastr.success('User Removed');
            this.getGroupDetails();
          } else {
            console.log('something went wrong');
          }
        })
    } else if (groupuser_id && groupdetails_id && this.data.value == "") {
      this.groupDashboardService.removeGroupMember(groupuser_id, groupdetails_id)
        .subscribe((response: any) => {
          if (response.object != null) {
            // console.log('remove user response', response)
            this.toastr.success('User has been successfully remove from the Group');
            this.getGroupDetails();
          } else {
            console.log('something went wrong');
          }
        })
    }
  }

  //make Group Admin

  value = 1;

  makeGroupAdmin(groupuser_id, groupdetails_id) {

    this.groupDashboardService.makeGroupAdmin(groupuser_id, groupdetails_id, this.value)
      .subscribe((response: any) => {
        if (response.object != null) {

          this.toastr.success('User status change successfully');
          this.getGroupDetails();
        } else {
          console.log('something went wrong', response);
        }
      })
  }

  removeUserFromGroup(groupuser_id, groupdetails_id) {
    this.groupDashboardService.removeGroupMember(groupuser_id, groupdetails_id)
      .subscribe((response: any) => {
        if (response.object != null) {
          // console.log('remove user response', response);
          this.toastr.success('User has been successfully remove from the Group');
          this.getGroupDetails();
        } else {
          console.log('something went wrong', response);
        }
      })
  }
  leaveFromGroup(groupuser_id, groupdetails_id){
    this.groupDashboardService.removeGroupMember(groupuser_id, groupdetails_id)
    .subscribe((response: any) => {
      if (response.object != null) {
        // console.log('remove user response', response);
        this.toastr.success('User has been successfully leave from the Group');
        this.router.navigate(['/navigation/groups']);
        this.getGroupDetails();
      } else {
        console.log('something went wrong', response);
      }
    })
  }
  leaveGroup(){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to view the activity of this group",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, leave it!'
    }).then((result) => {
      if (result.value) {
        this.groupDashboardService.removeGroupMember(this.socialusers.id,this.groupid)
    .subscribe((response: any) => {
      if (response.object != null) {
        // console.log('remove user response', response);
        // this.toastr.success('User has been successfully leave from the Group');
        this.router.navigate(['/navigation/groups']);
        this.getGroupDetails();
      } else {
        console.log('something went wrong', response);
      }
    })
        Swal.fire(
          'leaved!',
          'You has been successfully leave from the Group',
          'success'
        )
      }
    })
    
    
  }

  blockRequestStatus = 3;

  BlockUserFromGroup(groupuser_id, groupdetails_id) {
    this.groupDashboardService.blockGroupMember(groupuser_id, groupdetails_id, this.blockRequestStatus)
      .subscribe((response: any) => {
        if (response.object != null) {
          // console.log('Block user response', response);
          this.toastr.success('User block request has been sent successfully');
          this.getGroupDetails();
        } else {
          console.log('something went wrong', response);
        }
      })
  }


  //delete post

  deletePost(id) {
    this.groupDashboardService.deleteGroupPost(id)
      .subscribe((response: any) => {
        //console.log('Delete Post Response', response);
        this.toastr.success('Post has been successfully deleted');
        this.getGroupDetails();

      })
  }

  //get Tranding List

  trendingPost = [];
  type = 0;
  getTrendingPostList() {
    this.dashboardService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        // console.log('Trending List', response);
        this.trendingPost = response.object;
      })
  }



  // Groups On Behalf of Interest

  InterestedGroupsList = [];
  page1: number = 1;
  totalintrestedGroup
  groupsBasedOnInterest() {
    this.groupsService.groupsBasedOnInterest(this.socialusers.id, this.page1)
      .subscribe((response: any) => {
        // console.log('Group Details Based On Interest',response);
        this.InterestedGroupsList = response.object.data;
        this.totalintrestedGroup = response.object.total;
      })
  }

  intrestedGroupChange(event) {
    this.page1 = event;
    this.groupsService.groupsBasedOnInterest(this.socialusers.id, this.page1)
      .subscribe((response: any) => {
        const newPost = response.object.data;
        // console.log('page2 response',newPost);
        this.InterestedGroupsList = newPost;

      })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }

  //groups On Behalf Of Profile

  profileGroupList = [];
  page2: number = 1;
  groupsBasedOnProfile() {
    this.groupsService.groupsBasedOnProfile(this.socialusers.id, this.page2)
      .subscribe((response: any) => {
        // console.log('Group list On Behalf Of profile',response);
        this.profileGroupList = response.object.data;
      })
  }

  profileGroupChange(event) {
    // console.log('page1',event);
    this.page2 = event;
    //console.log('pageEvent',this.page2);
    this.groupsService.groupsBasedOnProfile(this.socialusers.id, this.page2)
      .subscribe((response: any) => {
        const newPost = response.object.data;
        // console.log('page2 response',newPost);
        this.profileGroupList = newPost;
      })
    //this.page += 1;
    //this.pendingSpolightConfig.currentPage = event;
  }


  // join Group

  joinStatus = 1;

  joinGroup(id) {
    this.groupsService.joinGroup(id, this.socialusers.id, this.joinStatus)
      .subscribe((response: any) => {
        //console.log('Group Join Response',response);
        if (response.object == null) {
          //console.log(response.message);
          this.toastr.warning(response.message);
        } else {
          //console.log(response.message);
          this.toastr.success('Group has been joined successfully');

        }
      })
  }

  comment_message: string = "";
  comment_img;
  imgURL: any = "";


  div_id;
  triggerUpload(id) {
    this.div_id = id;
    //console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);

    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }

  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;
  status_id: number;

  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }

  handleComment($event) {

    if (this.status_id != null) {
      this.model.comment += $event.emoji.native;
    }
    //this.selectedEmoji = $event.emoji;

  }

  seeAll: boolean = true;
  SeeAllFun() {
    this.seeAll = !this.seeAll
  }
  seeAll1: boolean = true;
  SeeAllFun1() {
    this.seeAll1 = !this.seeAll1
  }

}

