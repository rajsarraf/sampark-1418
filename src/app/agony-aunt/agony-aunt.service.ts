import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { AppSetting } from '../app.setting';
import { Observable,throwError } from 'rxjs';
import { Post } from '../dashboard/post';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AgonyAuntService {
  

  constructor(private http : HttpClient) { }

  
getGroupDetails(group_id,user_id){
    const params = new HttpParams().set('groupid',group_id).set('user_id',user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/fetch_group_details',params)

  }
  getGroupDetailsPage(group_id,user_id,page){
    const params = new HttpParams().set('groupid',group_id).set('user_id',user_id).set('page',page);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/fetch_group_details',params)
  }

  createPost(user_id, content, group_id, File, user_name): Observable<Post> {
    console.log('post service', content, user_id, user_name);
    if (File != null) {
      console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('group_id', group_id);
      formData.append('user_name', user_name);
     
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', formData)
    } else {
      console.log(user_name)
      const parmas = new HttpParams().set('user_id', user_id).set('content',content).set('group_id', group_id).set('user_name', user_name)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', parmas);
    }

  }
  storeLike(user_id, post_id) {

    console.log(post_id)
    const params = new HttpParams().set('user_id', user_id).set('post_id', post_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'like/post', params);

  }

  commentPost(user_id, parent_id, comment, user_name, File) {
    if (File == null) {
    console.log('in comment srvice', user_id, parent_id, comment, user_name)
    const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
      .set('user_name', user_name);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
    }else{
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('parent', parent_id);
      formData.append('user_id', user_id);
      formData.append('content', comment);
      formData.append('user_name', user_name);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post/', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
      
    }
  }

  private getEventMessage(event: HttpEvent<any>, formData) {

    switch (event.type) {
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
        break;
      case HttpEventType.Response:
        return this.apiResponse(event);
        break;
      default:
        return `File "${formData.get('file').name}" surprising upload event: ${event.type}.`;
    }
  }


  getTrendingPostList(type) {
    const params = new HttpParams().set('type',type)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'spotlight/trendingpost',params);
  }


  private fileUploadProgress(event) {
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    return event.body;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened. Please try again later.');
  }


  // Join Group Service

  joinGroup(groupdetailsid, user_id, status) {
    const params = new HttpParams().set('groupdetailsid', groupdetailsid).set('user_id', user_id)
      .set('status', status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_add_member_request', params);
  }

  // left group

  leftGroup(groupdetailsid, user_id){
    const params = new HttpParams().set('groupdetailsid', groupdetailsid).set('user_id', user_id);
  return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_remove_member', params);
  }

  deleteGroupPost(post_id) {
    //console.log('post delete in service', post_id)
    return this.http.delete(AppSetting.API_ENDPOINT_PHP + 'post/destory_post/' + post_id)
  }

}
