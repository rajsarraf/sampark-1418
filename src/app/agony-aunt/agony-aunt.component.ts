import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AgonyAuntService } from './agony-aunt.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Socialusers } from '../home/user.model';
import { ThrowStmt } from '@angular/compiler';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agony-aunt',
  templateUrl: './agony-aunt.component.html',
  styleUrls: ['./agony-aunt.component.css']
})
export class AgonyAuntComponent implements OnInit {

  message: string = "";
  error: string;
  model: any = { comment: '' };
  iFlag: boolean = false;
  socialusers: Socialusers;
  fileToUpload;
  fileUpload: any = { status: '', message: '', filePath: '' };
  status = false;
  showMore = false;
  showChar = 400;

  notscrolly = true;
  notEmptyPost = true;

  postForm = new FormGroup({

    content: new FormControl(),

  })
  constructor(private agonyAuntService: AgonyAuntService, private route: ActivatedRoute,
    private toastr: ToastrService,private router : Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));

    $("input[type='image']").click(function () {
      $("input[id='File']").click();


    });
    this.getGroupDetails();
  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  group_user_id
  groupid
  groupDatailsList = [];
  allPost;
  getGroupDetails() {
    console.log('start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.groupid = params["id"];
        console.log(this.groupid)
        console.log('group process.......');
        this.agonyAuntService.getGroupDetails(this.groupid, this.socialusers.id)
          .subscribe((response: any) => {
            console.log('Agony aunt group details', response)
            if (response.object != null) {
              console.log('Agony aunt group details', response)
              this.allPost = response.object.posts.data;
              console.log(this.allPost)
              this.groupDatailsList = response.object
              for(let data of response.object.userdetails){
                this.group_user_id= data.id;
                console.log('user in group id',this.group_user_id)
              }
            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }

  scrollcount
  page: number = 1;
  onScrollDown() {
    console.log('scrolling...................', this.page)
    if (this.notscrolly && this.notEmptyPost) {
      const start = this.page;
      this.page += 1;
      this.spinner.show();
      this.notscrolly = false;
      // console.log('page.................', this.page)
      this.agonyAuntService.getGroupDetailsPage(this.groupid, this.socialusers.id,this.page)
        .subscribe((response: any) => {
          const newPost = response.object.posts.data;
          this.spinner.hide();
          this.notEmptyPost = false;
           console.log('page 2 response',response);
          if (newPost != null) {
            this.allPost = this.allPost.concat(newPost)
            this.notscrolly = true;
            //this.notEmptyPost = false;
          } else {
            this.notscrolly = false;
            this.notEmptyPost = false;
          }
        })
    }

  }


  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }

  handleSelection($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  store() {

   var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi
   console.log('replace text data', badWords);

   
    var str = this.postForm.value.content;
    console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    console.log('New Words', newContent)

    console.log('start.....')
    this.route.params.subscribe(params => {
      if (params["id"]) {
        this.groupid = params["id"];
        console.log('group process.......');
        console.log(this.groupid)

        this.status = false;
        let post = this.postForm.value;
        console.log('post content', post)
        if (post.content != null && this.fileToUpload == null) {
          this.agonyAuntService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
            .subscribe((response: any) => {
              if (response.object != null) {
                console.log('response data in post', post);
                this.fileUpload = post;
                console.log(post);
                //this.fileUpload = post,
                this.status = true;
                this.iFlag = false;
                // this.fileToUpload = '';
                this.postForm.reset();
                this.getGroupDetails();
              } else if (response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );
        } else if (post.content != null && this.fileToUpload != null) {

          this.agonyAuntService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
            .subscribe((response: any) => {
              if (response.object != null) {
                console.log('response data in post', post);
                this.fileUpload = post;
                console.log(post);
                //this.fileUpload = post,
                this.status = true;
                //this.fileToUpload = '';
                this.iFlag = false;
                this.postForm.reset();
                this.getGroupDetails();

              } else if(response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );
        } else if (post.content == null && this.fileToUpload != null) {

          this.agonyAuntService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
            .subscribe((response: any) => {
              if (response.object != null) {
                console.log('response data in post', post);
                this.fileUpload = post;
                console.log(post);
                //this.fileUpload = post,
                this.status = true;
                this.iFlag = false;
                // this.fileToUpload = '';
                this.postForm.reset();
                this.getGroupDetails();
              } else if (response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );
        } else {
          console.log('something went wrong');
        }
      }


    })

  }

  addComment(id) {

    console.log(this.socialusers.id)
    //console.log('commnet and id',comment.value,id);
    this.agonyAuntService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.status = true;
        this.model = {};
        this.getGroupDetails();

      })
  }

  likePost(post_id) {

    return this.agonyAuntService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);

      this.getGroupDetails();
    });

  }

  deletePost(id) {
    this.spinner.show()
    //console.log(id)
    this.agonyAuntService.deleteGroupPost(id)
      .subscribe((response: any) => {
        // console.log('Delete Post Response', response);
        this.getGroupDetails(); 
        this.spinner.hide();
        this.toastr.success('Post has been deleted');
      })
  }


  div_id;
  comment_img
  triggerUpload(id) {
    this.div_id = id;
    console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }


  imgURL
  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }
  status_id
  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }
  emojiComment
  handleComment($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.model.comment += $event.emoji.native;
  }



  //Group Join

  // join Group

  joinStatus = 1;
  joinGroup() {
    console.log('start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.groupid = params["id"];
        console.log(this.groupid)
        console.log('group process.......');
        this.agonyAuntService.joinGroup(this.groupid, this.socialusers.id, this.joinStatus)
          .subscribe((response: any) => {
            console.log('group joined response', response)
            if (response.object == null) {
              console.log(response.message);
              this.toastr.warning(response.message);

            } else {
              console.log(response.message);
              this.toastr.success('Congratulations you have joined  Agony Aunt Group');
              this.getGroupDetails();
            }
          })

      }
    });
  }


 //left Group 


  leftGroup() {
    console.log('left group start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.groupid = params["id"];
        console.log(this.groupid,this.socialusers.id)
        console.log('group process.......');
        this.agonyAuntService.leftGroup(this.groupid, this.socialusers.id)
          .subscribe((response: any) => {
            console.log('left response', response)
            if (response.object == null) {
              console.log(response.message);
              this.toastr.warning(response.message);
            } else {
              console.log(response.message);
              this.toastr.success('You have left the Agony Aunt Group');
              this.router.navigateByUrl('/navigation/agony-aunt/'+this.groupid, { skipLocationChange: true }).then(() => {
                this.router.navigate(['/navigation/agony-aunt',this.groupid]);
            }); 
              this.getGroupDetails();
            }
          })

      }
    });
  }

}
