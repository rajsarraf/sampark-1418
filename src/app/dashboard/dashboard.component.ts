import { Component, OnInit, Input, Output } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Socialusers } from '../home/user.model';
import { HomeService } from '../home/home.service';
import { DashboardService } from './dashboard.service';
import { Observable } from 'rxjs';
import { Post } from './post';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PostLikeService } from './post-like.service';
// import * as $ from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SportlightService } from '../spotlight/sportlight.service';
import { PeopleFriendService } from '../people-friend/people-friend.service';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import Swal from 'sweetalert2';
import { MapsAPILoader } from '@agm/core';
import { NgxLinkifyOptions, NgxLinkifyjsService, Link } from 'ngx-linkifyjs';
import { GroupsService } from '../groups/groups.service';
import { EventEmitter } from 'events';
//import { google } from 'google-maps';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  socialusers: Socialusers;
  user: any;
  status = false;
  CloseBtn: boolean = true;
  post: any;
  postObj;
  model: any = { comment: '' };
  editPostModel: any = {};
  fileToUpload;
  fileUpload: any = { status: '', message: '', filePath: '' };
  error: string;
  iFlag: boolean = false;
  message: string = "";
  status_id: number;
  privatepost;
  currentusers: Socialusers;
  searchmodel: any = {};
  notscrolly = true;
  notEmptyPost = true;
  shareGroupId: any = {};

  lat: any;
  lng: any;
  city: any;
  address;
  zoom;
  private geoCoder;
  trendingContent = 10;

  // message:string="";
  comment_message: string = "";
  comment_img;
  imgURL: any = "";

  @Input() myInput: string;

  @Output() serchTerm = new EventEmitter()

  allPost;

  selectedItems = { id: "", groupname: "" };
  dropdownSettings = {};
  friendsDropdown = {};

  postForm = new FormGroup({

    content: new FormControl(),

  })

  editForm = new FormGroup({
    content: new FormControl(),

  })

  mySubscription: any;

  constructor(private formBuilder: FormBuilder, public OAuth: AuthService, private router: Router, private userService: HomeService,
    private postService: DashboardService, private postLikeService: PostLikeService,
    private spinner: NgxSpinnerService, private toastr: ToastrService,
    private spotLightservice: SportlightService, private peopleFriendService: PeopleFriendService,
    private updateProfileService: UpdateprofileService, private mapsAPILoader: MapsAPILoader,
    public linkifyService: NgxLinkifyjsService, private socialAuthService: AuthService, private groupsService: GroupsService,
   private route : ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    const options: NgxLinkifyOptions =
    {
      attributes: null,
      className: 'linkified',
      defaultProtocol: 'http',
      events: null,
      format: function (value, type) {
        return value;
      },
      formatHref: function (href, type) {
        return href;
      },
      ignoreTags: [],
      nl2br: false,
      tagName: 'a',
      target: {
        url: '_blank'
      },
      validate: true
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });

    //   $('video').each(function(){
    //     let audioPlayer = <HTMLVideoElement> document.getElementById("video");
    //     audioPlayer.play();
    //     if ($(this).is(":in-viewport")) {
    //       let audioPlayer = <HTMLVideoElement> document.getElementById("video");
    //       audioPlayer.play();
    //         ($(this)[0] as any).play();
    //     } else {
    //         ($(this)[0] as any).pause();
    //     }
    // })
    //   $(window).scroll(function() {
    //     let audioPlayer = <HTMLVideoElement> document.getElementById("video");
    //        audioPlayer.play();
    //     $('video').each(function() {
    //         if ($(this).visible(true)) {
    //           audioPlayer.play();
    //         } else {
    //           audioPlayer.pause();
    //         }
    //     })
    // });


  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }


  ngOnInit() {

    this.currentusers = JSON.parse(localStorage.getItem('currentUser'));
  const id = this.route.snapshot.queryParamMap.get('title');
console.log(id);
    this.getUserDetails();

    //console.log(this.myInput)
    this.getTrendingPostList()

    $("input[type='image']").click(function () {
      $("input[id='File']").click();


    });

    $("input[type='image1']").click(function () {
      $("input[id='File1']").click();


    });

    // localStorage.removeItem('currentUser');
    // localStorage.removeItem('socialusers')

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.findLatAndLon();
    if (this.socialusers != null) {
      // posts method called
      this.posts();
      // this.getHeader();
    }
    //console.log('Dash Board', this.socialusers);
    //console.log(this.socialusers.name);
    this.percentageCount();
    this.getPeopleFriendAllList()
    this.getVentBoxGroupList();
    this.getAdminTrending();
    this.getUserGroupList();
    // this.myFunction();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      enableCheckAll: false,
      // selectAllText: '',
      // unSelectAllText: '',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.friendsDropdown = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      enableCheckAll: false,
      // selectAllText: '',
      // unSelectAllText: '',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.getFriendList();

  }



  countData
  percentageCount() {
    this.postService.progressCount(this.socialusers.id)
      .subscribe((response: any) => {
        //console.log('response count', response);
        this.countData = response;
      })
  }

  ventList = [];
  getVentBoxGroupList() {
    this.postService.getVentBoxGroup()
      .subscribe((response: any) => {
        // console.log('vent Box Response', response);
        this.ventList = response.object;
      })
  }

  userGroupList: any = [];
  totaluserGroup
  pageNo: number = 1;
  getUserGroupList() {
    this.groupsService.getUserGroupList(this.socialusers.id, this.pageNo)
      .subscribe((response: any) => {
        //console.log('Your Group List', response.object.data);

        for (let data of response.groupquestions) {
          data.answer = ""
        }
        this.userGroupList = response.object.data;
        this.totaluserGroup = response.object.total;
      });
  }

  sharePostStatus = 1;
  sharePostInGroup(postId) {
    // console.log('Share In Group',postId,this.shareGroupId)
    if (this.shareGroupId.groupdetails_id == null) {
      this.toastr.warning('Please Choose The Group')
    } else {
      //user_id,post_id,share_status,user_name,group_id
      this.postService.sharePostInGroup(this.socialusers.id, postId, this.sharePostStatus,
        this.socialusers.name, this.shareGroupId.groupdetails_id)
        .subscribe((response: any) => {
          // console.log('response for share post in group',response)
          if (response.object != null) {
            this.toastr.success('Congratulations your post has been shared successfully');
            this.posts();
            this.router.navigate(['/navigation/dashboard']);
            window.document.getElementById("close_share_model").click();
          } else {
            this.toastr.error('Something Went Wrong');
          }
        })
    }
  }

  findLatAndLon() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
        //console.log("lng", this.lng);
        //console.log("lat", this.lat)
        //console.log('lat and log data for send', this.lat, this.lng);
        this.postService.updateLatitudeLog(this.lat, this.lng, this.socialusers.id)
          .subscribe((response: any) => {
            // console.log('update lat lng response', response);
          })
      });
    }

  }

  posturl = "http%3A%2F%2F103.240.90.156%3A4040%2Fnavigation%2Fdashboard&amp;src=sdkpreparse";
  postdesc = "hagdkfsjfsjkcvjksvcgsv";
  posttitle = "hkgdjfvdkgvdghcvjdvc";
  fb(e) {


    e.preventDefault();
    var windowObj = window.open(
      'https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F103.240.90.156%3A4040%2Fnavigation%2Fdashboard&amp;src=sdkpreparse' + 'jhgktrjhgjkthg',
      'facebook-popup',
      'height=350,width=600'
    );
    windowObj.document.head.innerHTML = '<meta property="og:title" content="The Rock"/><meta property="og:type" content="movie"/><meta property="og:url" content="http://www.imdb.com/title/tt0117500/"/><meta property="og:image" content="http://ia.media-imdb.com/rock.jpg"/><meta property="og:site_name" content="IMDb"/><meta property="fb:admins" content="USER_ID"/><meta property="og:description"      content="A group of U.S. Marines, under command of               a renegade general, take over Alcatraz and threaten San Francisco Bay with biological weapons."/>'
    //   var x = document.URL;
    // let  href: "https://jasonwatmore.com/post/2018/06/01/angular-2-social-sharing-buttons-for-facebook-google-plus-twitter-linkedin-and-pinterest";
    //   e.preventDefault();
    //   var facebookWindow = window.open(
    //     'https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F103.240.90.156%3A4040%2Fnavigation%2Fdashboard&amp;src=sdkpreparse'+ 'Use Referral code ->' + this.userData + '   to join the newest fantasy sports platform' +  href + '. Earn extra points along with lots of exciting prizes. Don’t delay, join the match today! url',
    //     'facebook-popup',
    //     'height=350,width=600'
    //   );
    //   if (facebookWindow.focus) {
    //     facebookWindow.focus();
    //   }
    //   return false;
  }


  destination
  position
  //   sosSend() {
  //     if (navigator) {
  //       navigator.geolocation.getCurrentPosition(pos => {
  //         this.lng = +pos.coords.longitude;
  //         this.lat = +pos.coords.latitude;
  //         console.log("lng", this.lng);
  //         console.log("lat", this.lat)
  //         // this.getAddress = (this.lat, this.lng)
  //         console.log('lat and log data for SOS', this.lat, this.lng);

  //         console.log('this is get Address Column')

  // let address = "Gurugram";


  //         this.postService.sendEmergencyMessage(this.socialusers.id, this.lng, this.lat,address)
  //           .subscribe((response: any) => {
  //             console.log('Send Emegency Message Response', response)
  //           })

  //       }

  //       )

  //     }

  //   }

  sosSend() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

    });
  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.lat, this.lng);
      });
    }
  }
  getAddress(latitude, longitude) {
    console.log('google address result by geocoder', latitude, longitude);
    this.geoCoder = new google.maps.Geocoder;
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      //console.log('google address result by geocoder', results);
      //console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          // console.log('Address In GeoCoder', this.address)
          Swal.fire({
            title: 'Are you sure?',
            text: "You want to sent SOS",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Send SOS'
          }).then((result) => {
            if (result.value) {
              this.postService.sendEmergencyMessage(this.socialusers.id, longitude, latitude, this.address)
                .subscribe((response: any) => {
                  // console.log('Send Emegency Message Response', response);
                  // this.toastr.success('Message Sent successfully');
                })
              Swal.fire(
                'Sent!',
                'Your SOS Sent Successfully',
                'success'
              )
            }
          })

        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }

    });
  }

  // getAddress(latitude, longitude) {
  //   console.log('this is get Address Column')
  //   this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
  //     console.log('this is result', results);
  //     console.log(status);
  //     if (status === 'OK') {
  //       if (results[0]) {
  //         this.zoom = 12;
  //         this.address = results[0].formatted_address;
  //       } else {
  //         window.alert('No results found');
  //       }
  //     } else {
  //       window.alert('Geocoder failed due to: ' + status);
  //     }

  //   });
  // }



  logout() {

    this.userService.logout();
    this.router.navigate(['/home']);
    this.toastr.success('logout successfull')
  }
  mode = []
  getUserDetails() {
    this.updateProfileService.getUserDetails(this.currentusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.mode = response.object;
      })
  }
  getUserPost() {
    console.log('serach by username', this.searchmodel.username)
    this.router.navigate(['/navigation/searchpost', this.searchmodel.username]);
  }
  logoutt() {
    this.userService.logout();
    this.router.navigate(['/home']);
  }

  imageError
  max_size = 104857600;
  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
     
    if ( this.fileToUpload.size > this.max_size) {
      this.imageError =
          'Maximum size allowed is 100MB';
     console.log(this.imageError)
      return false;
  }
    
  }
  privatePost(data) {
    //console.log('change', data)
    this.privatepost = data;
  }

  tagFriendsArr = []
  tagArrString
  // onTagFriendSelect(item: any) {

  //   console.log('item', item);
  //   this.tagFriendsArr.push(item.id);
  //   this.tagArrString = JSON.stringify(this.tagFriendsArr);
  //   console.log(this.tagArrString)
  //   console.log('my final array', this.tagFriendsArr);
  // }

  // onTagFriendDeselect(item: any) {
  //   this.tagFriendsArr.pop();
  //   console.log('Deselect item', item)
  // }
  searchText;
  onChangeCategoryEvent(event, data1 : any) { 

    if(event == true){
      this.friendsArr.push(data1.id);
      this.tagArrString = JSON.stringify(this.friendsArr);
      console.log(this.tagArrString)
    }else{
      this.friendsArr.pop();
      this.tagArrString = JSON.stringify(this.friendsArr);
      console.log(this.tagArrString)
     
    }
    
  }

  linkInfo
  key
  q
  metadata = 1
  store() {

    var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi

    var str = this.postForm.value.content;
    //console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    //console.log('New Words', newContent)
    console.log('tag friend id', this.tagArrString)
    if(this.tagArrString == undefined){
      console.log("undefined array");
      this.tagArrString = [];
    }

  //  console.log('cfdghfcd',this.tagArrString.length)
    const foundLinks: Link[] = this.linkifyService.find(newContent);
    console.log('link found', foundLinks)
    if (foundLinks.length != 0 && this.fileToUpload == null) {
      for (let link of foundLinks) {

        console.log(link.value);
        this.key = '1536acb288c438aae1c7084f769c988e';
        this.q = link.value;
        this.postService.getMetaData(this.key, this.q).subscribe((response: any) => {
          this.linkInfo = response;
          console.log(this.linkInfo);
          if (this.privatepost == null) {
            this.privatepost = 0;
            this.status = false;
            //let post = this.postForm.value;
            //console.log('metadata post',this.linkInfo.description)
            this.postService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.privatepost, this.linkInfo.title,
              this.linkInfo.image, this.linkInfo.url, this.metadata, this.tagArrString).subscribe(

                post => {
                  this.fileUpload = post;
                  this.iFlag = false;
                  console.log('response from server', post);
                  //this.fileUpload = post,
                  this.status = true;
                  this.emojiFlag = false;
                  // this.fileToUpload ='';
                  // this.privatepost;
                  this.postForm.reset();
                  this.posts();
                  this.router.navigate(['/navigation/dashboard']);
                },
                err => this.error = err
              );
          } else {
            this.status = false;
            //let post = this.postForm.value;
            this.postService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.privatepost, this.linkInfo.title,
              this.linkInfo.image, this.linkInfo.url, this.metadata, this.tagArrString).subscribe(

                post => {
                  this.fileUpload = post;
                  this.iFlag = false;
                  console.log('response from server', post);
                  //this.fileUpload = post,
                  this.status = true;
                  // this.fileToUpload ='';
                  this.emojiFlag = false;
                  this.privatepost;
                  this.postForm.reset();
                  this.posts();
                  this.router.navigate(['/navigation/dashboard']);
                },
                err => this.error = err
              );
          }
        }, err => {
          console.log('error response section', err);
          if (this.privatepost == null) {
            this.privatepost = 0;
            this.status = false;
            let post = this.postForm.value;
            this.postService.createPost(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost, this.tagArrString).subscribe(

              post => {
                this.fileUpload = post;
                this.iFlag = false;
                // console.log('response from server',post);
                //this.fileUpload = post,
                this.status = true;
                this.emojiFlag = false;
                // this.fileToUpload ='';
                // this.privatepost;
                this.postForm.reset();
                this.posts();
                this.router.navigate(['/navigation/dashboard']);
              },
              err => this.error = err
            );
          } else {
            this.status = false;
            let post = this.postForm.value;
            this.postService.createPost(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost, this.tagArrString).subscribe(

              post => {
                this.fileUpload = post;
                this.iFlag = false;
                // console.log('response from server',post);
                //this.fileUpload = post,
                this.status = true;
                // this.fileToUpload ='';
                this.emojiFlag = false;
                this.privatepost;
                this.postForm.reset();
                this.posts();
                this.router.navigate(['/navigation/dashboard']);
              },
              err => this.error = err
            );
          }


        })
      }
    } else {
      console.log('file',this.fileToUpload)
      if(this.fileToUpload != undefined && this.fileToUpload.size > this.max_size){
         this.toastr.warning('Maximum size allowed is 100MB')
      }else{
        if (this.privatepost == null) {
          this.privatepost = 0;
          this.status = false;
          let post = this.postForm.value;
          this.postService.createPost(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost, this.tagArrString).subscribe(
  
            post => {
              this.fileUpload = post;
              this.iFlag = false;
              // console.log('response from server',post);
              //this.fileUpload = post,
              this.status = true;
              this.emojiFlag = false;
              // this.fileToUpload ='';
              // this.privatepost;
              this.postForm.reset();
              this.posts();
              this.router.navigate(['/navigation/dashboard']);
            },
            err => this.error = err
          );
        } else {
          this.status = false;
          let post = this.postForm.value;
          this.postService.createPost(this.socialusers.id, newContent, this.fileToUpload, this.socialusers.name, this.privatepost, this.tagArrString).subscribe(
  
            post => {
              this.fileUpload = post;
              this.iFlag = false;
              // console.log('response from server',post);
              //this.fileUpload = post,
              this.status = true;
              // this.fileToUpload ='';
              this.emojiFlag = false;
              this.privatepost;
              this.postForm.reset();
              this.posts();
              this.router.navigate(['/navigation/dashboard']);
            },
            err => this.error = err
          );
        }
      }


    }




  }

  Platform_type = "dashboard";

  addComment(id) {
    console.log(this.dataArr)
    if (this.dataArr.length == 0) {
      var comment_type = 'normal';
      console.log('commnet and id', this.model.comment, id);
      this.postService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type,this.Platform_type)
        .subscribe((response: any) => {
          //console.log('comment Response', response)
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.router.navigate(['/navigation/dashboard']);
        })
    } else {
      console.log('taged section')
      var comment_type_tag = 'tag'
      this.postService.commentTagPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type_tag, this.dataArr,this.Platform_type)
        .subscribe((response: any) => {
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.router.navigate(['/navigation/dashboard']);
        })

    }

  }
  // pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
  pdfSrc = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";

  // get all posts
  page: number = 1;
  showMore = false;
  showChar = 500;
  link = []

  posts() {

    //console.log('page count', this.page)

    this.postService.getPosts(this.socialusers.id).subscribe((data: any) => {
      this.post = data
      console.log('post response', this.post.object.data);
      this.allPost = this.post.object.data;
      // for(let content of this.allPost){
      //  if(content.content != null){
      //   const foundLinks: Link[] = this.linkifyService.find(content.content);
      //   console.log('link found',foundLinks)
      //  }
      // }
    });
  }


  scrollcount

  onScrollDown() {
    console.log('scrolling...................', this.page)
    if (this.notscrolly && this.notEmptyPost) {
      const start = this.page;
      this.page += 1;
      this.spinner.show();
      this.notscrolly = false;
      // console.log('page.................', this.page)
      this.postService.getPostsPage(this.socialusers.id, this.page)
        .subscribe((response: any) => {
          const newPost = response.object.data;
          this.spinner.hide();
          this.notEmptyPost = false;
          // console.log('page 2 response',response);
          if (newPost != null) {
            this.allPost = this.allPost.concat(newPost)
            this.notscrolly = true;
            //this.notEmptyPost = false;
          } else {
            this.notscrolly = false;
            this.notEmptyPost = false;
          }
        })
    }

  }

  // Dashboard code
  btnClose() {
    this.CloseBtn = !this.CloseBtn
  }

  likePost(post_id) {
    //console.log('like'+post_id)
    return this.postLikeService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      //console.log('like Data ', data);
      this.posts();
      this.getTrendingPostList();
    });

  }

  likePostModal(post_id) {
    return this.postLikeService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      //console.log('like Data ', data);
      this.posts();
      this.getPostDetailsById(post_id);
    });

  }

  addCommentModal(id) {
    console.log(this.dataArr)
    if (this.dataArr.length == 0) {
      var comment_type = 'normal';
      console.log('commnet and id', this.model.comment, id);
      this.postService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type,this.Platform_type)
        .subscribe((response: any) => {
          //console.log('comment Response', response)
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.getPostDetailsById(id);
        })
    } else {
      console.log('taged section')
      var comment_type_tag = 'tag'
      this.postService.commentTagPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload, comment_type_tag, this.dataArr,this.Platform_type)
        .subscribe((response: any) => {
          this.status = true;
          this.model = {};
          this.commentEmoji = false;
          this.posts();
          this.getPostDetailsById(id);
        })

    }

  }

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }

  handleSelection($event) {
    //console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  trendingPost = [];
  trendingLoader: boolean = false;
  type = 0;
  getTrendingPostList() {
    this.trendingLoader = true;
    this.postService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        // console.log('Trending List', response);
        this.trendingLoader = false;
        this.trendingPost = response.object;
      })
  }

  //delete post

  deletePost(id) {
    this.spinner.show()
    //console.log(id)
    this.postService.deleteGroupPost(id)
      .subscribe((response: any) => {
        // console.log('Delete Post Response', response);
        this.posts();
        this.getTrendingPostList();
        this.spinner.hide();
        this.toastr.success('Post has been deleted');
      })
  }

  //Report post

  reportPost(id, receiver_id) {

    // console.log(id, this.socialusers.id, receiver_id)
    this.postService.reportPost(id, this.socialusers.id, receiver_id)
      .subscribe((response => {
        // console.log('response', response);
        // console.log('report Added successfully');
        this.toastr.success('Reported to admin');
        this.posts();
      }))


  }

  peopleList = [];
  getPeopleFriendAllList() {
    this.peopleFriendService.getPeopleFriendsList(this.socialusers.id)
      .subscribe((response: any) => {
        //console.log('people Friend Response list', response);
        if (response != null) {

          this.peopleList = response;
          //console.log(this.peopleList)
        } else {
          this.toastr.error('Something went wrong');
        }
      });
  }

  frienaddstatus: 0;

  addPeopleFriends(id) {
    //console.log(id)
    this.peopleFriendService.addFriend(this.socialusers.id, id, this.frienaddstatus)
      .subscribe((response: any) => {
        //console.log(response);
        if (response != null) {
          this.peopleList = response.object;
          this.toastr.success('Friend Request has been Sent successfully');
          this.getPeopleFriendAllList();
        } else {
          this.toastr.error('something went wrong');
        }

      })
  }

  editData = [];
  editFormData
  showPost(id) {
    // console.log(id);
    this.postService.showPostEdit(id)
      .subscribe((response: any) => {
        // console.log('showpost', response)
        this.editPostModel = response.object;

      })
  }


  imageToUpload;
  editPostImage(files: FileList) {
    this.imageToUpload = files.item(0);
    // console.log('Edit Image', this.imageToUpload);
  }


  editUserPost(id, content) {
    let formData = this.editForm.getRawValue()
    //console.log('Edit Image in Typescript', id, this.imageToUpload, content)
    this.postService.updatePost(id, this.imageToUpload, content)
      .subscribe((response: any) => {
        if (response.object != null) {
          this.toastr.success('Post has been updated successfully');
          content = {};
          this.posts();
          window.document.getElementById("close_model").click();
        } else {
          this.toastr.warning('Something Went wrong');
        }

      })
  }

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.postService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }

  share_status = 1;

  friends_Array = []
  id_Array
  sharePost(id) {

    this.friends_Array.push(this.socialusers.id)
    this.id_Array = JSON.stringify(this.friends_Array);
    //console.log('post_id for share post', id)
    this.postService.shareUserPost(this.socialusers.id, id, this.share_status, this.socialusers.name, this.id_Array)
      .subscribe((response: any) => {
        console.log(response)
        if (response.object != null) {
          console.log('share post response', response);
          this.toastr.success('Post has been shared successfully');
          this.posts();
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  div_id;
  triggerUpload(id) {
    this.div_id = id;
    // console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    //console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }

  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    //console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }

  handleComment($event) {
    //console.log($event);
    if (this.status_id != null) {
      this.model.comment += $event.emoji.native;
    }
    //this.selectedEmoji = $event.emoji;

  }

  trendingPage = 1
  adminTrendingLoader: boolean = false;
  adminTrendingPost = [];
  getAdminTrending() {
    this.adminTrendingLoader = true;
    this.postService.getAdminTrendingPost(this.trendingPage, this.socialusers.id)
      .subscribe((response: any) => {
        //console.log('admin Trending Response', response);
        this.adminTrendingLoader = false;
        this.adminTrendingPost = response.object.data;
      })
  }

  //   transform(url: string, args?: any): any {
  //     if (url) {
  //         if (url.length > 3) {
  //             let result;
  //             let match;
  //             if (match = url.match(/^(?:https?:\/\/)?(?:www\.)?([^:\/\n?=]+)/im)) {
  //                 result = match[1];
  //                 if (match = result.match(/^[^.]+\.(.+\..+)$/))
  //                     result = match[1];
  //             }
  //             return result;
  //         }
  //         return url;
  //     }
  //     return url;
  // }

  input_id
  focusFunction(id) {
    console.log('focus function', id)
    this.input_id = id;
  }

  tagSection: boolean = false;
  friendstatus = 1;
  friendsList = [];
  friend_type = 0;
  dropdownList = [];
  doSomething(event) {
    console.log(event)
    var compare_text = event
    var search_value = new Array("@");
    for (var i = 0; i < search_value.length; i++) {
      for (var j = 0; j < (compare_text.length); j++) {
        if (search_value[i] == compare_text.substring(j, (j + search_value[i].length)).toLowerCase()) {
          console.log('tag section');
          this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
            .subscribe((response: any) => {
              console.log('Friends List', response);
              if (response != null) {
                this.friendsList = response.object;
                this.dropdownList = response.object;
                this.tagSection = true
                //this.popUpOpen();
              } else {
                this.toastr.error('something went wrong');
              }
            })
        } else {
          //console.log('Comment Section')
        }
      }

    }


  }

  popUpOpen() {
    console.log('popup');
    ($('#tagModal') as any).modal('show');
  }

  dataArr = []
  myArrStr
  concvertData
  onItemSelect(item: any) {
    this.tagSection = false;
    console.log('item', item);
    this.dataArr.push(item.id);
    this.model.comment += item.name
    //this.myArrStr = JSON.stringify(this.dataArr);
    console.log('my final array', this.dataArr);
  }

  onDeSelect(item: any) {
    this.tagSection = false;
    this.dataArr.pop();
    console.log('Deselect item', item)
  }

  // videoModal(){
  //   ($('#mediumViewModal') as any).modal('show',{
  //     backdrop: 'static',
  //     keyboard: false
  //   });
  //   //($('#mediumViewModal')as any).modal({backdrop: 'static', keyboard: false})
  // }


  closeVideo() {
    console.log('clicked')
    let audioPlayer = <HTMLVideoElement>document.getElementById("videoId");
    audioPlayer.pause();

  }

  friendListData
  getFriendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        console.log('Friend List Response',response)
        this.friendListData = response.object;
      })
  }

  friendsArr = []
  arrString
  onfriendSelect(item: any) {

    console.log('item', item);
    this.friendsArr.push(item.id);
    this.arrString = JSON.stringify(this.friendsArr);
    console.log(this.arrString)
    console.log('my final array', this.friendsArr);
  }

  onFriendDeselect(item: any) {
    this.friendsArr.pop();
    console.log('Deselect item', item)
  }


  sharePostWithFriend(postId) {
    // console.log('Share In Group',postId,this.shareGroupId)
    if (this.friendsArr.length == 0) {
      this.toastr.warning('Please Choose The Friend')
    } else {
      //user_id,post_id,share_status,user_name,group_id
      console.log('share post friend data', postId, this.socialusers.id,
        this.socialusers.name, this.friendsArr.length)
      this.postService.sharePostWithFriend(postId, this.socialusers.id, this.socialusers.name, this.arrString)
        .subscribe((response: any) => {
          console.log('response for share post in group', response)
          if (response.object != null) {
            this.toastr.success('Congratulations your post has been shared successfully');
            this.posts();
            window.document.getElementById("close_share_model1").click();
          } else {
            this.toastr.error('Something Went Wrong');
          }
        })
    }
  }

  tagMember 
  getPostDetailsForTagView(id){
    console.log(id);
    this.postService.getPostDetailsById(id).subscribe((response : any) =>{
      console.log(response);
      this.tagMember = response.object;
      console.log(this.tagMember)
    },err =>{
      console.log(err)
    }
    )
  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

}


