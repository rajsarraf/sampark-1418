import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import { PostLikeService } from 'src/app/dashboard/post-like.service';
import { Socialusers } from 'src/app/home/user.model';

@Component({
  selector: 'app-common-trendingpost',
  templateUrl: './common-trendingpost.component.html',
  styleUrls: ['./common-trendingpost.component.css']
})
export class CommonTrendingpostComponent implements OnInit {

  socialusers: Socialusers;
  model: any = { comment: '' };
  
  constructor(private dashboardService : DashboardService,private postLikeService: PostLikeService,) { }

  ngOnInit() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getTrendingPostList();
  }

  trendingPost = [];
  trendingLoader: boolean = false;
  type = 0;
  getTrendingPostList() {
    this.trendingLoader = true;
    this.dashboardService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        // console.log('Trending List', response);
        this.trendingLoader = false;
        this.trendingPost = response.object;
      })
  }

  likePost(post_id) {
    //console.log('like'+post_id)
    return this.postLikeService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      //console.log('like Data ', data);

      this.getTrendingPostList();
    });

  }

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.dashboardService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }

  closeVideo() {
    console.log('clicked')
    let audioPlayer = <HTMLVideoElement>document.getElementById("videoId");
    audioPlayer.pause();

  }


}
