import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonTrendingpostComponent } from './common-trendingpost.component';

describe('CommonTrendingpostComponent', () => {
  let component: CommonTrendingpostComponent;
  let fixture: ComponentFixture<CommonTrendingpostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonTrendingpostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonTrendingpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
