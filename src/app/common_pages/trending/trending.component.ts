import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/dashboard/dashboard.service';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.css']
})
export class TrendingComponent implements OnInit {

  constructor(private dashboardService : DashboardService) { }

  ngOnInit() {
    this.getTrendingPostList();
  }

  trendingPost = [];
  trendingLoader: boolean = false;
  type = 0;
  getTrendingPostList() {
    this.trendingLoader = true;
    this.dashboardService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        // console.log('Trending List', response);
        this.trendingLoader = false;
        this.trendingPost = response.object;
      })
  }

}
