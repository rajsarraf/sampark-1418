import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { AppSetting } from '../app.setting';

@Injectable({
  providedIn: 'root'
})
export class PeopleFriendService {

  constructor(private http: HttpClient) { }

  userChanged = new Subject();

  //People Friend List

  getPeopleFriendsList(user_id) {
    const params = new HttpParams().set('user_id', user_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'suggestions_list', params);

  }

  //Add Friend

  addFriend(user_sender_id, user_receiver_id, request_status) {
    request_status = 0;
    console.log(user_sender_id, user_receiver_id, request_status)
    const params = new HttpParams().set('user_sender_id', user_sender_id).set('user_receiver_id', user_receiver_id)
      .set('request_status', request_status);
    return this.http.post<any>(AppSetting.API_ENDPOINT_PHP + 'addfriends', params);
  }

  // See Friend List

  friendList(user_id, request_status, friend_type) {

    const params = new HttpParams().set('user_id', user_id).set('request_status', request_status)
      .set('friend_type', friend_type);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'friendlist', params);

  }

  // friend Request List

  friendRequestList(user_id, request_status) {
    const params = new HttpParams().set('user_id', user_id).set('request_status', request_status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'friendrequestreceive', params);
  }

  //Confirm Friend Request

  confirmFriendRequest(user_sender_id, request_status, user_receiver_id) {
    console.log('confirm Friend Request', user_sender_id, user_receiver_id, request_status)
    const params = new HttpParams().set('user_sender_id', user_sender_id).set('request_status', request_status)
      .set('user_receiver_id', user_receiver_id);
    return this.http.post<any>(AppSetting.API_ENDPOINT_PHP + 'addfriends/confirmationrequest', params);
  }

  //Cancel Friend Request

  cancelFriendRequest(user_sender_id, user_receiver_id, request_status) {
    const params = new HttpParams().set('user_sender_id', user_sender_id).set('user_receiver_id', user_receiver_id)
      .set('request_status', request_status);
    return this.http.post<any>(AppSetting.API_ENDPOINT_PHP + 'addfriends/declinerequest', params);
  }

  //View User profile

  viewUserProfile(user_id, sender_id) {
    console.log('view profile service', user_id, sender_id)
    const params = new HttpParams().set('user_id', user_id).set('sender_id', sender_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'basicinfo', params)

  }


  // Followers List

  followersList(user_id) {
    const params = new HttpParams().set('user_id', user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'followers', params);
  }

  //Following List

  followingList(user_id) {
    const params = new HttpParams().set('user_id', user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'following', params);
  }


  // Mutual Friend List

  mutualFriendList(user_sender_id, user_receiver_id) {
    console.log('mutual friend list', user_sender_id, user_receiver_id)
    const params = new HttpParams().set('user_sender_id', user_sender_id).set('user_receiver_id', user_receiver_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'mutualfriends', params);
  }

  //hobbies

  friendHobbies(user_id, status) {
    const params = new HttpParams().set('user_id', user_id).set('status', status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'hobby_interest', params);
  }

  // Search Friend

  serachFriends(username) {
    const params = new HttpParams().set('username', username);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'search', params);
  }

  //UnFriend

  unFriend(user_sender_id, user_receiver_id) {
    console.log('unfriend in Service Console', user_sender_id, user_receiver_id)
    const params = new HttpParams().set('user_sender_id', user_sender_id).set('user_receiver_id', user_receiver_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'addfriends/unfriend', params);
  }

  //follow Friends

  followFriend(following, follower) {
    console.log('follow service', following, follower)
    const params = new HttpParams().set('follower', follower).set('following', following);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'follow', params);
  }


  //Block Friend

  blockUser(user_sender_id, user_receiver_id, request_status) {
    console.log('block detail', user_sender_id, user_receiver_id, request_status)
    const parms = new HttpParams().set('user_sender_id', user_sender_id).set('user_receiver_id', user_receiver_id).set('request_status', request_status)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'addfriends/block?', parms)

  }

  //report User

  reportUser(complainer_userid, receiver_id) {
    console.log('reportUser', complainer_userid, receiver_id)
    const params = new HttpParams().set('complainer_userid', complainer_userid).set('receiver_id', receiver_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post/report?', params);

  }

  // friend List On Behalf Of Interest

  interestFriendList(user_id, request_status, friend_type) {
    console.log(" interst based friends", user_id, request_status, friend_type)
    const params = new HttpParams().set('user_id', user_id).set('request_status', request_status).set('friend_type', friend_type);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'friendlist', params);

  }

  //friend List on Behalf of Organisation

  firendsOnBehalfOfOrganisation(user_id) {
    const params = new HttpParams().set('user_id', user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'organisation_list', params);
  }
  
  // Near By friend

  getNearByFriendList(user_id){
     const params = new HttpParams().set('user_id',user_id);
     return this.http.post(AppSetting.API_ENDPOINT_PHP+'nearest_friends',params);
  }



}
