import { Component, OnInit } from '@angular/core';
import { PeopleFriendService } from './people-friend.service';
import { Socialusers } from '../home/user.model';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { SportlightService } from '../spotlight/sportlight.service';
import { Router, NavigationEnd } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-people-friend',
  templateUrl: './people-friend.component.html',
  styleUrls: ['./people-friend.component.css']
})
export class PeopleFriendComponent implements OnInit {

  socialusers: Socialusers;
  peopleList = [];
  allFlag=false
  suggestionTitle = "See All"
  showAll(){
    
    if (this.allFlag==false){
    this.allFlag=true
    this.suggestionTitle="See Less"
  }
  else{
    this.allFlag=false  
    this.suggestionTitle="See All"
  }
}
mySubscription: any;
  model = {};
  private subscription: Subscription;

  constructor(private peopleFriendService: PeopleFriendService,
    private toastr: ToastrService, private spotLightservice: SportlightService,private router: Router) {
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
  
      this.mySubscription = this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          // Trick the Router into believing it's last link wasn't previously loaded
          this.router.navigated = false;
        }
      });
  
     }

  ngOnInit() {

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    console.log('peopleFriends By Id', this.socialusers.id);

    this.getPeopleFriendAllList();
    this.friendList();
    this.friendRequestList();
    this.mutualFriendList();
    this.friendsHobbies();
    this.interestFriendList();
    this.getTrendingPostList();
    this.firendsOnBehalfOfOrganisation();
    this.nearByFriend();

  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }


  frienaddstatus: 0;

  addPeopleFriends(id) {
    console.log(id)
    this.peopleFriendService.addFriend(this.socialusers.id, id, this.frienaddstatus)
      .subscribe((response: any) => {
        console.log(response);
        if (response != null) {
          this.peopleList = response.object;
          this.toastr.success('Your Friend request has been successfully sent');
          this.getPeopleFriendAllList();
        } else {
          this.toastr.error('something went wrong');
        }

      })
  }

  confirmstatus = 1;


  confirmRequest(id) {
    console.log(id, this.confirmstatus);
    this.peopleFriendService.confirmFriendRequest(this.socialusers.id, this.confirmstatus, id)
      .subscribe((response: any) => {
        console.log('confirm response', response);
        if (response != null) {
          this.peopleList = response.object;
          this.toastr.success('Your Friend request has been confirmed');
          this.getPeopleFriendAllList();
          this.friendList();
          this.friendRequestList();
          this.friendsHobbies();
          this.interestFriendList();
        } else {
          this.toastr.error('something went Wrong');
        }

      })
  }

  cancelRequestStatus = 2;

  cancelRequest(id) {
    this.peopleFriendService.cancelFriendRequest(this.socialusers.id, id, this.cancelRequestStatus)
      .subscribe((response: any) => {
        console.log(response);
        if (response != null) {
          this.toastr.success('Your Friend request has been cancelled ')
          this.peopleList = response.object;
          this.getPeopleFriendAllList();
          this.friendList();
          this.friendsHobbies();
          this.interestFriendList();
        } else {
          this.toastr.error('something went wrong');
        }
      })
  }

  viewProfile(id) {
    console.log('user profile details', this.socialusers.id, id)
    this.peopleFriendService.viewUserProfile(this.socialusers.id, id)
      .subscribe((response) => {
        console.log('Users Profilec Data', response);
      })
  }

 nearByFriendList = [] ;
 nearByFriend(){
   this.peopleFriendService.getNearByFriendList(this.socialusers.id)
   .subscribe((response : any)=>{
     console.log('Near By friend Response',response);
     this.nearByFriendList = response.object;
   })
 }

 suggestionList;
  getPeopleFriendAllList() {
    this.peopleFriendService.getPeopleFriendsList(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('people Friend Response list', response);
        if (response != null) {
          this.suggestionList = response.object;
          console.log(this.suggestionList)
        } else {
          this.toastr.error('something went wrong');
        }
      });
  }

  friendstatus = 1;
  requestList = [];
  friendsList = [];
  friend_type = 0;
  friendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        console.log('Friends List', response);
        if (response != null) {
          this.friendsList = response.object;
        } else {
          this.toastr.error('something went wrong');
        }
      })
  }

  requestStatus = 0;
  friendRequest = [];

  friendRequestList() {
    
    this.peopleFriendService.friendRequestList(this.socialusers.id, this.requestStatus)
      .subscribe((response: any) => {
        console.log('Friend Request', response);
        if (response != null) {
          this.friendRequest = response.object;
        } else {
          console.log('something went wrong');
        }
      })
  }

  followers = [];

  followersList() {
    this.peopleFriendService.followersList(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('followers List', response);

        this.followers = response;

      })
  }


  following = [];

  followingList() {
    this.peopleFriendService.followingList(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('Following List', response);
        this.following = response;
        console.log('object in following', this.following)
      })
  }

  id = 3;
  mutualFriendList() {
    console.log('id is', this.socialusers.id)
    this.peopleFriendService.mutualFriendList(this.socialusers.id, this.id)
      .subscribe((response: any) => {
        console.log('mutual Friend List', response)
      })
  }

  hobbiesStatus = 0;
  friendsHobbies() {
    this.peopleFriendService.friendHobbies(this.socialusers.id, this.hobbiesStatus)
      .subscribe((response: any) => {
        console.log('user Hobbies', response);
      })
  }

  serachFriend() {
    this.peopleFriendService.serachFriends(this.model)
      .subscribe((response: any) => {
        console.log('search Data', response);
      })
  }

  unFriend(id) {
    this.peopleFriendService.unFriend(this.socialusers.id, id)
      .subscribe((response: any) => {
        console.log('Unfriend Response', response);
        this.friendList();
        this.friendsHobbies();
        this.interestFriendList();
      })
  }

  followFriend(id) {
    console.log(this.socialusers.id, id)
    this.peopleFriendService.followFriend(this.socialusers.id, id)
      .subscribe((response: any) => {
        console.log('follow Response sent', response);
        this.friendList();
        this.friendsHobbies();
        this.interestFriendList();
      })
  }

  //get Tranding List

  trendingPost = [];
  type = 0;
  page : number = 1;
  getTrendingPostList() {
    this.spotLightservice.getTrendingPostList(this.type,this.page)
      .subscribe((response: any) => {
        console.log('Trending List', response);
        this.trendingPost = response.object;
      })
  }


  //Trending post like

  likePost(post_id) {

    return this.spotLightservice.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);

    });

  }

  reportUser(receiver_id) {
    console.log(this.socialusers.id, receiver_id)
    this.peopleFriendService.reportUser(this.socialusers.id, receiver_id)
      .subscribe((response: any) => {
        this.toastr.success('Report Added Successfully');
        console.log('report User', response);
        this.friendList();
        this.friendsHobbies();
        this.interestFriendList();
      })
  }

  request_status = 3;
  blockUser(receiver_id) {
    console.log(this.socialusers.id, receiver_id, this.request_status)
    this.peopleFriendService.blockUser(this.socialusers.id, receiver_id, this.request_status)
      .subscribe((response: any) => {
        console.log('block user', response);
        this.toastr.success('User Blocked');
        this.friendList();
        this.friendsHobbies();
        this.interestFriendList();;
      })
  }

  friends_list = [];

  interestFriendList() {

    this.friend_type = 1;
    this.peopleFriendService.interestFriendList(this.socialusers.id, this.friendstatus, this.friend_type)
      .subscribe((response: any) => {
        if (response != null) {
          this.friends_list = response;
          console.log('interest Based Friends', response)
        } else {
          this.toastr.error('something went wrong')
        }
      })
  }

  schoolFriends = [];
  firendsOnBehalfOfOrganisation() {
    this.peopleFriendService.firendsOnBehalfOfOrganisation(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('response organization based', response);
        this.schoolFriends = response.object;
      })
  }

}


