import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { RegistrationComponent } from './registration/registration.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UpdateprofileComponent } from './updateprofile/updateprofile.component';
import { PeopleFriendComponent } from './people-friend/people-friend.component';
import { AuthGuard } from './auth.guard';
import { SpotlightComponent } from './spotlight/spotlight.component';
import { GroupsComponent } from './groups/groups.component';
import { EventsComponent } from './events/events.component';
import { ChatComponent } from './chat/chat.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProfileComponent } from './profile/profile.component';
import { GroupdashboardComponent } from './groupdashboard/groupdashboard.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { SearchpostComponent } from './searchpost/searchpost.component';
import { Role } from './home/role';
import { AdminComponent } from './Admin/admin/admin.component';
import { SpotlightDashboardComponent } from './spotlight-dashboard/spotlight-dashboard.component';
import { AdminDashboardComponent } from './Admin/admin-dashboard/admin-dashboard.component';
import { AdminPostComponent } from './Admin/admin-post/admin-post.component';
import { AdminEventComponent } from './Admin/admin-event/admin-event.component';
import { AdminGroupComponent } from './Admin/admin-group/admin-group.component';
import { AdminGroupDashboardComponent } from './Admin/admin-group-dashboard/admin-group-dashboard.component';
import { AgonyAuntComponent } from './agony-aunt/agony-aunt.component';
import { VentBoxComponent } from './vent-box/vent-box.component';
import { AdminSpotlightDashboardComponent } from './Admin/admin-spotlight-dashboard/admin-spotlight-dashboard.component';
import { TrendingPostComponent } from './Admin/trending-post/trending-post.component';
import { HidePostsComponent } from './Admin/hide-posts/hide-posts.component';
import { AdminChatComponent } from './Admin/admin-chat/admin-chat.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AboutusComponent } from './infopages/aboutus/aboutus.component';
import { ContactusComponent } from './infopages/contactus/contactus.component';
import { CarrierComponent } from './xplore/carrier/carrier.component';
import { InterestComponent } from './xplore/interest/interest.component';
import { TrendingComponent } from './xplore/trending/trending.component';
import { EverythingComponent } from './xplore/everything/everything.component';
import { MeComponent } from './xplore/me/me.component';
import { ChatDemoComponent } from './chat-demo/chat-demo.component';
import { HomeService } from './home/home.service';
import { FirebaseChatComponent } from './firebase/firebase-chat/firebase-chat.component';



const routes: Routes = [

  { path: '', component: HomeComponent, data: { title: 'Home Componet' } },
  { path: 'home', component: HomeComponent, data: { title: ' Home Componet ' } },
  { path: 'register', component: RegisterComponent, data: { title: 'Register Componet' } },
  { path: 'registration/:id', component: RegistrationComponent, data: { title: 'Header Componet' } },
  { path: 'header', component: HeaderComponent, data: { title: 'Header Componet' } },
  { path: 'bckBtn', component: RegisterComponent, data: { title: 'Dashboard Componet' } },
  { path: 'term&condition', component: PrivacyPolicyComponent, data: {title: 'Polocyc component'}},
  {
    path: 'navigation', component: NavigationComponent, canActivate: [AuthGuard],
    data: { roles: [Role.User] }, children: [
      { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard Componet' } },
      { path: 'updateProfile', component: UpdateprofileComponent, data: { title: 'UpdateProfile Componet' } },
      { path: 'peopleFriend', component: PeopleFriendComponent, data: { title: 'PeopleFriendComponent Componet' } },
      { path: 'spotlight', component: SpotlightComponent, data: { title: 'SpotlightComponent Componet' } },
      { path: 'groups', component: GroupsComponent, data: { title: 'GroupsComponent Componet' } },
      { path: 'events', component: EventsComponent, data: { title: 'EventsComponent Componet' } },
      // { path: 'chat', component: ChatComponent, data: { title: 'ChatComponent Componet' } },
      { path: 'profile/:id', component: ProfileComponent, data: { title: 'ProfileComponent Componet' } },
      { path: 'groupdesk/:groupdetails_id', component: GroupdashboardComponent, data: { title: 'GroupDashboard Componet' } },
      { path: 'userProfile/:id', component: UserprofileComponent, data: { title: 'UserProfile Componet' } },
      { path: 'searchpost/:username', component: SearchpostComponent, data: { title: 'SerachPost Componet' } },
      { path: 'spotlight-dashboard/:id', component: SpotlightDashboardComponent, data: { title: 'SpotlightDashboard Componet' } },
      { path: 'agony-aunt/:id', component: AgonyAuntComponent, data: { title: 'AgonyAuntComponent Componet' } },
      { path: 'vent-box/:id', component: VentBoxComponent, data: { title: 'VentBoxComponent Componet' } },
      { path: 'privacy', component: PrivacyPolicyComponent, data: { title: 'PrivacyPolicyComponent Componet' } },
      { path: 'about', component: AboutusComponent, data: { title: 'AboutusComponent Componet' } },
      { path: 'contact', component: ContactusComponent, data: { title: 'ContactusComponent Componet' } },
      { path: 'carrier', component: CarrierComponent, data: { title: 'CarrierComponent Componet' } },
      { path: 'interest', component: InterestComponent, data: { title: 'InterestComponent Componet' } },
      { path: 'trending', component: TrendingComponent, data: { title: 'TrendingComponent Componet' } },
      { path: 'everything', component: EverythingComponent, data: { title: 'EverythingComponent Componet' } },
      { path: 'me', component: MeComponent, data: { title: 'MeComponent Componet' } },
      {path : 'firebase-chat', component:FirebaseChatComponent},
      {path: 'chat', component: ChatDemoComponent,canActivate: [HomeService]}
    ]
  },
  {
    path: 'admin', component: AdminComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Admin] }, children: [
      { path: 'dashboard', component: AdminDashboardComponent, data: { title: 'Dashboard Component' } },
      // { path: 'dashboard/user/:0', component: AdminDashboardComponent, data: { title: 'Dashboard Componet' } },
      { path: 'posts', component: AdminPostComponent, data: { title: 'Admin Post Component' } },
      { path: 'event', component: AdminEventComponent, data: { title: 'AdminEvent Component' } },
      { path: 'group', component: AdminGroupComponent, data: { title: 'Admin Group Component' } },
      { path: 'group/dashboard/:id', component: AdminGroupDashboardComponent, data: { title: 'AdminGroupDashboard Component' } },
      { path: 'spotlight-dashboard/:id', component: AdminSpotlightDashboardComponent, data: { title: 'AdminSpotlightDashboard Componet' } },
      { path: 'trendingpost', component: TrendingPostComponent, data: { title: 'TrendingPost Componet' } },
      { path: 'hide-post', component: HidePostsComponent, data: { title: 'HidePosts Componet' } },
      { path: 'admin-chat', component: AdminChatComponent, data: { title: 'AdminChat Componet' } },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
