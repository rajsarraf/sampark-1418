import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Socialusers } from '../home/user.model';
import { UserprofileService } from './userprofile.service';
import { PeopleFriendService } from '../people-friend/people-friend.service';
import { ToastrService } from 'ngx-toastr';
import {DashboardService} from '../dashboard/dashboard.service';
import { GroupdashboardService } from '../groupdashboard/groupdashboard.service';
import { ProfileService } from '../profile/profile.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  private sender_id: string;
  socialusers: Socialusers;
  UserProfileDetails = [];
  model: any = { comment: '' };
  showMore = false;
  showChar = 100;
  comment_message: string = "";
  comment_img;
  imgURL: any = "";
  status_id: number;
  message: string = "";
  status = false;
  fileToUpload
  
  constructor(private route: ActivatedRoute,
    private router: Router, private userProfileService: UserprofileService,
    private peopleFriendService : PeopleFriendService,private toastr : ToastrService,
    private postService:DashboardService,private groupDashboardService : GroupdashboardService,
    private profileService : ProfileService) { }

  ngOnInit() {

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.userDetails();
    this.myFriendList();
    this.sentFriendRequestList();
  }

  userPost = [];
  userDetails() {
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.sender_id = params["id"];
        console.log(this.sender_id)
        console.log('group process.......');
        this.userProfileService.viewUserProfile(this.socialusers.id, this.sender_id)
          .subscribe((response: any) => {
            if (response.object != null) {
              this.UserProfileDetails = response.object;
            console.log('kshgchfdjhkd',this.UserProfileDetails)
            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }
  Platform_type = "dashboard";
  addComment(id) {
    console.log('commnet and id', this.model.comment, id);
    var comment_type = 'normal';
    this.postService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name, this.fileToUpload,comment_type,this.Platform_type)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.model.comment = "";
        this.status = true;
        this.model = {};
        this.fileToUpload = null;
        this.imgURL = "";
        this.userDetails();
      })
  }

   //post like

   likePost(post_id) {

    return this.groupDashboardService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.userDetails();
    });

  }

  friendstatus = 1;
  requestList = [];
  friendsListData = [];
  friend_type = 0;
  friendList(id) {
    this.peopleFriendService.friendList(id, this.friendstatus,this.friend_type)
      .subscribe((response: any) => {
        console.log('Friends List', response);
        if (response != null) {
          this.friendsListData = response.object;
        } else {
          this.toastr.error('something went wrong');
        }
      })
  }

  followers = [];

  followersList(id) {
    this.peopleFriendService.followersList(id)
      .subscribe((response: any) => {
        console.log('followers List', response);
        if(response.object !=  null){
          this.followers = response.object;
        }else{
          console.log('something went wrong...No Followers Available');
        }

      })
  }

mutualFriendList = [];

  mutualFriendsProfile(id) {
    console.log('id is', this.socialusers.id)
    this.peopleFriendService.mutualFriendList(this.socialusers.id, id)
      .subscribe((response: any) => {
        console.log('mutual Friend List', response);
        this.mutualFriendList = response.object;
      })
  }
  
reportPost(id,receiver_id){
 
  console.log(id,this.socialusers.id,receiver_id)
  this.postService.reportPost(id,this.socialusers.id,receiver_id)
  .subscribe((response=>{
    console.log('response',response);
    console.log('report Added successfully');
    this.toastr.success('Report to admin successful');
   
  }))


}


  myfriendstatus = 1;
  myrequestList = [];
  myfriendsListData = [];
  myfriend_type = 0;
  friendListid : any = {};
  alreadyFriend
  myFriendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.friendstatus,this.friend_type)
      .subscribe((response: any) => {
        console.log('My Friends List', response);
        if (response != null) {
          this.myfriendsListData = response.object;
          for(this.friendListid of this.myfriendsListData){
           // console.log('Friend List id',this.friendListid.id)
            if(this.sender_id == this.friendListid.id){
              console.log("matched",this.friendListid.id);
              this.alreadyFriend = this.friendListid.id;
            }
          }
        } else {
          this.toastr.error('something went wrong');
        }
      })
  }

  div_id;
  triggerUpload(id) {
    this.div_id = id;
    console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }
  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }
  handleComment($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.model.comment += $event.emoji.native;
  }

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.postService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }

  unFriend() {
    this.peopleFriendService.unFriend(this.socialusers.id, this.sender_id)
      .subscribe((response: any) => {
        console.log('Unfriend Response', response);
        window.location.reload();
      })
  }

  frienaddstatus: 0;

  addPeopleFriends() {
   
    this.peopleFriendService.addFriend(this.socialusers.id, this.sender_id, this.frienaddstatus)
      .subscribe((response: any) => {
        console.log(response);
        if (response != null) {
          this.toastr.success('Friend request has been sent successfully');
          // window.location.reload();
          this.router.navigateByUrl('/navigation/userProfile/'+this.sender_id, { skipLocationChange: true }).then(() => {
            this.router.navigate(['/navigation/userProfile',this.sender_id]);
        }); 
        } else {
          this.toastr.error('Something went wrong');
        }

      })
  }

  sentRequestList = [];
  request = 0;
  type = 0;
  sentRequestAlready
  sentFriendRequestList(){
    this.profileService.sentFriendRequest(this.socialusers.id,this.request,this.type)
    .subscribe((response: any)=>{
      if(response.object != null){
        console.log('sent Friend Request list',response);
        this.sentRequestList = response.object;
        for(let requestid of this.sentRequestList){
           console.log('Friend List id',requestid.id)
           if(this.sender_id == requestid.id){
             console.log("matched",requestid.id);
             this.sentRequestAlready = requestid.id;
           }
         }
      }else{
        console.log('something Went wrong')
      }
      
    })
  }

  cancelSentRequest(){
    this.profileService.cancelSentRequest(this.socialusers.id,this.sender_id)
    .subscribe((response : any) =>{
      if(response != null){
        console.log('cancel request',response);
        this.toastr.success('Request has been cancelled');
       // window.location.reload();
       this.router.navigateByUrl('/navigation/userProfile/'+this.sender_id, { skipLocationChange: true }).then(() => {
        this.router.navigate(['/navigation/userProfile',this.sender_id]);
    }); 
        //this.router.navigate(['/navigation/peopleFriend']);
      }else{
        this.toastr.error('Something went wrong');
      }
     
    })
  }
  reportUser(){
    console.log(this.socialusers.id, this.sender_id)
    this.userProfileService.reportUser(this.socialusers.id, this.sender_id)
      .subscribe((response : any) => {
        this.toastr.success('Report to admin successful');
        console.log('report User', response);
       this.userDetails();
      })
  }

  request_status = 3;
  blockUser() {

    Swal.fire({
      title: '*Reason For Blocking The User',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Submit',
      showLoaderOnConfirm: true,
      preConfirm: (remark) => {
        console.log('text',remark)
        this.userProfileService.blockUser(this.socialusers.id,this.sender_id,this.request_status,remark)
        .subscribe((response : any)=>{
          console.log(response);
          if (response.object == null) {
            this.userDetails();
                  Swal.fire({
                    title: 'User Blocked Successfully',
                    icon: 'success'
                  })
                } else {
                  Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error'
                  })
                }
        })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }

}
