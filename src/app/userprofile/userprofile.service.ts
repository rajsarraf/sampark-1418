import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppSetting } from '../app.setting';

@Injectable({
  providedIn: 'root'
})
export class UserprofileService {

  constructor(private http: HttpClient) { }

  viewUserProfile(user_id, sender_id) {
    console.log('view profile service', user_id, sender_id)
    const params = new HttpParams().set('user_id', user_id).set('sender_id', sender_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'testbasicinfo', params)

  }

  //Block Friend

  blockUser(user_sender_id, user_receiver_id, request_status,remark) {
    console.log('block detail', user_sender_id, user_receiver_id, request_status)
    const parms = new HttpParams().set('user_sender_id', user_sender_id).set('user_receiver_id', user_receiver_id).set('request_status', request_status)
    .set('remark',remark);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'addfriends/block?', parms)

  }

  //report User

  reportUser(complainer_userid, receiver_id) {
    console.log('reportUser', complainer_userid, receiver_id)
    const params = new HttpParams().set('complainer_userid', complainer_userid).set('receiver_id', receiver_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post/report?', params);

  }
 
}
