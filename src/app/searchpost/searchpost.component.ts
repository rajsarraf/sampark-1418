import { Component, OnInit } from '@angular/core';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import { Router, ActivatedRoute } from "@angular/router";
import { PostLikeService } from '../dashboard/post-like.service';
import { DashboardService } from '../dashboard/dashboard.service';
import { Socialusers } from '../home/user.model';
import { Post } from '../dashboard/post';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { HomeService } from '../home/home.service';
import { AppSetting } from './../app.setting';
import { ProfileService } from '../profile/profile.service';
import { PeopleFriendService } from '../people-friend/people-friend.service';
import { GroupsService } from '../groups/groups.service';


@Component({
  selector: 'app-searchpost',
  templateUrl: './searchpost.component.html',
  styleUrls: ['./searchpost.component.css']
})
export class SearchpostComponent implements OnInit {
  // route: any;
  post: any;
  myImage;
  searchpost: Observable<Post[]>;
  searchmodel;
  username;
  PrivatePost;
  error: string;
  fileToUpload;
  socialusers: Socialusers;
  model: any = { comment: '' };
  fileupload: any = { status: '', message: '', filePath: '' };
  status = false;
  message: string = "";
  iFlag: boolean = false;
  showMore = false;
  showChar = 200;

  PostForm = new FormGroup({
    //user_id:new FormControl(12),
    content: new FormControl()

  })
  currentusers: Socialusers;
  // Post:any;


  constructor(private updateProfileService: UpdateprofileService, private route: ActivatedRoute, private router: Router,
    private postLikeService: PostLikeService, private postService: DashboardService, private toastr: ToastrService,
    private userService: HomeService,private profileService: ProfileService,private peopleFriendService : PeopleFriendService,
    private groupsService : GroupsService) { }

  ngOnInit() {

    this.currentusers = JSON.parse(localStorage.getItem('currentUser'));

    this.getUserDetails();
    this.socialUser()

    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });
    this.searchData();
    this.myFriendList();
    this.sentFriendRequestList();
  }



  searchData() {
    this.route.params.subscribe((params) => {
      console.log('paramas', params)
      this.username = params["username"]
      this.updateProfileService.searchUserPost(this.username)
        .subscribe((response: any) => {
          this.post = response
          console.log('reponse of username', this.post);
          this.searchpost = this.post.object
        })
    });
  }

  searchGroupType = "group";
  groupsearch;
  searchGroup() {

    this.route.params.subscribe((params) => {
      console.log('paramas', params)
      this.username = params["username"]
      this.updateProfileService.groupSearch(this.username, this.searchGroupType, this.currentusers.id)
        .subscribe((response: any) => {
          this.post = response
          console.log('reponse of groupsearch', this.post);
          this.groupsearch = this.post.object
        })
    });
  }
  searchType = "events"
  eventsearch;
  searchEvent() {
    this.route.params.subscribe((params) => {
      this.username = params["username"]
      this.updateProfileService.groupSearch(this.username, this.searchType, this.currentusers.id)
        .subscribe((response: any) => {
          this.post = response
          console.log('reponse of groupsearch', this.post);
          this.eventsearch = this.post.object
        });

    });
  }
  search = "people"
  peoplesearch;
  friendsid
  requestid
  searchPeople() {
    this.route.params.subscribe((parms) => {
      this.username = parms["username"]
      this.updateProfileService.groupSearch(this.username, this.search, this.currentusers.id)
        .subscribe((response: any) => {
          this.post = response
         // console.log('response of the people', this.post);
          this.peoplesearch = this.post.object;
          for(let peopleid of this.peoplesearch){
            for(let friends of this.myfriendsListData){
              //console.log('friend',friends);
              if(peopleid.id == friends.id){
                //console.log('friends found',peopleid.id);
               this.friendsid = peopleid.id;
              }
              for(let sentRequestid of this.sentRequestList){
                if(sentRequestid.id == peopleid.id){
                 //// console.log('sent Request Already');
                  this.requestid = peopleid.id;
                }
              }
            }
           // console.log('peoples id',peopleid);
          }
        })
    })
  }
  searchname = "spotlights";
  spotlightsearch;
  searchSpotlight() {
    this.route.params.subscribe((parms) => {
      this.username = parms["username"]
      this.updateProfileService.groupSearch(this.username, this.searchname, this.currentusers.id)
        .subscribe((response: any) => {
          this.post = response
          console.log('response of thr spotlight', this.post);
          this.spotlightsearch = this.post.object
        })

    })
  }
  socialUser() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));

  }
  logout() {

    this.userService.logout();
    this.router.navigate(['/home']);
    this.toastr.success('logout successfull')
  }
  mode = []
  getUserDetails() {
    this.updateProfileService.getUserDetails(this.currentusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.mode = response.object;
      })
  }

  Platform_type = "dashboard";
  addComment(id) {
    console.log(this.currentusers.id)

    var comment_type = 'normal';
    console.log(this.model.comment, id);
    this.postService.commentPost(this.socialusers.id, id, this.model.comment, this.currentusers.name, this.fileToUpload,comment_type,this.Platform_type)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.status = true;
        this.model.comment = "";
        this.searchData();
      })
  }

  likepost(post_id) {
    //console.log('like'+post_id)
    return this.postLikeService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.searchData();

    });

  }

  //delete post

  deletePost(id) {
    console.log(id)
    this.postService.deleteGroupPost(id)
      .subscribe((response: any) => {
        console.log('Delete Post Response', response);
        this.toastr.success('Post has been successfully deleted');
      })
  }

  ReportPost(id, receiver_id) {
    console.log(id, this.socialusers.id, receiver_id)
    this.postService.reportPost(id, this.socialusers.id, receiver_id)
      .subscribe((response => {
        console.log('response', response)
        this.toastr.success('Post has been reported successfully')
      }))

  }
  privatePost(data) {
    console.log('change', data)
    this.PrivatePost = data;
  }
  tagArrString = []
  Store() {
    this.status = false;
    let post = this.PostForm.value; this.postService.createPost(this.socialusers.id, post, this.fileToUpload, this.socialusers.name, this.privatePost,this.tagArrString).subscribe(

      post => {
        this.fileupload = post;
        this.iFlag = false;
        console.log(post);
        this.fileupload = post,
          this.status = true;
        this.fileToUpload = '';
        this.PrivatePost;
        this.PostForm.reset();
      },
      err => this.error = err
    )
  }
  ci(event) {
    if (event.target.files.length > 0) {
      this.myImage = event.target.files[0];
      // console.log(this.myImage);
    }
  }

  createpostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  Show() {
    this.iFlag = true;
  }
  handleSelection($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  status_id: number;
  comment_message: string = "";
  comment_img;
  imgURL: any = "";
  div_id;
  triggerUpload(id) {
    this.div_id = id;
    console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }

  showCommentEmoji: boolean = true;
  commentEmoji: boolean = false;
  closeCommentEmojiFlag: boolean = false;


  show2(id) {
    this.commentEmoji = true;
    this.closeCommentEmojiFlag = true;
    this.showCommentEmoji = false;
    this.status_id = id;
    console.log(id);

  }
  closeCommentEmoji() {
    this.commentEmoji = false;
    this.closeCommentEmojiFlag = false;
    this.showCommentEmoji = true;
  }

  handleComment($event) {
    console.log($event);
    if (this.status_id != null) {
      this.model.comment += $event.emoji.native;
    }
    //this.selectedEmoji = $event.emoji;

  }

  frienaddstatus: 0;

  addPeopleFriends(id) {
   
    this.peopleFriendService.addFriend(this.socialusers.id, id, this.frienaddstatus)
      .subscribe((response: any) => {
        console.log(response);
        if (response != null) {
          this.toastr.success('Friend Request Sent successfully');
          window.location.reload();
          this.searchPeople();
        } else {
          this.toastr.error('something went wrong');
        }

      })
  }

  myfriendstatus = 1;
  myrequestList = [];
  myfriendsListData = [];
  myfriend_type = 0;
  friendListid : any = {};
  alreadyFriend
  myFriendList() {
    this.peopleFriendService.friendList(this.socialusers.id, this.myfriendstatus,this.myfriend_type)
      .subscribe((response: any) => {
        console.log('My Friends List', response);
        if (response != null) {
          this.myfriendsListData = response.object;
          for(this.friendListid of this.myfriendsListData){
            this.alreadyFriend = this.friendListid.id;
          }
        } else {
          this.toastr.error('something went wrong');
        }
      })
  }

  sentRequestList = [];
  request = 0;
  type = 0;
  sentRequestAlready
  sentFriendRequestList(){
    this.profileService.sentFriendRequest(this.socialusers.id,this.request,this.type)
    .subscribe((response: any)=>{
      if(response.object != null){
        console.log('sent Friend Request list',response);
        this.sentRequestList = response.object;
        for(let requestid of this.sentRequestList){
          // console.log('Friend List id',requestid.id)
           this.sentRequestAlready = requestid.id;
          
         }
      }else{
        console.log('something Went wrong')
      }
      
    })
  }

  cancelSentRequest(id){
    this.profileService.cancelSentRequest(this.socialusers.id,id)
    .subscribe((response : any) =>{
      if(response != null){
        console.log('cancel request',response);
        this.toastr.success('Request has been cancelled');
        window.location.reload();
        this.searchPeople();
      }else{
        this.toastr.error('Something went wrong');
      }
     
    })
  }

  joinStatus = 1;

  joinGroup(id) {
    this.groupsService.joinGroup(id, this.socialusers.id, this.joinStatus)
      .subscribe((response: any) => {
        console.log(response);
        if (response.object == null) {
          console.log(response.message);
          this.toastr.warning(response.message);
          
        } else {
          console.log(response.message);
          this.toastr.success('Joined Group successfully');
          window.location.reload();
        }
      })
  }


}
