import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UpdateprofileComponent } from './updateprofile/updateprofile.component';
import { PeopleFriendComponent } from './people-friend/people-friend.component';
import { EventsComponent } from './events/events.component';




const appRoutes: Routes =[
    { path: '', component: HomeComponent, data: {title: 'Home Componet'}},
    { path : 'home', component : HomeComponent, data : { title : ' Home Componet ' }},
    { path: 'register', component: RegisterComponent, data: {title: 'Register Componet'}},
    { path: 'registration', component: RegistrationComponent, data: {title: 'Header Componet'}},
    { path: 'header', component: HeaderComponent, data: {title: 'Header Componet'}},
    { path: 'bckBtn', component: RegisterComponent, data: {title: 'Dashboard Componet'}},
    { path: 'dashboard', component: DashboardComponent, data: {title: 'Dashboard Componet'}},
    { path: 'updateProfile', component: UpdateprofileComponent, data: {title: 'UpdateProfile Componet'}},
    { path: 'peopleFriend', component: PeopleFriendComponent, data: {title: 'PeopleFriendComponent Componet'}},  


];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    
exports: [RouterModule]
})
export class Routing{}