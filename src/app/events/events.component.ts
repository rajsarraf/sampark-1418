import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EventsService } from './events.service';
import { Socialusers } from '../home/user.model';
import { HomeService } from '../home/home.service';
import { Router } from '@angular/router';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  socialusers:Socialusers;
  currentUser: Socialusers; 
       
  constructor(private toastr :ToastrService,private eventService:EventsService,
    private updateProfileService: UpdateprofileService
    ,private homeService:HomeService
    ,private router:Router) { }

  ngOnInit() {
   
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getUserDetails();

    this.eventList();
   
    this.getJionedEvent();
    this.getEventListBasedOnInterest();
  }

  eventListData = [];
  eventList(){
    this.eventService.getAlleventList()
    .subscribe((response: any)=>{
      console.log('Event List data',response);
      this.eventListData=response.object;
    })
  }
  model=[]
  getUserDetails() {
    this.updateProfileService.getUserDetails(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.model = response.object;
      })
  }
  logout() {
    this.homeService.logout();
    this.router.navigate(['/home']);
}
  eventDetails = []
  getEventDetailsById(id){
    // window.document.getElementById("eventModel").click();
   this.eventService.getEventDetails(id,this.socialusers.id)
   .subscribe((response : any)=>{
     if(response.object != null){
      console.log('event details by id 1....',response);
      this.eventDetails = response.object;
      

     }else{
       this.toastr.error('Something Went Wrong');
     }   
   })
  }

  eventJoinStatus=1;

  joinEvent(id){

    this.eventService.joinEventRequest(id,this.socialusers.id,this.eventJoinStatus)
    .subscribe((response : any)=>{
      if(response.object != null){
        console.log('event join response',response);
        window.document.getElementById("close_model").click();
        this.toastr.success(response.message)
        this.eventList();
        this.getJionedEvent();
      }else{
        this.toastr.error(response.message);
      }
    })
  }

  joinedEvent = [];
  getJionedEvent(){
    this.eventService.getEventListById(this.socialusers.id).subscribe((response : any)=>{
      if(response.object != null){
        console.log('Event By User id',response);
        this.joinedEvent = response.object;
      }else{
        console.log('Something Went Wrong');
      }
    })
  }

  interestEvent = [];
  getEventListBasedOnInterest(){
    this.eventService.getEventByInterest(this.socialusers.id)
    .subscribe((response : any)=>{
      if(response.object != null){
        this.interestEvent = response.object;
        console.log('Interest based Events',response);
      }else{
        console.log('Something Went Wrong');
      }
    })
  }

}
