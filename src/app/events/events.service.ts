import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppSetting } from '../app.setting';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private http : HttpClient) { }

  getAlleventList(){
    return this.http.get(AppSetting.API_ENDPOINT_PHP+'event/fetch_events');
  }

  getEventDetails(event_id,user_id){
   const params = new HttpParams().set('event_id',event_id).set('user_id',user_id);
   return this.http.post(AppSetting.API_ENDPOINT_PHP+ 'event/fetch_events_id',params);
  }

  joinEventRequest(event_id,user_id,status){
    const params = new HttpParams().set('event_id',event_id).set('user_id',user_id).set('status',status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'event/save_Invitations',params);
  }

  getEventListById(user_id){
    const params = new HttpParams().set('user_id',user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'event/fetch_event_user_id',params)
  }

  getEventByInterest(user_id){
    const params = new HttpParams().set('user_id',user_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'event/fetch_based_interest',params)
  }

}
