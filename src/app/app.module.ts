import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
//import { Routing } from './app.routing';
import { RegisterComponent } from './register/register.component'
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RegistrationComponent } from './registration/registration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationService } from './registration/registration.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomeService } from './home/home.service';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angularx-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardService } from './dashboard/dashboard.service';
import { PostLikeService } from './dashboard/post-like.service';
import { UpdateprofileComponent } from './updateprofile/updateprofile.component';
import { PeopleFriendComponent } from './people-friend/people-friend.component';
import { PeopleFriendService } from './people-friend/people-friend.service';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from './auth.guard';
import { SpotlightComponent } from './spotlight/spotlight.component';
import { GroupsComponent } from './groups/groups.component';
import { EventsComponent } from './events/events.component';
import { ChatComponent } from './chat/chat.component';
import { SportlightService } from './spotlight/sportlight.service';
import { GroupsService } from './groups/groups.service';
import { NavigationComponent } from './navigation/navigation.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { TimeAgoPipe } from 'time-ago-pipe';
import { ProfileComponent } from './profile/profile.component';
import { GroupdashboardComponent } from './groupdashboard/groupdashboard.component';
import { GroupdashboardService } from './groupdashboard/groupdashboard.service';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { DatePipe } from '@angular/common';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchpostComponent } from './searchpost/searchpost.component';
import { ErrorInterceptor } from './error.interceptor';
import { fakeBackendProvider } from './fake-backend';
import { JwtInterceptor } from './jwt.interceptor';
import { AdminComponent } from './Admin/admin/admin.component';
import { SpotlightDashboardComponent } from './spotlight-dashboard/spotlight-dashboard.component';
import { AdminDashboardComponent } from './Admin/admin-dashboard/admin-dashboard.component';
import { AdminDashboardService } from './Admin/admin-dashboard/admin-dashboard.service';
import { DataTablesModule } from 'angular-datatables';
import { AdminPostComponent } from './Admin/admin-post/admin-post.component';
import { AdminEventComponent } from './Admin/admin-event/admin-event.component';
import { SubstringPipe } from './groups/substring.pipe';
import { AdminGroupComponent } from './Admin/admin-group/admin-group.component';
import { AdminGroupDashboardComponent } from './Admin/admin-group-dashboard/admin-group-dashboard.component';
//import { SubstringPipe } from './groups/substring.pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AgonyAuntComponent } from './agony-aunt/agony-aunt.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { AgmCoreModule } from '@agm/core';
import { VentBoxComponent } from './vent-box/vent-box.component';
import { AsyncPipe } from '@angular/common';
import { environment } from '../environments/environment';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { NotificationServiceService } from './commonService/notification-service.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxPaginationModule } from 'ngx-pagination';
import { AdminSpotlightDashboardComponent } from './Admin/admin-spotlight-dashboard/admin-spotlight-dashboard.component';
import { TrendingPostComponent } from './Admin/trending-post/trending-post.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { TrendingPostService } from './Admin/trending-post/trending-post.service';
import { HidePostsComponent } from './Admin/hide-posts/hide-posts.component';
import { AdminChatComponent } from './Admin/admin-chat/admin-chat.component';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AboutusComponent } from './infopages/aboutus/aboutus.component';
import { ContactusComponent } from './infopages/contactus/contactus.component';
import { TrendingComponent } from './xplore/trending/trending.component';
import { MeComponent } from './xplore/me/me.component';
import { EverythingComponent } from './xplore/everything/everything.component';
import { CarrierComponent } from './xplore/carrier/carrier.component';
import { InterestComponent } from './xplore/interest/interest.component';
import { ShortDomainPipe } from './short-domain.pipe';
import { SafePipe } from './safe.pipe';
import { HighlightTextPipe } from './highlight-text.pipe';
import { TagPipe } from './tag.pipe';
import { MenuComponent } from './menu/menu.component';
import { MyDatePickerModule } from 'mydatepicker';
import { CommonTrendingpostComponent } from './common_pages/common-trendingpost/common-trendingpost.component';
import { QBHelper } from './helper/qbHelper';
import { Helpers } from './helper/helpers';
import { ChatDemoComponent } from './chat-demo/chat-demo.component';
import { CreateDialogComponent } from './chat-demo/create-dialog/create-dialog.component';
import { DialogsComponent } from './chat-demo/dialogs/dialogs.component';
import { EditDialogComponent } from './chat-demo/edit-dialog/edit-dialog.component';
import { MessageComponent } from './chat-demo/messages/message.component';
import { MessageService } from './chat-demo/messages/message.service';
import { FirebaseChatComponent } from './firebase/firebase-chat/firebase-chat.component';
import { ChatFormComponent } from './firebase/chat-form/chat-form.component';
import { UserListComponent } from './firebase/user-list/user-list.component';
import { UserItemComponent } from './firebase/user-item/user-item.component';
import { FeedComponent } from './firebase/feed/feed.component';


//723541261387828
//260752901060-trg98n5r1nocfll7li4q5te2oi0qdemv.apps.googleusercontent.com
export function socialConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('615907958945611')
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('66905378320-715n9olvorio2ql3ps69daqgtb4t9t7t.apps.googleusercontent.com')
      },

    ]
  );
  return config;
}

/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12
    },
    vertical: {
      position: 'bottom',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    RegisterComponent,
    RegistrationComponent,
    DashboardComponent,
    UpdateprofileComponent,
    PeopleFriendComponent,
    SpotlightComponent,
    GroupsComponent,
    EventsComponent,
    ChatComponent,
    NavigationComponent,
    TimeAgoPipe,
    ProfileComponent,
    GroupdashboardComponent,
    UserprofileComponent,
    SearchpostComponent,
    AdminComponent,
    SpotlightDashboardComponent,
    AdminDashboardComponent,
    AdminPostComponent,
    AdminEventComponent,
    SubstringPipe,
    AdminGroupComponent,
    AdminGroupDashboardComponent,
    AgonyAuntComponent,
    VentBoxComponent,
    AdminSpotlightDashboardComponent,
    TrendingPostComponent,
    HidePostsComponent,
    AdminChatComponent,
    PrivacyPolicyComponent,
    AboutusComponent,
    ContactusComponent,
    TrendingComponent,
    MeComponent,
    EverythingComponent,
    CarrierComponent,
    InterestComponent,
    ShortDomainPipe,
    SafePipe,
    HighlightTextPipe,
    TagPipe,
    MenuComponent,
    CommonTrendingpostComponent,
    ChatDemoComponent,
    CreateDialogComponent,
    DialogsComponent,
    EditDialogComponent,
    MessageComponent,
    FirebaseChatComponent,
    ChatFormComponent,
    UserListComponent,
    UserItemComponent,
    FeedComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    Ng2SearchPipeModule,
    PdfViewerModule,
    NgxExtendedPdfViewerModule,
    // Routing,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PickerModule,
    ToastrModule.forRoot(),
    DataTablesModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    // AngularFireDatabase,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgMultiSelectDropDownModule.forRoot(),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    AgmCoreModule.forRoot({
      //AIzaSyCSIFuXPQXel1splGkx5ElXoU1bL60Jn-I

      //Api Key From 1418 ---- AIzaSyAK7yK2Mj0i0dUPLCjI86Mqp4Bf6IjxOx0
      apiKey: 'AIzaSyCeWKPGfWnoIqzIpoyU8lpHTK-gTrnGhuQ'
    }),
    NotifierModule.withConfig(customNotifierOptions),
    InfiniteScrollModule,
    NgxPaginationModule,
    NgxLinkifyjsModule.forRoot(
      {
        enableHash: false, // optional - default true
        enableMention: false // optional - default true
      }
    ),
    MyDatePickerModule

  ],
  providers: [RegistrationService, HomeService, DashboardService, PostLikeService, AuthGuard,
    PeopleFriendService, SportlightService, GroupsService, GroupdashboardService, DatePipe, AsyncPipe,
    AuthService, AdminDashboardService, NotificationServiceService, TrendingPostService,
    QBHelper, MessageService,
    Helpers,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    fakeBackendProvider,
    {
      provide: AuthServiceConfig,
      useFactory: socialConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
