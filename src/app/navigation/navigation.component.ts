import { Component, OnInit } from '@angular/core';
import { Socialusers } from '../home/user.model';
import { Router, NavigationEnd } from '@angular/router';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { HomeService } from '../home/home.service';
import { Role } from '../home/role';
import { DashboardService } from '../dashboard/dashboard.service';
import { RegistrationService } from '../registration/registration.service';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  // private currentUserSubject: BehaviorSubject<Socialusers>;
  // public currentUser: Observable<Socialusers>;
  currentUser: Socialusers;
  searchmodel: any = {};
  mySubscription: any;
  form: FormGroup;
  registerForm: FormGroup;
  submitted = false;
  constructor(private router: Router, private updateProfileService: UpdateprofileService, private homeService: HomeService,
    private dashboardService: DashboardService, private registrationService: RegistrationService, fb: FormBuilder,
    private toastr : ToastrService) {

    this.homeService.currentUser.subscribe(x => this.currentUser = x);
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {

        this.router.navigated = false;
      }
    });


    //Confirm Password validation

    this.form = fb.group({
      currentpassword: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validator: this.MatchPassword // your validation method
    })

  }


  ngOnInit() {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.getUserDetails();
    this.getAdminMessage();
    this.getNotificationList();
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }




  MatchPassword(AC: AbstractControl) {
    let password = AC.get('password').value; // to get value in input tag
    let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if (password != confirmPassword) {
     // console.log('false');
      AC.get('confirmPassword').setErrors({ MatchPassword: true })
    } else {
      //console.log('true');
      return null
    }
  }
  //changePassword 
  formData
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      //console.log("Invalid form details");
      return;
    } else {
      //console.log(this.form.value);
      this.formData = this.form.value;
      this.dashboardService.changePassword(this.currentUser.id, this.formData.currentpassword, this.formData.password)
      .subscribe((response : any)=>{
         //console.log('change Password response',response);
         if(response.object == null){
           this.toastr.warning(response.message);
         }else{
           this.toastr.success(response.message);
           window.document.getElementById("closeChnagePasswordModel").click();
         }
      })
    }
  }

  model = [];

  getUserDetails() {
    this.updateProfileService.getUserDetails(this.currentUser.id)
      .subscribe((response: any) => {
        //console.log('user Details Response', response);
        this.model = response.object;
      })
  }
  getUserPost() {

    //console.log('serach by username', this.searchmodel.username)
    this.router.navigate(['/navigation/searchpost', this.searchmodel.username]);
  }

  message = [];
  getAdminMessage() {
    this.registrationService.getAdminMessage().subscribe((response: any) => {
      //console.log('Admin Message ', response)
      this.message = response.object
    })
  }

  changePassword() {
    //console.log('Change Password Function')
  }

  logout() {
    this.toastr.success('You have logged out of 1418')
    this.homeService.logout();
    this.router.navigate(['/home']);
  }

  notificationPage
  notificationList = []
  getNotificationList(){
    this.dashboardService.getNotificationList(this.currentUser.id)
    .subscribe((response : any)=>{
      console.log('Notification List Response',response);
      this.notificationList = response.object.data;
    })
  }

  goToNotification(type,id){
    if(type == 15){
      console.log(type,id);
      this.router.navigate(['/navigation/dashboard'],{queryParams:{title: id, si:true}})
    }else{
      console.log('working');
    }
    
  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }
}
