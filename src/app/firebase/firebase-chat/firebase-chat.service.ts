import { Injectable } from '@angular/core';
import { AngularFireDatabase,FirebaseListObservable } from 'angularfire2/database-deprecated';
import { chatMessage } from '../models/chat-message-model';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class FirebaseChatService {

  user: any;
  chatMessages: FirebaseListObservable<chatMessage[]>
  chatMessage: chatMessage;
  userName: Observable<String>;

  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(auth => {
      // if (auth !== undefined && auth !== null) {
      //   this.user = auth;
      // }
    })
  }

  //   getMessagesList() {  
  //     return this.db.object('Chat').valueChanges(); 
  // }

  sendMessage(msg: string) {
    const timestamp = this.getTimeStamp();
    const email = this.user.userName;
    this.chatMessages = this.getMessages();
    this.chatMessages.push({
      message: msg,
      timeSet: timestamp,
      // user: this.userName,
      email: email,
    })

  }

  getMessages(): FirebaseListObservable<chatMessage[]> {
  
    return this.db.list('messages', {
      query: {
        limitToLast : 25,
      }
    })
  }

  getTimeStamp() {
    const now = new Date()
    const date = now.getUTCFullYear() + '/' +
      (now.getUTCMonth() + 1) + '/' +
      now.getUTCDate();

    const time = now.getUTCHours() + ':' +
      now.getUTCMinutes() + ':' +
      now.getUTCSeconds();

    return (date + ' ' + time)
  }

}
