import { Component, OnInit } from '@angular/core';
import { FirebaseChatService } from '../firebase-chat/firebase-chat.service';
import { User } from '../models/user-model';

@Component({
  selector: 'app-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.css']
})
export class ChatFormComponent implements OnInit {

  message;
 user : User

  constructor(private chatService : FirebaseChatService) { }

  ngOnInit() {
  }
selectUser = 123;
  send(){
  //   const user = {
  //     email: 'rajsarraf311@gmail.com',
  //     userName: 'raj'
  // };
  // console.log(this.message,user,this.selectUser)
  // this.chatService.sendMessage(this.message);
    
    this.chatService.sendMessage(this.message)
  }

  handleSubmit(event){
    if(event.keyCode === 13){
      this.send();
    }
  }

}
