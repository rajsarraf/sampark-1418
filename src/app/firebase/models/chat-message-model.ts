export class chatMessage {
    $key?: string;
    email?: string;
    userName?: string;
    message?: string;
    timeSet?: Date = new Date();

}