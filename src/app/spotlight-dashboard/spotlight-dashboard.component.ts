import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotlightDashboardService } from './spotlight-dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { Socialusers } from '../home/user.model';
import * as $ from 'jquery';
import { SportlightService } from '../spotlight/sportlight.service';
import { UpdateprofileService } from '../updateprofile/updateprofile.service';
import { DatePipe } from '@angular/common';
import { Link, NgxLinkifyjsService } from 'ngx-linkifyjs';
import { DashboardService } from '../dashboard/dashboard.service';
import Swal from 'sweetalert2';
import { IMyDpOptions } from 'mydatepicker';

@Component({
  selector: 'app-spotlight-dashboard',
  templateUrl: './spotlight-dashboard.component.html',
  styleUrls: ['./spotlight-dashboard.component.css']
})
export class SpotlightDashboardComponent implements OnInit {

  socialusers: Socialusers;
  fileToUpload;
  model: any = {};
  spotLightForm: boolean = true;
  spotLightFormPost: boolean = false;
  data: any = {};
  userModal: any = {};
  iFlag: boolean = false;
  imgData: String = "assets/img/avtarbg.jpg";
  editModel: any = {};
  myDate = new Date();
  currentUser: Socialusers;
  subCateroryName: any = {};

  postForm = new FormGroup({
    content: new FormControl(),

  })

  public myDatePickerOptions: IMyDpOptions = {
    // other options...2019-12-12
    // dateFormat: 'dd/mm/yyyy',
    dateFormat : 'yyyy-mm-dd',
    // showInputField : false,
};

  constructor(private route: ActivatedRoute, private sportlightDashboardService: SpotlightDashboardService,
    private toastr: ToastrService, private spotLightservice: SportlightService, private updateProfileService: UpdateprofileService,
    private datePipe: DatePipe, public linkifyService: NgxLinkifyjsService, private dashboardService: DashboardService,
    private router: Router) { }

  ngOnInit() {

    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getSpotLightDetails();
    this.getCategoryList();
    this.getUserDetails();
  }

  shop(){
    Swal.fire({
      title: 'Shop',
      text: 'Coming soon',
      imageUrl: 'assets/img/Shop.svg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  getUserDetails() {
    this.updateProfileService.getUserDetails(this.socialusers.id)
      .subscribe((response: any) => {
        console.log('user Details Response', response);
        this.userModal = response.object;
        console.log('data in userModal', this.userModal)
      })
  }
  editData
  editSpotlightDetails(id) {
    console.log(id);
    this.sportlightDashboardService.getSpotLightDetails(id)
      .subscribe((response: any) => {
        this.editData = response.object.spotlightdata;
        console.log('edit spotlight response', this.editData);
        for (let spotlightDetails of this.editData) {
          this.editModel = spotlightDetails;
          this.onChageValue(this.editModel.category);
          // var dateFormate = this.datePipe.transform(this.editModel.date, "MM/dd/yyyy");
          // this.editModel.date = dateFormate;
          // console.log(this.editModel);
          // console.log(this.editModel.date);
        }
      })

  }

  onChageValue(id){
    console.log('on change Value',id);
    if(id != null && id != 0){
      this.spotLightservice.getIntrestList(id)
      .subscribe((response: any) => {
        this.subCaterory = response.object;
        console.log(response)
      })
    }
  }

  dataData


  onDateChanged(value){
    this.editModel.date = value.formatted;
    console.log('hsjdfhf',value.formatted)
  }

  editSpotlight() {

    console.log('update spotlight response for submission', this.editModel,this.editModel.date.formatted)
    // var dateFormate = this.datePipe.transform(this.editModel.date, "MM/dd/yyyy");
    // console.log('Change Date Formate', dateFormate); //output - 14-02-2019
//this.editModel.value.activity_name
    this.sportlightDashboardService.updateSpotlight(this.editModel.category, this.editModel.subcategory, this.fileToUpload, this.editModel.description,
      this.editModel.achievement, this.editModel.name,this.editModel.date, this.editModel.location, this.editModel.id)
      .subscribe((response: any) => {
        console.log(response);
        if (response.object != null) {
          this.toastr.success('Spotlight updated successfully');
          this.model = {};
          window.document.getElementById('close_spotlight_model').click();
          this.getSpotLightDetails();
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  //spotlight details
  spotLightDetails = [];
  spotLightPosts = [];
  spotlight_id
  getSpotLightDetails() {
    console.log('start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.spotlight_id = params["id"];
        console.log(this.spotlight_id)
        console.log('group process.......');
        this.sportlightDashboardService.getSpotLightDetails(this.spotlight_id)
          .subscribe((response: any) => {
            if (response.object != null) {
              console.log('spotlight response', response);
              this.spotLightDetails = response.object;
            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }

  imageUrl
  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  // spotlight post


  error = "";
  is_spotlight = 1;
  linkInfo
  key
  q
  metadata = 1
  postSpotlight() {

    var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi
    console.log('replace text data', badWords);

    var str = this.postForm.value.content;
    console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    console.log('New Words', newContent)

    this.route.params.subscribe(params => {
      if (params["id"]) {
        this.spotlight_id = params["id"];
        console.log('group process.......');
        console.log(this.spotlight_id)

        const foundLinks: Link[] = this.linkifyService.find(newContent);
        console.log('link found', foundLinks)

        if (foundLinks.length != 0 && this.fileToUpload == null) {
          for (let link of foundLinks) {
            console.log(link.value);
            this.key = '1536acb288c438aae1c7084f769c988e';
            this.q = link.value;
            this.dashboardService.getMetaData(this.key, this.q).subscribe((response: any) => {
              this.linkInfo = response;
              console.log(this.linkInfo);
              this.sportlightDashboardService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.linkInfo.title,
                this.linkInfo.image, this.linkInfo.url, this.metadata, this.spotlight_id, this.is_spotlight).subscribe(

                  post => {
                    // this.fileUpload = post;
                    this.iFlag = false;
                    console.log('response from server', post);
                    //this.fileUpload = post,
                    //this.status = true;
                    this.emojiFlag = false;
                    // this.fileToUpload ='';
                    // this.privatepost;
                    this.postForm.reset();
                    // this.posts();
                    this.router.navigateByUrl('/navigation/spotlight-dashboard/' + this.spotlight_id, { skipLocationChange: true }).then(() => {
                      this.router.navigate(['/navigation/spotlight-dashboard', this.spotlight_id]);
                    });
                  },
                  err => this.error = err
                );
            }, err => {

              this.sportlightDashboardService.createPost(this.socialusers.id, newContent, this.spotlight_id, this.fileToUpload, this.socialusers.name, this.is_spotlight)
                .subscribe((response: any) => {
                  if (response.object != null) {
                    // console.log('response data in post', post);

                    // console.log(post);
                    this.emojiFlag = false;
                    this.fileToUpload = '';
                    this.postForm.reset();
                    this.getSpotLightDetails();
                    this.router.navigateByUrl('/navigation/spotlight-dashboard/' + this.spotlight_id, { skipLocationChange: true }).then(() => {
                      this.router.navigate(['/navigation/spotlight-dashboard', this.spotlight_id]);
                    });
                  } else if (response.object == null) {
                    console.log('Something Went Wrong')
                  }

                },
                  err => this.error = err
                );
            })
          }
        } else {
          this.sportlightDashboardService.createPost(this.socialusers.id, newContent, this.spotlight_id, this.fileToUpload, this.socialusers.name, this.is_spotlight)
            .subscribe((response: any) => {
              if (response.object != null) {
                // console.log('response data in post', post);

                // console.log(post);
                this.emojiFlag = false;
                this.fileToUpload = '';
                this.postForm.reset();
                this.getSpotLightDetails();
                this.router.navigateByUrl('/navigation/spotlight-dashboard/' + this.spotlight_id, { skipLocationChange: true }).then(() => {
                  this.router.navigate(['/navigation/spotlight-dashboard', this.spotlight_id]);
                });
              } else if (response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );
        }

      } else {
        console.log('No Response Available')
      }

    })

  }

  //   error = "";
  // is_spotlight = 1;
  //   postSpotlight() {

  //      var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi
  //    console.log('replace text data', badWords);

  //     var str = this.postForm.value.content;
  //     console.log('compare text', this.postForm.value.content);
  //     var newContent = str.replace(badWords, '*****');
  //     console.log('New Words', newContent)

  //     this.route.params.subscribe(params => {
  //       if (params["id"]) {
  //         this.spotlight_id = params["id"];
  //         console.log('group process.......');
  //         console.log(this.spotlight_id)
  //         // this.status = false;
  //         let post = this.postForm.value;
  //         console.log('post content', post)
  //         if (post.content != null && this.fileToUpload == null) {
  //           this.sportlightDashboardService.createPost(this.socialusers.id, newContent, this.spotlight_id, this.fileToUpload, this.socialusers.name,this.is_spotlight)
  //             .subscribe((response: any) => {
  //               if (response.object != null) {
  //                 console.log('response data in post', post);

  //                 console.log(post);
  //                 this.emojiFlag = false;
  //                 this.fileToUpload = '';
  //                 this.postForm.reset();
  //                 this.getSpotLightDetails();
  //               } else if (response.object == null) {
  //                 console.log('Something Went Wrong')
  //               }

  //             },
  //               err => this.error = err
  //             );

  //         } else if (post.content != null && this.fileToUpload != null) {
  //           this.sportlightDashboardService.createPost(this.socialusers.id, newContent, this.spotlight_id, this.fileToUpload, this.socialusers.name,this.is_spotlight)
  //             .subscribe((response: any) => {
  //               if (response.object != null) {
  //                 console.log('response data in post', post);

  //                 console.log(post);
  //                 this.emojiFlag = false;
  //                 this.fileToUpload = '';
  //                 this.postForm.reset();
  //                 this.getSpotLightDetails();
  //               } else if (response.object == null) {
  //                 console.log('Something Went Wrong')
  //               }

  //             },
  //               //err => this.error = err
  //             );
  //         } else if (post.content == null && this.fileToUpload != null) {
  //           this.sportlightDashboardService.createPost(this.socialusers.id, newContent, this.spotlight_id, this.fileToUpload, this.socialusers.name,this.is_spotlight)
  //             .subscribe((response: any) => {
  //               if (response.object != null) {
  //                 console.log('response data in post', post);

  //                 console.log(post);
  //                 this.emojiFlag = false;
  //                 this.fileToUpload = '';
  //                 this.postForm.reset();
  //                 this.getSpotLightDetails();

  //               } else if (response.object == null) {
  //                 console.log('Something Went Wrong')
  //               }

  //             },
  //               err => this.error = err
  //             );
  //         } else {
  //           console.log('something went wrong');
  //         }
  //       }


  //     })

  //   }

  // group_id
  // linkInfo
  // key
  // q
  // metadata = 1
  // store() {
  //   var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi

  //   var str = this.postForm.value.content;
  //   //console.log('compare text', this.postForm.value.content);
  //   var newContent = str.replace(badWords, '*****');
  //   //console.log('New Words', newContent)

  //   this.route.params.subscribe(params => {
  //     if (params["groupdetails_id"]) {
  //       this.groupid = params["groupdetails_id"];
  //       this.status = false;
  //       const foundLinks: Link[] = this.linkifyService.find(newContent);
  //       console.log('link found', foundLinks)
  //       if (foundLinks.length != 0 && this.fileToUpload == null) {
  //         for (let link of foundLinks) {
  //           console.log(link.value);
  //           this.key = '1536acb288c438aae1c7084f769c988e';
  //           this.q = link.value;
  //           this.dashboardService.getMetaData(this.key, this.q).subscribe((response: any) => {
  //             this.linkInfo = response;
  //             console.log(this.linkInfo);
  //             this.groupDashboardService.createPostWithMetaData(this.socialusers.id, newContent, this.socialusers.name, this.linkInfo.title,
  //               this.linkInfo.image, this.linkInfo.url, this.metadata,this.groupid).subscribe(

  //                 post => {
  //                   this.fileUpload = post;
  //                   this.iFlag = false;
  //                   console.log('response from server', post);
  //                   //this.fileUpload = post,
  //                   this.status = true;
  //                   this.emojiFlag = false;
  //                   // this.fileToUpload ='';
  //                   // this.privatepost;
  //                   this.postForm.reset();
  //                  // this.posts();
  //                  this.router.navigateByUrl('/navigation/groupdesk/'+this.groupid, { skipLocationChange: true }).then(() => {
  //                   this.router.navigate(['/navigation/groupdesk',this.groupid]);
  //               }); 
  //                 },
  //                 err => this.error = err
  //               );
  //           },err =>{
  //             this.privatepost = 0;
  //             this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
  //               .subscribe((response: any) => {
  //                 if (response.object != null) {
  //                   // console.log(post);
  //                   //this.fileUpload = post,
  //                   this.status = true;
  //                   this.iFlag = false;
  //                   this.emojiFlag = false;
  //                   // this.fileToUpload = '';
  //                   this.postForm.reset();
  //                   this.getGroupDetails();
  //                   this.router.navigateByUrl('/navigation/groupdesk/'+this.groupid, { skipLocationChange: true }).then(() => {
  //                     this.router.navigate(['/navigation/groupdesk',this.groupid]);
  //                 }); 
  //                 } else if (response.object == null) {
  //                   console.log('Something Went Wrong')
  //                 }

  //               },
  //                 err => this.error = err
  //               );
  //           })
  //         }
  //       } else {
  //         this.privatepost = 0;
  //         this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name)
  //           .subscribe((response: any) => {
  //             if (response.object != null) {
  //               // console.log(post);
  //               //this.fileUpload = post,
  //               this.status = true;
  //               this.iFlag = false;
  //               this.emojiFlag = false;
  //               // this.fileToUpload = '';
  //               this.postForm.reset();
  //               this.getGroupDetails();
  //               this.router.navigateByUrl('/navigation/groupdesk/'+this.groupid, { skipLocationChange: true }).then(() => {
  //                 this.router.navigate(['/navigation/groupdesk',this.groupid]);
  //             }); 
  //             } else if (response.object == null) {
  //               console.log('Something Went Wrong')
  //             }

  //           },
  //             err => this.error = err
  //           );

  //       }




  //     } else {
  //       console.log('No Response From Server');
  //     }
  //   })

  // }


  //post comment

  addComment(id) {
    console.log(this.socialusers.id)

    console.log(this.model.comment, id);
    this.sportlightDashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.socialusers.name)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        //this.status = true;
        this.model.comment = "";
        this.getSpotLightDetails();

      })

  }

  //emoji
  message: string = "";
  handleSelection($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  // this.spotLightForm = !this.spotLightForm;
  // this.spotLightFormPost = !this.spotLightFormPost

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }

  //post like

  likePost(post_id) {

    return this.sportlightDashboardService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.getSpotLightDetails();
    });

  }

  //Edit spotlight

  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.spotLightservice.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
        console.log('get Category List response', response);
        this.categoryList = response;
      })
  }

  subCaterory = [];
  changeCategoryListValue(value) {
    console.log(value)
    this.spotLightservice.getIntrestList(value)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;

      })
  }

  achivementImage(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imgData = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    console.log(this.fileToUpload);
  }



}
