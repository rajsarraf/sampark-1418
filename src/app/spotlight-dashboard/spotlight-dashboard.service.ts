import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AppSetting } from '../app.setting';
import { Post } from '../dashboard/post';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpotlightDashboardService {

  constructor(private http: HttpClient) { }

  getSpotLightDetails(spotlight_id) {
    const params = new HttpParams().set('spotlight_id', spotlight_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'spotlight/show_spotlight_post', params);
  }

  createPost(user_id, content, spotlight_id, File, user_name, spotlight): Observable<Post> {
    console.log('post service', content, user_id, user_name);
    if (File != null) {
      console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('spotlight_id', spotlight_id);
      formData.append('user_name', user_name);
      formData.append('spotlight', spotlight)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', formData)
    } else {
      console.log(user_name)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('spotlight_id', spotlight_id).set('user_name', user_name)
        .set('spotlight', spotlight)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', parmas);
    }

  }
  createPostWithMetaData(user_id, content, user_name, url_title, metafile, url, metadata, spotlight_id, spotlight): Observable<Post> {
    console.log('metadata post service', user_id, content, user_name, url_title, metafile, url, metadata)
    const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('user_name', user_name)
      .set('url_title', url_title).set('metafile', metafile).set('url', url)
      .set('metadata', metadata).set('spotlight_id', spotlight_id).set('spotlight', spotlight);
    return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'store_post/', parmas);
    // }

  }

  commentPost(user_id, parent_id, comment, user_name) {
    console.log('in comment srvice', user_id, parent_id, comment, user_name)
    const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
      .set('user_name', user_name);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
  }

  storeLike(user_id, post_id) {

    console.log(post_id)

    const params = new HttpParams().set('user_id', user_id).set('post_id', post_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'like/post', params);

  }
  //update Spotlight


  // this.editModel.id, this.data.value, this.editModel.value.id, this.fileToUpload, this.editModel.description,
  //       this.editModel.achievement, this.editModel.name, this.editModel.value.activity_name, dateFormate, this.editModel.location
  updateSpotlight(category, subcategory, File, description, achievement, name
    , date, location, spotlight_id) {

    //category,subcategory,File,description

    if (File != null) {
      console.log('in service with file ', File, description, category, subcategory, date, location);
      const formData: FormData = new FormData();
      formData.append('category', category)
      formData.append('subcategory', subcategory)
      formData.append('file', File, File.name);
      //formData.append('interest', interest);
      //formData.append('content', content);
      formData.append('description', description);
      formData.append('achievement', achievement);
      formData.append('name', name);
      //formData.append('subcategoryname',subcategoryname);
      formData.append('date', date);
      formData.append('location', location);
      formData.append('spotlight_id', spotlight_id);
      //http://devsocial.nuagedigitech.com/api/spotlight/edit_spotlight_data?spotlight_id=2
      return this.http.post(AppSetting.API_ENDPOINT_PHP + 'spotlight/edit_spotlight_data', formData)
    } else {
      console.log('in service no image file', description, category, subcategory, achievement, name);
      const params = new HttpParams().set('category', category).set('subcategory', subcategory).set('description', description).set('achievement', achievement)
        .set('name', name).set('date', date).set('location', location).set('spotlight_id', spotlight_id);
      return this.http.post(AppSetting.API_ENDPOINT_PHP + 'spotlight/edit_spotlight_data', params)
    }

  }
}
