import { Component, OnInit } from '@angular/core';
import { Socialusers } from '../home/user.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  socialusers:Socialusers;
  constructor() { }

  ngOnInit() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
  }

}
