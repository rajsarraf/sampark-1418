import { Component, OnInit } from '@angular/core';
import { AdminDashboardService } from './admin-dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { Socialusers } from 'src/app/home/user.model';
import { HomeService } from 'src/app/home/home.service';
import { SportlightService } from 'src/app/spotlight/sportlight.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {


  
  constructor(private adminDashboardService: AdminDashboardService,
    private route: ActivatedRoute, private toastr: ToastrService,private router : Router,
    private homeService : HomeService,private spotLightservice : SportlightService,private datePipe: DatePipe) { }
  usersData = [];
  IFlag: boolean = false;
  JFlag: boolean = false;
  KFlag: boolean = false;
  LFlag: boolean = false;
  GroupFlag: boolean = false;
  pendingGroupFlag: boolean = false;
  postFlag: boolean = true;
  adminSpotlightFlag : boolean = false;
  InActiveGroupFlag : boolean = false;
  InActiveSpotlightFlag : boolean = false;
  removeGroupModel : boolean;
  socialusers:Socialusers;
  model : any = {};
  model1 : any = {};
  data: any = {};
  fileToUpload;
  subCateroryName: any = {};
  myDate = new Date();
  currentUser: Socialusers;
  imgData : String ="assets/img/avtarbg.jpg";
  postForm = new FormGroup({

    content: new FormControl(),

  })
  
  ngOnInit() {

    

    $("input[type='image']").click(function () {
      $("input[id='File']").click();


    });

    
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getCategoryList();

  }

  //Create Spotlight 

  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.spotLightservice.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
        console.log(response);
        this.categoryList = response;
      })
  }

  subCaterory = [];
  changeCategoryListValue(value) {
    console.log(value)
    this.spotLightservice.getIntrestList(value)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;

      })
  }

  achivementImage(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onload = (event:any)=>{
      this.imgData= event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    console.log(this.fileToUpload);
  }
  
  spotlightid
  admin_isapprove = 1;
  role = "admin";
  requestSportlight() {
    console.log('category id', this.data.value)
    console.log('subCategory id', this.model.value.id,this.model.value.activity_name)
    console.log('subcateroryname', this.subCateroryName.value)
    console.log(this.model.description, this.model.achievement)
    console.log('before change date formate', this.model.date)

    var dateFormate = this.datePipe.transform(this.model.date, "MM/dd/yyyy");
    console.log('Change Date Formate', dateFormate); //output - 14-02-2019

    this.adminDashboardService.sendsportLightRequest(this.socialusers.id, this.data.value, this.model.value.id, this.fileToUpload, this.model.description,
      this.model.achievement, this.model.name, this.model.value.activity_name, dateFormate, this.model.location,this.admin_isapprove,this.role)
      .subscribe((response: any) => {
        console.log(response);
        if (response.object != null) {
          this.toastr.success('SpotLight Request Sent Successfully');
          this.model = {};
          //this.mySpotlightForm();
          this.spotlightid = response.object.id;
          window.document.getElementById('close_spotlight_model').click();
          //this.pendingSpotlightList();
          console.log('response id for create post in spotlight',response.object.id);
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  adminName = "SPOTLIGHT_ADMIN";
  adminSpotlightStatus = 1;
  adminSpotlight = [];
  getAdminSpotlight(){
    this.adminSpotlightFlag =true;
    this.adminDashboardService.getAdminSpotlight(this.adminName,this.adminSpotlightStatus)
    .subscribe((response : any)=>{
      console.log('admin spotlight response',response);
      this.adminSpotlight = response.object;
      this.IFlag = false;
      this.JFlag = false;
      this.KFlag = false;
      this.LFlag = false;
      this.pendingGroupFlag = false;
      this.GroupFlag = false;
      this.InActiveGroupFlag = false;
      this.InActiveSpotlightFlag = false;
    })
  }

  // users status

  activeUser
  inActiveUser
  activeStatus = 1;
  user = "users";
  getActiveUsers() {
    this.adminDashboardService.getAllUsers(this.user, this.activeStatus)
      .subscribe((response: any) => {
        console.log(response);
        this.usersData = response.object;
        this.IFlag = true;
        this.JFlag = false;
        this.KFlag = false;
        this.LFlag = false;
        this.pendingGroupFlag = false;
        this.GroupFlag = false;
        this.adminSpotlightFlag = false;
        this.InActiveGroupFlag = false;
        this.InActiveSpotlightFlag = false;
      })
  }

  inActiveStatus = 0;
  usersDataInActive = [];
  getInActiveUsers() {
    this.adminDashboardService.getAllUsers(this.user, this.inActiveStatus)
      .subscribe((response: any) => {
        console.log(response);
        this.usersDataInActive = response.object;
        this.IFlag = false;
        this.JFlag = true;
        this.KFlag = false;
        this.LFlag = false;
        this.pendingGroupFlag = false;
        this.GroupFlag = false;
        this.adminSpotlightFlag = false;
        this.InActiveGroupFlag = false;
        this.InActiveSpotlightFlag = false;
      })
  }

  blockUserRemark  :any = {};
  userRole ="ADMIN";

  blockUser(id) {

    Swal.fire({
      title: '*Reason For Blocking The User',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Submit',
      showLoaderOnConfirm: true,
      preConfirm: (remark) => {
        console.log('text',remark)
        this.adminDashboardService.blockUser(this.user,this.inActiveStatus, id,this.socialusers.id,remark,this.userRole)
        .subscribe((response : any)=>{
          console.log(response);
          if (response.object != null) {
                  this.getActiveUsers();
                  Swal.fire({
                    title: 'User Blocked Successfully',
                    icon: 'success'
                  })
                } else {
                  Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error'
                  })
                }
        })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })

    // this.adminDashboardService.blockUser(this.user, this.inActiveStatus, id,this.socialusers.id,this.blockUserRemark.remark,this.userRole)
    //   .subscribe((response: any) => {
    //     console.log('block user response', response);
    //     if (response.object != null) {
    //       this.toastr.success('User Status Updated');
    //       this.getActiveUsers();
    //     } else {
    //       this.toastr.success('something went wrong');
    //     }
    //   })
  }

  unBlockUser(id) {
    this.adminDashboardService.unBlockUser(this.user, this.activeStatus, id)
      .subscribe((response: any) => {
        console.log('unblock user response', response);
        if (response.object != null) {
          this.toastr.success('User Status Updated');
          this.getInActiveUsers();
        } else {
          this.toastr.success('something went wrong');
        }
      })
  }

  userDetails = [];
  viewUser(id) {
    this.adminDashboardService.viewUser(id)
      .subscribe((response: any) => {
        if (response.object != null) {

          console.log('user Details Response', response);
          this.userDetails = response.object;
        } else {
          this.toastr.error('Something Went Wrong')
        }

      })
  }

  //spotlight status

  penddingSpotLight = []
  spotLightRequestName = "SPOTLIGHT";
  spotLightStatus = 0;
  getSpotLightRequest() {

    this.adminDashboardService.getSpotLightList(this.spotLightRequestName, this.spotLightStatus)
      .subscribe((response: any) => {
        console.log('spotLight Pending Response', response);
        this.penddingSpotLight = response.object;
        this.IFlag = false;
        this.JFlag = false;
        this.KFlag = true;
        this.LFlag = false;
        this.pendingGroupFlag = false;
        this.GroupFlag = false;
        this.adminSpotlightFlag = false;
        this.InActiveGroupFlag = false;
        this.InActiveSpotlightFlag = false;

      })
  }

  confirmSpotlight = [];
  spotLightApproveStatus = 1;
  getApproveSpotLight() {
    this.adminDashboardService.getSpotLightList(this.spotLightRequestName, this.spotLightApproveStatus)
      .subscribe((response: any) => {
        console.log('confirm spotlights response', response);
        this.confirmSpotlight = response.object;
        this.IFlag = false;
        this.JFlag = false;
        this.KFlag = false;
        this.LFlag = true;
        this.pendingGroupFlag = false;
        this.GroupFlag = false;
        this.adminSpotlightFlag = false;
        this.InActiveGroupFlag = false;
        this.InActiveSpotlightFlag = false;
      })
  }

  InActiveSpotlightStatus = 2;
   InActiveSpotlightList = [];
  getInActiveSpotlight(){
    this.adminDashboardService.getSpotLightList(this.spotLightRequestName,this.InActiveSpotlightFlag)
    .subscribe((response : any)=>{
      console.log('InActiveSpotlight Response ',response);
      this.InActiveSpotlightList = response.object;
      this.InActiveSpotlightFlag = true;
      this.IFlag = false;
      this.JFlag = false;
      this.KFlag = false;
      this.LFlag = false;
      this.pendingGroupFlag = false;
      this.GroupFlag = false;
      this.adminSpotlightFlag = false;
      this.InActiveGroupFlag = false;
    })
  }

  cancelSpotlightRequest(id) {
    this.adminDashboardService.cancelSpotlightRequest(this.spotLightRequestName, this.spotLightStatus, id)
      .subscribe((response: any) => {
        console.log('cancel spotlight request', response);
        if (response.object != null) {
          this.getApproveSpotLight();
          this.toastr.success('Spotlight status updated');
        } else {
          this.toastr.error('Something Went Wrong');
        }

      })
  }

  approveSpotlight(id) {
    this.adminDashboardService.approveSpotlightRequest(this.spotLightRequestName, this.spotLightApproveStatus, id)
      .subscribe((response: any) => {
        console.log('approveed spotlight status', response);
        if (response.object != null) {
          this.toastr.success('Spotlight Status Updated');
          this.getSpotLightRequest();
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  spotlightStatus = 0;
  removeSpotlightRequest(id){
    console.log(id,this.model1)

    Swal.fire({
      title: '*Reason For Decline The Spotlight',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Submit',
      showLoaderOnConfirm: true,
      preConfirm: (remark) => {
        console.log('text',remark)
        this.adminDashboardService.removeSpotlightRequest(this.spotLightRequestName,this.spotlightStatus,id,remark)
        .subscribe((response : any)=>{
          console.log(response);
          if (response.object != null) {
            this.getSpotLightRequest();
                  Swal.fire({
                    title: 'Spotlight Removed Successfully',
                    icon: 'success'
                  })
                } else {
                  Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error'
                  })
                }
        })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })

    // this.adminDashboardService.removeSpotlightRequest(this.spotLightRequestName,this.spotlightStatus,id,this.model1.remark)
    // .subscribe((response : any )=>{
    //   if(response.object != null){
    //     console.log('spotlight  request',response);
    //     this.toastr.success('Spotlight Request Removed');
    //     this.getSpotLightRequest();
    //     this.model1 = {};
        
    //   }else{
    //     this.toastr.error('Something Went Wrong');
    //   }
    // })
  }

  spotlightDetails = [];
  viewSpotlight(id) {
    this.adminDashboardService.getSpotlightDetails(id)
      .subscribe((response: any) => {
        if (response.object != null) {
          this.spotlightDetails = response.object;
          console.log('spotlight Details', this.spotlightDetails);
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  // Group status

  pendingGroups = [];
  groupName = "group";
  pendingGroupStatus = 0;

  getPendingGroup() {
    this.adminDashboardService.getGroupsList(this.groupName, this.pendingGroupStatus)
      .subscribe((response: any) => {
        console.log('pending Group request', response);
        this.pendingGroups = response.object;
        this.IFlag = false;
        this.JFlag = false;
        this.KFlag = false;
        this.LFlag = false;
        this.pendingGroupFlag = true;
        this.GroupFlag = false;
        this.adminSpotlightFlag = false;
        this.InActiveGroupFlag = false;
        this.InActiveSpotlightFlag = false;
      })
  }


  approvGroupRequest(id) {
    this.adminDashboardService.approveGroupRequest(this.groupName, this.approvedGroupStatus, id)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('Approve Group Creation request', response)
          this.toastr.success('Group Approved');
          this.getPendingGroup();
        } else {
          this.toastr.error('Something Went wrong');
        }
      })
  }

  approvedGroup = [];
  approvedGroupStatus = 1;
  getApprovedGroup() {
    this.adminDashboardService.getGroupsList(this.groupName, this.approvedGroupStatus)
      .subscribe((response: any) => {
        console.log('Approved group List', response);
        this.approvedGroup = response.object;
        this.IFlag = false;
        this.JFlag = false;
        this.KFlag = false;
        this.LFlag = false;
        this.pendingGroupFlag = false;
        this.GroupFlag = true;
        this.adminSpotlightFlag = false;
        this.InActiveGroupFlag = false;
        this.InActiveSpotlightFlag = false;
      })
  }

  cancelGroupRequest(id) {
    this.adminDashboardService.cancelGroupRequest(this.groupName, this.pendingGroupStatus, id)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('Cancel Group Creation request', response)
          this.toastr.success('Group Request cancel');
          this.getApprovedGroup();
        } else {
          this.toastr.error('Something Went wrong');
        }
      })
  }

  groupDetails = [];
  viewGroup(id) {
    this.adminDashboardService.viewGroupDetails(id)
      .subscribe((response: any) => {
        if (response.object != null) {
          this.groupDetails = response.object.groupdetails;
          console.log('Group details Response', this.groupDetails)
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  //InActiveGroupFlag

  inActiveGroupStatus = 2;
  inActiveGroup = [];
  inActiveGroupList(){
    this.adminDashboardService.getGroupsList(this.groupName,this.inActiveStatus)
    .subscribe((response : any)=>{
      console.log('InActive Group Response',response);
      this.inActiveGroup = response.object;
      this.InActiveGroupFlag = true;
      this.IFlag = false;
      this.JFlag = false;
      this.KFlag = false;
      this.LFlag = false;
      this.pendingGroupFlag = false;
      this.GroupFlag = false;
      this.adminSpotlightFlag = false;
      this.InActiveSpotlightFlag = false;
    })
  }

  removeStatus = 2;
  removeGroupRequest(id){
  
    Swal.fire({
      title: '*Reason Decline the Group Request...',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Submit',
      showLoaderOnConfirm: true,
      preConfirm: (remark) => {
        console.log('text',remark)
        this.adminDashboardService.removeGroupRequest(this.groupName,this.removeStatus,id,remark)
        .subscribe((response : any)=>{
          console.log(response);
          if (response.object != null) {
            this.getSpotLightRequest();
                  Swal.fire({
                    title: 'Group Remove Successfully',
                    icon: 'success'
                  })
                } else {
                  Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error'
                  })
                }
        })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  
    // console.log(id,this.model)
    // this.adminDashboardService.removeGroupRequest(this.groupName,this.removeStatus,id,this.model.remark)
    // .subscribe((response : any )=>{
    //   if(response.object != null){
    //     console.log('remove group request',response);
    //     this.toastr.success('Group Request Removed');
    //     this.getPendingGroup();
    //     this.model = {};

    //   }else{
    //     this.toastr.error('Something Went Wrong');
    //   }
    // })
  }
  welcomeMessage : any = {};
  sendWelcomMessage(){
    this.adminDashboardService.sendWelcomMessage(this.welcomeMessage.message)
    .subscribe((response : any)=>{
      if(response.object != null){
         this.toastr.success('Welcome Message Sent');
         console.log('sent welcome message',response);
         window.document.getElementById("close_group_model").click();
      }else{
        this.toastr.error('Something went Wrong');
      }
    })
  }

  logout(){
    this.homeService.logout();
    this.router.navigate(['/home']);
  }

  //Post ANd Articles

  getAllPost() {

  }
  


}
