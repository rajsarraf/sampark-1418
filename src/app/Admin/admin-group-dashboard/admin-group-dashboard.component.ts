import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AdminGroupDashboardService } from './admin-group-dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { Socialusers } from 'src/app/home/user.model';

@Component({
  selector: 'app-admin-group-dashboard',
  templateUrl: './admin-group-dashboard.component.html',
  styleUrls: ['./admin-group-dashboard.component.css']
})
export class AdminGroupDashboardComponent implements OnInit {

  socialusers: Socialusers;
  fileToUpload;
  model: any = {};
  editPostModel: any = {};
  groupUserDetails;
  groupDetailsData;
  postContent;
  privatepost;
  status = false;
  error: string;
  data: any = {};
  fileUpload: any = { status: '', message: '', filePath: '' };
  showMore : boolean = false;
  showChar = 100;
  postForm = new FormGroup({
    //user_id:new FormControl(12),
    content: new FormControl(),

  })

  constructor(private route: ActivatedRoute, private groupDashboardService: AdminGroupDashboardService,
    private toastr: ToastrService) { }

  ngOnInit() {

    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getGroupDetails();
  }



  groupid;
  groupDetails = [];
  getGroupDetails() {
    console.log('start')
    this.route.params.subscribe(params => {
      console.log(params)
      if (params["id"]) {
        this.groupid = params["id"];
        console.log(this.groupid)
        console.log('group process.......');
        this.groupDashboardService.getGroupDetails(this.groupid)
          .subscribe((response: any) => {
            if (response.object != null) {
              console.log('group details data', response);
              this.groupDetails = response.object;
            } else {
              console.log('No Data Available')
            }

          })

      }
    });
  }

  //make Group Admin

  value = 1;

  makeGroupAdmin(groupuser_id, groupdetails_id) {

    this.groupDashboardService.makeGroupAdmin(groupuser_id, groupdetails_id, this.value)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('update Member Response', response);
          //this.toastr.success('User Removed');
          this.getGroupDetails();
        } else {
          console.log('something went wrong', response);
        }
      })
  }

  removeUserFromGroup(groupuser_id, groupdetails_id) {
    this.groupDashboardService.removeGroupMember(groupuser_id, groupdetails_id)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('remove user response', response);
          this.toastr.success('User Removed');
          this.getGroupDetails();
        } else {
          console.log('something went wrong', response);
        }
      })
  }

  blockRequestStatus = 3;

  BlockUserFromGroup(groupuser_id, groupdetails_id) {
    this.groupDashboardService.blockGroupMember(groupuser_id, groupdetails_id, this.blockRequestStatus)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('Block user response', response);
          this.toastr.success('User Block Request Sent Successfully');
          this.getGroupDetails();
        } else {
          console.log('something went wrong', response);
        }
      })
  }

  // post in group
  imageUrl
  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  name="1418";
  is_admin = 1;
  group_id
  store() {

    var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi

    console.log('replace text data', badWords);

    var str = this.postForm.value.content;
    console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    console.log('New Words', newContent)

    this.route.params.subscribe(params => {
      if (params["id"]) {
        this.groupid = params["id"];
        console.log('group process.......');
        console.log(this.groupid)

        this.status = false;
        let post = this.postForm.value;
        console.log('post content', post)
        if (post.content != null && this.fileToUpload == null) {
          this.privatepost = 0;
          this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload,this.name,this.is_admin)
            .subscribe((response: any) => {
              if (response.object != null) {
                console.log('response data in post', post);
                this.fileUpload = post;
                console.log(post);

                this.status = true;

                this.postForm.reset();
                this.getGroupDetails();
              } else if (response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );

        } else if (post.content != null && this.fileToUpload != null) {
          this.groupDashboardService.createPost(this.socialusers.id, newContent, this.groupid, this.fileToUpload, this.socialusers.name,this.is_admin)
            .subscribe((response: any) => {
              if (response.object != null) {
                console.log('response data in post', post);
                this.fileUpload = post;
                console.log(post);

                this.status = true;

                this.postForm.reset();
                this.getGroupDetails();

              } else if (response.object == null) {
                console.log('Something Went Wrong')
              }

            },
              err => this.error = err
            );
        } else {
          console.log('something went wrong');
        }
      }


    })

  }

  //comment and like on post


  addComment(id) {
    console.log(this.socialusers.id)

    console.log(this.model.comment, id);
    this.groupDashboardService.commentPost(this.socialusers.id, id, this.model.comment, this.name)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.status = true;
        this.model.comment = "";
        this.getGroupDetails();
      })

  }

  //post like

  likePost(post_id) {

    return this.groupDashboardService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.getGroupDetails();
    });

  }

}
