import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppSetting } from 'src/app/app.setting';
import { Post } from 'src/app/dashboard/post';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminGroupDashboardService {

  constructor(private http:HttpClient) { }

  getGroupDetails(groupid) {
    const params = new HttpParams().set('groupid', groupid)
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/fetch_group_details', params);
  }

  makeGroupAdmin(user_id, groupdetailsid, is_admin) {
    const params = new HttpParams().set('user_id', user_id).set('groupdetailsid', groupdetailsid)
      .set('is_admin', is_admin)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_create_admin', params);
  }

  removeGroupMember(user_id, groupdetailsid) {
    const params = new HttpParams().set('user_id', user_id).set('groupdetailsid', groupdetailsid);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'group/group_remove_member', params)
  }

  blockGroupMember(user_id, groupdetailsid,status){
    const params = new HttpParams().set('user_id', user_id).set('groupdetailsid', groupdetailsid)
      .set('status', status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/block_member',params)
  }

  createPost(user_id, content, group_id, File, user_name,is_admin): Observable<Post> {
    console.log('post service', content, user_id, user_name,is_admin);
    if (File != null) {
      console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('group_id', group_id);
      formData.append('user_name', user_name);
      formData.append('is_admin', is_admin);


      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', formData)
    } else {
      console.log(user_name)
      const parmas = new HttpParams().set('user_id', user_id).set('content', content).set('group_id', group_id).set('user_name', user_name)
      .set('is_admin', is_admin);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'group/post_in_group', parmas);
    }

  }

  commentPost(user_id, parent_id, comment, user_name) {
    console.log('in comment srvice', user_id, parent_id, comment, user_name)
    const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
      .set('user_name', user_name);
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
  }

  storeLike(user_id, post_id) {

    console.log(post_id)

    const params = new HttpParams().set('user_id', user_id).set('post_id', post_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'like/post', params);

  }

}
