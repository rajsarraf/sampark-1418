import { Component, OnInit } from '@angular/core';
import { Socialusers } from 'src/app/home/user.model';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AdminGroupService } from './admin-group.service';
import { FormGroup, FormControl } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-admin-group',
  templateUrl: './admin-group.component.html',
  styleUrls: ['./admin-group.component.css']
})
export class AdminGroupComponent implements OnInit {

  groupmodel: any = {};
  data: any = {};
  fileToUpload;
  socialusers: Socialusers;
  model: boolean;
  friendListModel: boolean;
  category: any = {};
  interest: any = {};

  user: any;
  status = false;
  CloseBtn: boolean = true;
  post: any;
  postObj;
  editPostModel: any = {};
  fileUpload: any = { status: '', message: '', filePath: '' };
  error: string;
  iFlag: boolean = false;
  message: string = "";
  status_id: number;
  searchText;
  postFlag: boolean = true;

  postForm = new FormGroup({

    content: new FormControl(),

  })

  constructor(private groupsService: AdminGroupService, private toastr: ToastrService,
    private router: Router) { }

  private subscription: Subscription;

  dropdownList = [];
  selectedItems = { id: "", groupname: "" };
  dropdownSettings = {};


  ngOnInit() {

    this.getAllGroupsList();

    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });



    // this.dropdownList = [
    //   { item_id: 1, item_text: 'Mumbai' },
    //   { item_id: 2, item_text: 'Bangaluru' },
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' },
    //   { item_id: 5, item_text: 'New Delhi' }
    // ];
    // this.selectedItems = [
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' }
    // ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'groupname',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };



    this.getUserInfo();
    this.getCategoryList();


  }

  // this.interestArr.push(subCategoryList.id);
  //   console.log(this.interestArr)

  dataArr = []
  myArrStr
  concvertData
  onItemSelect(item: any) {
    console.log('item', item);
    this.dataArr.push(item);
    this.myArrStr = JSON.stringify(this.dataArr);
    console.log('my final array', this.myArrStr);
    // console.log('converted my string',JSON.parse(myArrStr));
    // console.log('selected Items',this.dataArr)
  }

  onDeSelect(item: any) {
    this.dataArr.pop();
    console.log('Deselect item', item)
  }
  onSelectAll(items: any) {
    console.log(items)
    this.myArrStr = JSON.stringify(items);
    console.log('select all section', this.myArrStr);
  }

  name = "1418";
  store() {
    console.log('group post items', this.dataArr)
    this.status = false;
    let post = this.postForm.value;
    this.groupsService.createPost(this.socialusers.id, post, this.fileToUpload, this.name, this.is_admin, this.myArrStr).subscribe(

      post => {
        console.log('admin post', post)
        this.fileUpload = post;
        this.iFlag = false;
        console.log(post);
        this.status = true;
        this.postForm.reset();
        // this.router.navigate(['/admin/posts']);
      },
      err => this.error = err
    );

  }

  getUserInfo() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));

  }


  groupIcon(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  requestStatus = 1;
  is_admin = 1;
  createGropupRequest() {
    console.log(this.socialusers.id, this.category.value)
    this.groupsService.createGroupRequest(this.socialusers.id, this.category.value,
      this.groupmodel.name, this.groupmodel.description,
      this.fileToUpload, this.requestStatus, this.data.value, this.interest.value, this.is_admin).subscribe((response: any) => {
        console.log(response);
        if (response.object != null) {
          this.toastr.success('Group Creation Request Send successfully');
          this.getUserInfo();
          this.router.navigate(['/admin/group']);
          this.getAllGroupsList();
          window.document.getElementById("close_group_model").click();
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  openGroup() {
    console.log('clicked')
    this.friendListModel = true;
  }


  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.groupsService.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
        console.log('interest category', response);
        this.categoryList = response.object;
      })
  }

  subCaterory = [];
  changeCategoryListValue(value) {
    console.log(value)
    this.groupsService.getIntrestList(value)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;
      })
  }


  groupList = [];
  approvedGroupStatus = 1;
  groupName = "group";
  getAllGroupsList() {
    this.groupsService.getGroupsList(this.groupName, this.approvedGroupStatus)
      .subscribe((response: any) => {
        console.log('group List', response);
        this.groupList = response.object;
        this.dropdownList = response.object;
      })
  }




  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }

  show2(id) {
    this.status_id = id;

  }


  handleSelection($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  // handleComment($event) {
  //   console.log($event);
  //   //this.selectedEmoji = $event.emoji;
  //   this.model.comment += $event.emoji.native;
  // }



  seeAll: boolean = true;
  SeeAllFun() {
    this.seeAll = !this.seeAll
  }
  seeAll1: boolean = true;
  SeeAllFun1() {
    this.seeAll1 = !this.seeAll1
  }

  //your groups seeAll
  seeAllgroup: boolean = true;
  SeeAllFungroup() {
    this.seeAllgroup = !this.seeAllgroup
  }
  seeAll1group: boolean = true;
  SeeAllFun1group() {
    this.seeAll1group = !this.seeAll1group
  }

  //profile based groups
  seeAllProfile: boolean = true;
  SeeAllFunProfile() {
    this.seeAllProfile = !this.seeAllProfile
  }
  seeAll1Profile: boolean = true;
  SeeAllFun1Profile() {
    this.seeAll1Profile = !this.seeAll1Profile
  }

}
