import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { AppSetting } from 'src/app/app.setting';
import { Post } from 'src/app/dashboard/post';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminGroupService {

  constructor(private http : HttpClient) { }

  //request for group creation

  createGroupRequest(user_id, groupmastersid, groupname, groupdescription, File, status, agelimit,interest_id,is_admin) {
   if(File != null){
    const formData = new FormData();
    formData.append('user_id', user_id);
    formData.append('groupmastersid', groupmastersid);
    formData.append('groupname', groupname);
    formData.append('groupdescription', groupdescription);
    formData.append('file', File, File.name);
    formData.append('status', status);
    formData.append('agelimit', agelimit);
    formData.append('interest_id',interest_id);
    formData.append('is_admin',is_admin);
    console.log(formData,"tetting formdata")
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/create_group', formData);
   }else{
     const params =  new HttpParams().set('user_id',user_id).set('groupmastersid',groupmastersid)
     .set('groupname',groupname).set('groupdescription',groupdescription).set('status',status)
     .set('agelimit',agelimit).set('interest_id',interest_id).set('is_admin',is_admin);
     return this.http.post(AppSetting.API_ENDPOINT_PHP+'group/create_group',params);
   }
  }

  getCategoryList(status){
    const params = new HttpParams().set('status',status)
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'hobby_interestlist',params)
  }


  getIntrestList(category_id){
    console.log('category_id',category_id)
    const params = new HttpParams().set('category_id',category_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'spotlight/subcategory_list',params)
  }

  getGroupsList(name,status){
    const params =  new HttpParams().set('name',name).set('status',status);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'Detailslists',params);
  }

  createPost(user_id, post: Post, File, user_name,is_admin,group_id): Observable<Post> {
    console.log('post service', post, user_id, user_name,group_id);
    if (File != null) {
      console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', post.content);
      formData.append('user_name', user_name);
      formData.append('is_admin',is_admin);
      formData.append('group_id',group_id);

      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'admin/admin_group_post', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
    } else {
      console.log(user_name)
      const parmas = new HttpParams().set('user_id', user_id).set('content', post.content).set('user_name', user_name).set('is_admin',is_admin)
      .set('group_id',group_id);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'admin/admin_group_post', parmas);
    }

  }

  private getEventMessage(event: HttpEvent<any>, formData) {

    switch (event.type) {
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
        break;
      case HttpEventType.Response:
        return this.apiResponse(event);
        break;
      default:
        return `File "${formData.get('file').name}" surprising upload event: ${event.type}.`;
    }
  }
  private fileUploadProgress(event) {
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    return event.body;
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      
      console.error('An error occurred:', error.error.message);
    } else {
      
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    
    return throwError('Something bad happened. Please try again later.');
  }

}
