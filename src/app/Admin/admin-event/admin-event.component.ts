import { Component, OnInit } from '@angular/core';
import { AdminEventService } from './admin-event.service';
import { DatePipe } from '@angular/common';
import { Socialusers } from 'src/app/home/user.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-event',
  templateUrl: './admin-event.component.html',
  styleUrls: ['./admin-event.component.css']
})
export class AdminEventComponent implements OnInit {

  model: any = {};
  data: any = {};
  subCateroryName: any = {};
  fileToUpload;
  socialusers:Socialusers;
  imgData : String ="assets/img/avtarbg.jpg";

  constructor(private adminEventService : AdminEventService,private datePipe: DatePipe,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getCategoryList();
    this.eventList();
  }

  categoryList = [];
  statusInterest = 1;
  getCategoryList() {
    this.adminEventService.getCategoryList(this.statusInterest)
      .subscribe((response: any) => {
        console.log('interest category', response);
        this.categoryList = response.object;
      })
  }

  subCaterory = [];
  changeCategoryListValue(value) {
    console.log(value)
    this.adminEventService.getIntrestList(value)
      .subscribe((response: any) => {
        console.log(response);
        this.subCaterory = response.object;
      })
  }

  subCategoryName
  changeCategoryName(value){
    this.subCategoryName = value;
    console.log('Sub category name',this.subCategoryName);
  }

  eventImage(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onload = (event:any)=>{
      this.imgData= event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    console.log(this.fileToUpload);
    
  }

  //name,description,category,sub_category,sub_categoryname,address,event_date,event_time,prize,
  //live_link,landmark,organized_by,email,phone,live_location,File
  createEvent(){
    console.log('create event parammeters',this.model.eventName,this.model.description,this.data.value,this.model.value,
    this.subCategoryName,this.model.location,dateFormate,this.model.time,this.model.prize,
    this.model.link,this.model.landMark,this.model.organizer,this.model.email,this.model.phone,this.model.liveLocation,
    this.fileToUpload,this.socialusers.id);
    
    ;
    var dateFormate = this.datePipe.transform(this.model.date, "MM/dd/yyyy");
    console.log('Change Date Formate', dateFormate); 
    var currnetDate = new Date();   
    console.log('Current date',currnetDate)
    var EventDate = new Date(this.model.date); 
    console.log("event date",EventDate);
    if (currnetDate < EventDate) {  
       console.log('Event date valid');
       this.adminEventService.createEvent(this.model.eventName,this.model.description,this.data.value,this.model.value,
      this.subCategoryName,this.model.city,this.model.state,this.model.pincode,dateFormate,this.model.time,this.model.prize,
      this.model.link,this.model.landMark,this.model.organizer,this.model.email,this.model.phone,this.model.liveLocation,
      this.fileToUpload,this.model.address,this.socialusers.id)
      .subscribe((response : any)=>{
        if(response.object != null){
          console.log('create Event response from server',response)
          this.toastr.success(response.message);
          window.document.getElementById("close_group_model").click();
          this.eventList();
        }else{
          this.toastr.error(response.message);
        }
       
      })
  }else {  
    this.toastr.warning('Please Enter Valid Event Date')
  }  
  
    
  }

  eventListData = [];
  eventList(){
    this.adminEventService.getAlleventList()
    .subscribe((response: any)=>{
      console.log('Event List data',response);
      this.eventListData=response.object;
    })
  }

  eventDetails = []
  getEventDetailsById(id){
   this.adminEventService.getEventDetails(id)
   .subscribe((response : any)=>{
     if(response.object != null){
      console.log('event details by id',response);
      this.eventDetails = response.object;
     }else{
       this.toastr.error('Something Went Wrong');
     }
    
     
   })
  }

}
