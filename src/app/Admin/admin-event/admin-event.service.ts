import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppSetting } from 'src/app/app.setting';

@Injectable({
  providedIn: 'root'
})
export class AdminEventService {

  constructor(private http : HttpClient) { }

  getCategoryList(status){
    const params = new HttpParams().set('status',status)
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'hobby_interestlist',params)
  }


  getIntrestList(category_id){
    console.log('category_id',category_id)
    const params = new HttpParams().set('category_id',category_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'spotlight/subcategory_list',params)
  }

  //this.model.city,this.model.state,this.model.pincode
  createEvent(name,description,category,sub_category,sub_categoryname,city,state,pincode,event_date,event_time,prize,
    live_link,landmark,organized_by,email,phone,live_location,File,address,user_id){
    
      const formData = new FormData();

    if(File != null){
      formData.append('event_name',name);
      formData.append('description',description);
      formData.append('category',category);
      formData.append('sub_category',sub_category);
      formData.append('sub_categoryname',sub_categoryname);
      formData.append('city',city);
      formData.append('state',state);
      formData.append('pincode',pincode);
      formData.append('event_date',event_date);
      formData.append('event_time',event_time);
      formData.append('prize',prize);
      formData.append('live_link',live_link);
      formData.append('landmark',landmark);
      formData.append('organized_by',organized_by);
      formData.append('email',email);
      formData.append('phone',phone);
      formData.append('live_location',live_location);
      formData.append('file',File,File.name);
      formData.append('address',address);
      formData.append('user_id',user_id);
      return this.http.post(AppSetting.API_ENDPOINT_PHP+'event/create_events',formData);
    }else{
      const params = new HttpParams().set('event_name',name).set('description',description).set('category',category)
      .set('category',category).set('sub_category',sub_category).set('sub_categoryname',sub_categoryname)
      .set('city',city).set('state',state).set('pincode',pincode).set('event_date',event_date).set('event_time',event_time).set('prize',prize)
      .set('live_link',live_link).set('landmark',landmark).set('organized_by',organized_by).set('email',email).set('phone',phone)
      .set('live_location',live_location).set('address',address).set('user_id',user_id);
      return this.http.post(AppSetting.API_ENDPOINT_PHP+'event/create_events',params);
    }
   
  }


  getAlleventList(){
    return this.http.get(AppSetting.API_ENDPOINT_PHP+'event/fetch_events');
  }

  getEventDetails(event_id){
   const params = new HttpParams().set('event_id',event_id);
   return this.http.post(AppSetting.API_ENDPOINT_PHP+ 'event/fetch_events_id',params);
  }


}
