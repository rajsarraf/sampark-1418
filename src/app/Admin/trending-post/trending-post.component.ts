import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminPostService } from '../admin-post/admin-post.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Socialusers } from 'src/app/home/user.model';
import { TrendingPostService } from './trending-post.service';

@Component({
  selector: 'app-trending-post',
  templateUrl: './trending-post.component.html',
  styleUrls: ['./trending-post.component.css']
})
export class TrendingPostComponent implements OnInit {

  socialusers: Socialusers;
  user: any;
  status = false;
  CloseBtn: boolean = true;
  post: any;
  postObj;
  model: any = { comment: '' };
  editPostModel: any = {};
  fileToUpload;
  fileUpload: any = { status: '', message: '', filePath: '' };
  error: string;
  iFlag: boolean = false;
  message: string = "";
  status_id: number;
  searchText;
  showEmoji: boolean = false;
  showEmoji2: boolean = true;
  comment_img;
  imgURL: any = "";
  public href: string = "";

  allPost;

  postForm = new FormGroup({

    content: new FormControl(),

  })

  editForm = new FormGroup({
    content: new FormControl(),

  })
  constructor(private postService: AdminPostService, private router: Router,
    private toastr: ToastrService, private trendingPostService: TrendingPostService) { }

  ngOnInit() {
    this.href = this.router.url;
    console.log('current Url', this.router.url);
    console.log('widow location', window.location.href)
    var newURL = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    console.log('url with ', newURL)
    $("input[type='image']").click(function () {
      $("input[id='File']").click();
    });

    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.getAllPostList();
    this.getTrendingPostList();
  }


  trendingPost = [];

  type = 0;
  getTrendingPostList() {
    this.postService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        console.log('Trending List', response);
        this.trendingPost = response.object;
      })
  }

  createPostImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  name = "1418";
  privatepost = 1;
  is_admin = 1;
  admin_trending = 1;
  store() {

    var badWords = /2g1c|acrotomophilia|alabama hot pocket|alaskan pipeline|anal|anilingus|anus|apeshit|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|baby juice|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastard|bastardo|bastinado|bbw|bdsm|beaner|beaners|beaver cleaver|beaver lips|bestiality|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|bitches|black cock|blonde action|blonde on blonde action|blowjob|blow job|blow your load|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bullshit|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|coon|coons|creampie|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dendrophilia|dick|dildo|dingleberry|dingleberries|dirty pillows|dirty sanchez|doggie style|doggiestyle|doggy style|doggystyle|dog style|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|dry hump|dvda|eat my ass|ecchi|ejaculation|erotic|erotism|escort|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingerbang|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fuckin|fucking|fucktards|fudge packer|fudgepacker|futanari|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|god damn|gokkun|golden shower|goodpoop|goo girl|goregasm|grope|group sex|g-spot|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot carl|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jelly donut|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|mf|milf|missionary position|mofo|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nigga|nigger|nig nog|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|paki|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|pissing|piss pig|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poon|poontang|punany|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queaf|queef|quim|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|sadism|santorum|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shitblimp|shitty|shota|shrimping|skeet|slanteye|slut|s&m|smut|snatch|snowballing|sodomize|sodomy|spic|splooge|splooge moose|spooge|spread legs|spunk|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet wand|vorarephilia|voyeur|vulva|wank|wetback|wet dream|white power|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia/gi

    console.log('replace text data', badWords);

    var str = this.postForm.value.content;
    console.log('compare text', this.postForm.value.content);
    var newContent = str.replace(badWords, '*****');
    console.log('New Words', newContent)

    this.status = false;
    let post = this.postForm.value;
    this.trendingPostService.createPost(this.socialusers.id, newContent, this.fileToUpload, this.name, this.privatepost, this.is_admin, this.admin_trending).subscribe(

      post => {
        console.log('admin post', post)
        this.fileUpload = post;
        this.iFlag = false;
        console.log(post);
        this.status = true;
        this.postForm.reset();
        this.getAllPostList();
        //this.router.navigate(['/admin/posts']);
      },
      err => this.error = err
    );

  }

  // Add Comment
  addComment(id) {
    console.log(this.socialusers.id)
    console.log(this.model.comment, id);
    this.postService.commentPost(this.socialusers.id, id, this.model.comment, this.name, this.fileToUpload)
      .subscribe((response: any) => {
        console.log('comment Response', response)
        this.status = true;
        this.model.comment = "";
        this.getAllPostList();
        //  this.router.navigate(['/admin/posts']);
      })

  }

  likePost(post_id) {
    //console.log('like'+post_id)
    return this.postService.storeLike(this.socialusers.id, post_id).subscribe((data) => {
      console.log('like Data ', data);
      this.getAllPostList();
    });

  }

  //get All Post

  page = 1;
  getAllPostList() {
    console.log('get All Post', this.page, this.socialusers.id)
    this.trendingPostService.getAllPost(this.page, this.socialusers.id).subscribe((response: any) => {
      console.log('get All Post', response)
      this.allPost = response.object.data;
      console.log('post Data', this.allPost)
    })
  }

  //share post
  share_status = 1;
  sharePost(id) {
    console.log('post_id for share post', id)
    this.postService.shareUserPost(this.socialusers.id, id, this.share_status, this.name)
      .subscribe((response: any) => {
        if (response.object != null) {
          console.log('share post response', response);
          this.toastr.success('Post Share Successfully');
          this.getAllPostList();
        } else {
          this.toastr.error('Something Went Wrong');
        }
      })
  }

  //delete post

  deletePost(id) {
    console.log(id)
    this.postService.deletePost(id)
      .subscribe((response: any) => {
        console.log('Delete post Response', response)
        this.toastr.success('Post Delete Successfully');
        this.getAllPostList();
      })
  }

  //Report post

  reportPost(id, receiver_id) {
    console.log(id, this.socialusers.id, receiver_id)
    this.postService.reportPost(id, this.socialusers.id, receiver_id)
      .subscribe((response: any) => {
        console.log('response', response);
        console.log('report Added successfully');
        this.toastr.success('Report Added');
        this.getAllPostList
      })
  }

  editData = [];
  editFormData
  showPost(id) {
    console.log(id);
    this.postService.showPostEdit(id)
      .subscribe((response: any) => {
        console.log('showpost', response)
        this.editPostModel = response.object;

      })
  }


  imageToUpload;
  editPostImage(files: FileList) {
    this.imageToUpload = files.item(0);
    console.log('Edit Image', this.imageToUpload);
  }


  editUserPost(id, content) {
    let formData = this.editForm.getRawValue()
    console.log('Edit Image in Typescript', id, this.imageToUpload, content)
    this.postService.updatePost(id, this.imageToUpload, content)
      .subscribe((response: any) => {
        if (response.object != null) {
          this.toastr.success('post updated Successfully');
          content = {};
          this.getAllPostList();
          window.document.getElementById("close_model").click();
        } else {
          this.toastr.warning('Something Went wrong');
        }

      })
  }

  postData = []

  getPostDetailsById(id) {
    console.log(id);
    this.postService.getPostDetailsById(id).subscribe((response: any) => {
      console.log('post details by id response', response);
      this.postData = response.object;
    })
  }

  div_id;
  triggerUpload(id) {
    this.div_id = id;
    console.log('this is div_id', this.div_id);
    var comment_id = 'comment_file' + id;
    $("#comment_file" + id).click();
  }

  imagePath: FileList;
  createCommentImage(files: FileList) {
    var reader = new FileReader();
    this.imagePath = files;
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    this.comment_img = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  close_img() {
    this.imgURL = "";
    this.fileToUpload = null;

  }

  emojiFlag: boolean = false;
  showEmojiFlag: boolean = true;
  closeEmojiFlag: boolean = false;
  show() {
    this.emojiFlag = true;
    this.showEmojiFlag = false;
    this.closeEmojiFlag = true;
    // this.iFlag = !this.iFlag;
  }
  closeEmoji() {
    this.closeEmojiFlag = false;
    this.showEmojiFlag = true;
    this.emojiFlag = false;
  }


  show2(id) {
    this.status_id = id;
    this.showEmoji2 = false;
    this.showEmoji = true;

  }
  hideEmoji(id) {
    this.showEmoji2 = true;
    this.showEmoji = false;
  }


  handleSelection($event) {
    console.log($event);
    //this.selectedEmoji = $event.emoji;
    this.message += $event.emoji.native;
  }

  handleComment($event) {
    console.log($event);
    this.showEmoji2 = true;
    //this.selectedEmoji = $event.emoji;
    this.model.comment += $event.emoji.native;
  }

}
