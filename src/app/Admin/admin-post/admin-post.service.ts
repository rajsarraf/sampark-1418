import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders, HttpParams, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Post } from 'src/app/dashboard/post';
import { AppSetting } from 'src/app/app.setting';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminPostService {

  constructor(private http : HttpClient) { }

  createPost(user_id, content, File, user_name,visibility,is_admin): Observable<Post> {
    console.log('post service', content, user_id, user_name);
    if (File != null) {
      console.log('file-name', File.name);
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('user_id', user_id);
      formData.append('content', content);
      formData.append('user_name', user_name);
      formData.append('visibility', visibility);
      formData.append('is_admin',is_admin);

      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post/', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
    } else {
      console.log(user_name,visibility)
      const parmas = new HttpParams().set('user_id', user_id).set('content',content).set('user_name', user_name).set('visibility',visibility).set('is_admin',is_admin)
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post/', parmas);
    }

  }

  private getEventMessage(event: HttpEvent<any>, formData) {

    switch (event.type) {
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
        break;
      case HttpEventType.Response:
        return this.apiResponse(event);
        break;
      default:
        return `File "${formData.get('file').name}" surprising upload event: ${event.type}.`;
    }
  }
  private fileUploadProgress(event) {
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    return event.body;
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      
      console.error('An error occurred:', error.error.message);
    } else {
      
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    
    return throwError('Something bad happened. Please try again later.');
  }

  //comment

  commentPost(user_id, parent_id, comment, user_name, File) {
    if (File == null) {
      console.log('in comment srvice', user_id, parent_id, comment, user_name)
      const params = new HttpParams().set('parent', parent_id).set('user_id', user_id).set('content', comment)
        .set('user_name', user_name);
      return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post', params);
    } else {
      const formData: FormData = new FormData();
      formData.append('file', File, File.name);
      formData.append('parent', parent_id);
      formData.append('user_id', user_id);
      formData.append('content', comment);
      formData.append('user_name', user_name);
      return this.http.post<Post>(AppSetting.API_ENDPOINT_PHP + 'post/', formData, {
        reportProgress: true,
        observe: 'events',
      }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );

    }
  }

  //like

  storeLike(user_id, post_id) {
    console.log(post_id)
    const params = new HttpParams().set('user_id', user_id).set('post_id', post_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'like/post', params);

  }

  //get All Post

  
  getAllPost(status){
    const params = new HttpParams().set('status',status)
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'admin/admin_posts',params);
  }

  //get All Trending Post

  getTrendingPostList(type) {
    const params = new HttpParams().set('type',type)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'spotlight/trendingpost',params);
  }

  //share post

  shareUserPost(user_id,post_id,share_status,user_name){
    const params = new HttpParams().set('user_id',user_id).set('post_id',post_id).set('share_status',share_status)
    .set('user_name',user_name);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+ 'post/post_share',params);
  }

  //delete post

  deletePost(post_id) {
    console.log('post delete in service', post_id)
    return this.http.delete(AppSetting.API_ENDPOINT_PHP + 'post/destory_post/' + post_id)
  }

  //report post

  reportPost(post_id,complainer_userid,receiver_id){
    console.log("report",post_id,complainer_userid,receiver_id)
    const params=new HttpParams().set('complainer_userid',complainer_userid).set('receiver_id',receiver_id)
    return this.http.post(AppSetting.API_ENDPOINT_PHP + 'post/report?',params);
  
  }

  
  showPostEdit(post_id){
    console.log('post id' ,post_id)
    return this.http.get(AppSetting.API_ENDPOINT_PHP+ 'post/show_post/' + post_id)
     }

    
  updatePost(Post_id,File,content){

    console.log('in service section',Post_id,File,content)
    if(File != null){
      const formData = new FormData();
      formData.append('post_id',Post_id);
      formData.append('file',File,File.name);
      formData.append('content',content);
    console.log('data',formData)
    return this.http.post(AppSetting.API_ENDPOINT_PHP +'post/update_post/',formData)
    }else{
      const formData = new FormData();
      formData.append('post_id',Post_id);
      formData.append('file',File);
      formData.append('content',content);
         console.log('data',formData)
      const params = new HttpParams().set('post_id',Post_id).set('content',content)
      return this.http.post(AppSetting.API_ENDPOINT_PHP +'post/update_post/',params)
    }
    
  }
  
  //http://social.nuagedigitech.com/api/post/fetch_post_postid?post_id=3

  getPostDetailsById(post_id){
    const params = new HttpParams().set('post_id',post_id);
    return this.http.post(AppSetting.API_ENDPOINT_PHP+'post/fetch_post_postid',params)
  }  


  hidePost(post_id,status){
    const params = new HttpParams().set('post_id',post_id).set('status',status)

    return this.http.post(AppSetting.API_ENDPOINT_PHP+'post/hide_show_post',params)
  }

}
