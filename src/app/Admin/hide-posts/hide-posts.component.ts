import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { Socialusers } from 'src/app/home/user.model';
import { AdminPostService } from '../admin-post/admin-post.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-hide-posts',
  templateUrl: './hide-posts.component.html',
  styleUrls: ['./hide-posts.component.css']
})
export class HidePostsComponent implements OnInit {

  socialusers:Socialusers;
  user:any;
  status=false;
  CloseBtn:boolean=true;
  post:any;
  postObj;
  model :any = {comment:''};
  editPostModel : any = {};
  fileToUpload;
  fileUpload:any = {status: '', message: '', filePath: ''};
  error:string;
  iFlag:boolean=false;
  message:string="";
  status_id:number;
  searchText;
  
  allPost;
  
  postForm = new FormGroup({
    
    content:new FormControl(),
   
  })

  editForm=new FormGroup({
    content:new FormControl(),
    
  })
  constructor(private postService : AdminPostService,private router : Router,
    private toastr : ToastrService) { }

  ngOnInit() {
    
  this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
  this.getAllPostList();
  this.getTrendingPostList();
  }

 

  name="1418";
  
  // Add Comment
   addComment(id)
   {
     console.log(this.socialusers.id)
     console.log(this.model.comment,id);
   this.postService.commentPost(this.socialusers.id,id,this.model.comment,this.name,this.fileToUpload)
   .subscribe((response : any) =>{
     console.log('comment Response',response)
     this.status=true;
   this.model.comment="";
   this.getAllPostList();
   this.router.navigate(['/admin/posts']);
   })
     
  }

  likePost(post_id)
  {
      //console.log('like'+post_id)
    return this.postService.storeLike(this.socialusers.id, post_id).subscribe((data)=>{
      console.log('like Data ',data);
      this.getAllPostList();
    });

  }

  //get All Post

  postStatus = 0;
  getAllPostList(){
    this.postService.getAllPost(this.postStatus).subscribe((response : any)=>{
      console.log('get All Post',response)
      this.allPost=response.object;
      console.log(this.allPost)
    })
  }

  //share post
  share_status =1;
  sharePost(id){
    console.log('post_id for share post',id)
    this.postService.shareUserPost(this.socialusers.id,id,this.share_status,this.name)
    .subscribe((response : any)=>{
      if(response.object != null){
        console.log('share post response',response);
        this.toastr.success('Post Share Successfully');
        this.getAllPostList();
      }else{
        this.toastr.error('Something Went Wrong');
      }
    })
  }

  //delete post

deletePost(id){
  
  console.log(id)
  this.postService.deletePost(id)
  .subscribe((response : any) =>{
    console.log('Delete post Response',response)
    this.toastr.success('Post Delete Successfully');
    this.getAllPostList();
  })
}

//Report post

reportPost(id,receiver_id){
 
  console.log(id,this.socialusers.id,receiver_id)
  this.postService.reportPost(id,this.socialusers.id,receiver_id)
 .subscribe((response : any)=>{
  console.log('response',response);
  console.log('report Added successfully');
  this.toastr.success('Report Added');
  this.getAllPostList
 })
}

editData = [];
editFormData
showPost(id){
  console.log(id);
  this.postService.showPostEdit(id)
  .subscribe((response :any )=>{
  console.log('showpost',response)
  this.editPostModel=response.object;
  
  })
}


imageToUpload;
editPostImage(files:FileList )
{

  this.imageToUpload = files.item(0);
  console.log('Edit Image',this.imageToUpload);
}


editUserPost(id,content){
  let formData=this.editForm.getRawValue()
      console.log('Edit Image in Typescript',id,this.imageToUpload,content)
      this.postService.updatePost(id,this.imageToUpload,content)
      .subscribe((response : any) =>{
        if(response.object != null){
          this.toastr.success('post updated Successfully');
          content = {};
          this.getAllPostList();
          window.document.getElementById("close_model").click() ;
        }else{
          this.toastr.warning('Something Went wrong');
        }
       
      }) 
}

postData =[]

getPostDetailsById(id){
  console.log(id);
  this.postService.getPostDetailsById(id).subscribe((response :any)=>{
    console.log('post details by id response',response);
    this.postData = response.object;
  })
}

trendingPost = [];

  type = 0;
  getTrendingPostList() {
    this.postService.getTrendingPostList(this.type)
      .subscribe((response: any) => {
        console.log('Trending List', response);
        this.trendingPost = response.object;
      })
  }
  hideStatus = 1
  unHidePost(id){
    console.log('post id',id)
    this.postService.hidePost(id,this.hideStatus)
    .subscribe((response : any)=>{
      this.getAllPostList();
      console.log('hide response',response)
    })
  }

}
