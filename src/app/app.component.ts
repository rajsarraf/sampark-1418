import { Component } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { Socialusers } from './home/user.model';
import { NotifierService } from 'angular-notifier';
import { NotificationServiceService } from './commonService/notification-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sampark1418';

  message; 
  socialusers: Socialusers;
  currentMessage = new BehaviorSubject(null);
  private notifier: NotifierService;
  constructor(private angularFireMessaging: AngularFireMessaging,
    private http : HttpClient,private toastr : ToastrService,notifier: NotifierService,
    private notificationService : NotificationServiceService) {
      this.notifier = notifier;  
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )

     }
  ngOnInit() {
   
    this.socialusers = JSON.parse(localStorage.getItem('currentUser'));
    this.requestPermission();
    this.receiveMessage();;
   // console.log(this.message);
  }

  devicetype = "web";
  requestPermission() {
  
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        //console.log('user_id',this.socialusers.id);
        //console.log(token);
       //return this.http.post(this.url,token);
       this.notificationService.sentDeviceToken(this.socialusers.id,token,this.devicetype)
       .subscribe((response : any)=>{
         //console.log('send device token response',response);
       })
        
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  
  receiveMessage() {
    
    this.angularFireMessaging.messages.subscribe(
      (payload : any) => {    
        //console.log("new message received. ", payload);
        this.currentMessage.next(payload);
        this.message = this.currentMessage;
       // this.toastr.show(this.message._value.notification.icon,this.message._value.notification.body)
        this.notifier.notify('success',this.message._value.notification.body)
       // this.toastr.success(this.message._value.notification.body)
       // console.log('notification',this.message._value.notification.body);
        
      })
  }


}
