const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(__dirname + '/dist/sampark1418'));

app.get('/*', function(req,res) {
    
res.sendFile(path.join(__dirname+'/dist/sampark1418/index.html'));
});
let port=4040;
app.listen(process.env.PORT || port),function(){
console.log(`Server Started at ${port}`)
};